/* 
 *  Projet Euler  #4
 */
#include <stdio.h>
#include <math.h>


#define  NDIGITS  3



unsigned  mypow
  (unsigned b, unsigned p)
{
	unsigned acc;
	
	for(acc = 1;  p;  p /= 2) {
		if(p%2)
			acc *= b;
		b *= b;
	}
	
	return acc;
}



unsigned  infpow
  (unsigned b, unsigned n)
{
	unsigned r;
	
	for(r = 1;  !(n % r);  r *= b);
	
	return r / b;
}



/*unsigned  getPalindromic
  (unsigned ndig)
{
	unsigned tens = mypow(10, ndig);
	unsigned n = tens*tens - 1;
	
	tens /= 10;
	
	/* Let’s enumerate all palindromic numbers with 2*ndig digits, in descending
	   order. * /
	for(unsigned i = 1;  i <= 9*tens;  i++) {    // i is the nº of the palindrom
		//printf("%u\n", n);
		for(unsigned k = ceil(tens/11.);  k <= tens*10/11;  k++) {
			if(!(n % (11*k))  &&  n/(11*k) < tens*10  &&  n/(11*k) >= tens) {
				printf("%u = %u*%u\n", n, 11*k, n/(11*k));
				return n;
			}
		}
		/* Next palindrom * /
		n -= 11 * tens / infpow(10, i);
	}
	
	return 0xfeel;
}*/



/* Optimization: in fact, we divide n per 11, so n is not the real palindrom
 * but its quotient per 11. */
unsigned  getPalindromic
  (unsigned ndig)
{
	unsigned tens = mypow(10, ndig);
	unsigned n = (tens*tens - 1) / 11;
	
	tens /= 10;
	
	/* Let’s enumerate all palindromic numbers with 2*ndig digits, in descending
	   order. */
	for(unsigned i = 1;  i <= 9*tens;  i++) {    // i is the nº of the palindrom
		//printf("%u\n", 11*n);
		for(unsigned k = ceil(tens/11.);  k <= tens*10/11;  k++) {
			if(!(n % k)  &&  n/k < tens*10  &&  n/k >= tens) {
				printf("%u = %u*%u\n", 11*n, 11*k, n/k);
				return 11*n;
			}
		}
		/* Next palindrom */
		n -= tens / infpow(10, i);
	}
	
	return 0xfeel;
}



int main(void)
{
	unsigned n = getPalindromic(NDIGITS);
	
	printf(
	  "Largest palindromic product of two %u-digits integers:\n"
	  "    %u\n",
	  NDIGITS, n
	);
	return 0;
}
