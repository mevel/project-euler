/* 
 *  Projet Euler  #9
 */
#include <stdio.h>



#define  N  1000



/*
	
	| a + b + c = n   ⇔   c = n-a-b
	| a² + b² = c²
	⇔   a² + b² = (n-a-b)²
	⇔   …   
	⇔   0 = n² - 2na - 2nb + 2ab
	⇔   b = n(n-2a) / (2(n-a))
	⇔   b = n/2 (1 - a/(n-a))
	
	So we must find a such as b is an integer, that is, such that n/2 * a is
	divisible by n-a).
	
*/
unsigned  PythagoreanTripletProduct
  (unsigned n)
{
	unsigned a, b, c;
	
	if(n%2)
		return 0;
	
	/* While b is not an integer… */
	for(a = 1;  (n/2 * a % (n-a));  a++)
		if(a  >  n/2 * (1 - a/(n-a)))
			return 0;
	
	b = n/2 - n/2 * a/(n-a);
	c = n - a - b;
	
	printf("%u² + %u² = %u²\n", a, b, c);
	printf("%u + %u + %u = %u\n", a, b, c, n);
	return a*b*c;
}



int main(void)
{
	unsigned p = PythagoreanTripletProduct(N);
	
	printf(
	  "Product of the first Pythagorean triplet (a,b,c) with a+b+c = %u:\n"
	  "    %u\n",
	  N, p
	);
	return 0;
}
