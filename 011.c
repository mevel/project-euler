/* 
 *  Projet Euler  #11
 */
#include <stdio.h>
#include <stdlib.h>



unsigned  largest_in_line
  (const unsigned** grid, size_t i, size_t c, size_t n, unsigned* max)
{
	unsigned m;
	size_t j, k;
	
	k = 0;
	
	/* k est la position dans la ligne d’où l’on démarre le calcul. Quand on
	   rencontre un 0 (en position j), on recommence après ce 0 (donc à k=j+1)
	   pour calculer la nouvelle valeur initiale, etc. */
  Begin:
	
	if(k+n <= c) {
		/* Calcul de la valeur initiale. */
		m = 1;
		for(j = k;  j < k+n;  ++j) {
			if(grid[i][j])
				m *= grid[i][j];
			else {
				k = j+1;
				goto Begin;
			}
		}
		/* Parcours du reste, avec mise à jour du maximum. */
		do {
			if(m > *max)
				*max = m;
			if(j == c)
				break;
			if(grid[i][j]) {
				m *= grid[i][j];
				m /= grid[i][j-n];
			}
			else {
				k = j+1;
				goto Begin;
			}
		} while(++j);
	}
	
	return *max;
}



unsigned  largest_in_col
  (const unsigned** grid, size_t l, size_t j, size_t n, unsigned* max)
{
	unsigned m;
	size_t i, k;
	
	k = 0;
	
	/* Idem que largest_in_line, mais sur une colonne (i varie, j fixé). */
  Begin:
	
	if(k+n <= l) {
		m = 1;
		for(i = k;  i < k+n;  ++i) {
			if(grid[i][j])
				m *= grid[i][j];
			else {
				k = i+1;
				goto Begin;
			}
		}
		do {
			if(m > *max)
				*max = m;
			if(i == l)
				break;
			if(grid[i][j]) {
				m *= grid[i][j];
				m /= grid[i-n][j];
			}
			else {
				k = i+1;
				goto Begin;
			}
		} while(++i);
	}
	
	return *max;
}



unsigned  largest_in_diag1
  (const unsigned** grid, size_t l, size_t c, size_t i, size_t j, size_t n, unsigned* max)
{
	unsigned m;
	size_t h, k;
	
	k = 0;
	
	/* Idem que largest_in_line, mais sur une diagonale descendante (i et j
	   fixés, h varie). */
  Begin:
	
	if(i+(k+n) <= l  &&  j+(k+n) <= c) {
		m = 1;
		for(h = k;  h < k+n;  ++h) {
			if(grid[i+h][j+h])
				m *= grid[i+h][j+h];
			else {
				k = h+1;
				goto Begin;
			}
		}
		do {
			if(m > *max)
				*max = m;
			if(i+h == l  || j+h == c)
				break;
			if(grid[i+h][j+h]) {
				m *= grid[i+h][j+h];
				m /= grid[i+h-n][j+h-n];
			}
			else {
				k = h+1;
				goto Begin;
			}
		} while(++h);
	}
	
	return *max;
}



unsigned  largest_in_diag2
  (const unsigned** grid, size_t l, size_t c, size_t i, size_t j, size_t n, unsigned* max)
{
	unsigned m;
	size_t h, k;
	
	k = 0;
	
	/* Idem que largest_in_line, mais sur une diagonale ascendante (i et j
	   fixés, h varie). */
  Begin:
	
	if(i+(k+n) <= l  &&  j >= (k+n)) {
		m = 1;
		for(h = k;  h < k+n;  ++h) {
			if(grid[i+h][j-h])
				m *= grid[i+h][j-h];
			else {
				k = h+1;
				goto Begin;
			}
		}
		do {
			if(m > *max)
				*max = m;
			if(i+h == l  || j-h == 0)
				break;
			if(grid[i+h][j+h]) {
				m *= grid[i+h][j-h];
				m /= grid[i+h-n][j-h+n];
			}
			else {
				k = h+1;
				goto Begin;
			}
		} while(++h);
	}
	
	return *max;
}



unsigned  largest_in_grid
  (const unsigned** grid, size_t l, size_t c, size_t n)
{
	unsigned max;
	size_t i, j;
	
	max = 0;
	
	/* En ligne. */
	if(c >= n)
		for(i = 0;  i < l;  ++i)
			largest_in_line(grid, i, c, n, &max);
	
	/* En colonne. */
	if(l >= n)
		for(j = 0;  j < c;  ++j)
			largest_in_col(grid, l, j, n, &max);
	
	if(c >= n  &&  l >= n) {
		/* En diagonale descendante. */
		for(i = 1;  i <= l-n;  ++i)
			largest_in_diag1(grid, l, c, i, 0, n, &max);
		for(j = 0;  j <= c-n;  ++j)
			largest_in_diag1(grid, l, c, 0, j, n, &max);
		
		/* En diagonale ascendante. */
		for(j = n;  j < c;  ++j)
			largest_in_diag2(grid, l, c, 0, j, n, &max);
		for(i = 1;  i <= l-n;  ++i)
			largest_in_diag2(grid, l, c, i, c-1, n, &max);
	}

	
	return max;
}



#define  DIM  20

const unsigned** grid;



int main(void)
{
	printf("%i\n", largest_in_grid(grid, DIM, DIM, 4));
	
	return EXIT_SUCCESS;
}



const unsigned** grid = (const unsigned*[DIM]){
	(unsigned[DIM]){ 8,  2, 22, 97, 38, 15,  0, 40,  0, 75,  4,  5,  7, 78, 52, 12, 50, 77, 91,  8},
	(unsigned[DIM]){49, 49, 99, 40, 17, 81, 18, 57, 60, 87, 17, 40, 98, 43, 69, 48,  4, 56, 62,  0},
	(unsigned[DIM]){81, 49, 31, 73, 55, 79, 14, 29, 93, 71, 40, 67, 53, 88, 30,  3, 49, 13, 36, 65},
	(unsigned[DIM]){52, 70, 95, 23,  4, 60, 11, 42, 69, 24, 68, 56,  1, 32, 56, 71, 37,  2, 36, 91},
	(unsigned[DIM]){22, 31, 16, 71, 51, 67, 63, 89, 41, 92, 36, 54, 22, 40, 40, 28, 66, 33, 13, 80},
	(unsigned[DIM]){24, 47, 32, 60, 99,  3, 45,  2, 44, 75, 33, 53, 78, 36, 84, 20, 35, 17, 12, 50},
	(unsigned[DIM]){32, 98, 81, 28, 64, 23, 67, 10, 26, 38, 40, 67, 59, 54, 70, 66, 18, 38, 64, 70},
	(unsigned[DIM]){67, 26, 20, 68,  2, 62, 12, 20, 95, 63, 94, 39, 63,  8, 40, 91, 66, 49, 94, 21},
	(unsigned[DIM]){24, 55, 58,  5, 66, 73, 99, 26, 97, 17, 78, 78, 96, 83, 14, 88, 34, 89, 63, 72},
	(unsigned[DIM]){21, 36, 23,  9, 75,  0, 76, 44, 20, 45, 35, 14,  0, 61, 33, 97, 34, 31, 33, 95},
	(unsigned[DIM]){78, 17, 53, 28, 22, 75, 31, 67, 15, 94,  3, 80,  4, 62, 16, 14,  9, 53, 56, 92},
	(unsigned[DIM]){16, 39,  5, 42, 96, 35, 31, 47, 55, 58, 88, 24,  0, 17, 54, 24, 36, 29, 85, 57},
	(unsigned[DIM]){86, 56,  0, 48, 35, 71, 89,  7,  5, 44, 44, 37, 44, 60, 21, 58, 51, 54, 17, 58},
	(unsigned[DIM]){19, 80, 81, 68,  5, 94, 47, 69, 28, 73, 92, 13, 86, 52, 17, 77,  4, 89, 55, 40},
	(unsigned[DIM]){ 4, 52,  8, 83, 97, 35, 99, 16,  7, 97, 57, 32, 16, 26, 26, 79, 33, 27, 98, 66},
	(unsigned[DIM]){88, 36, 68, 87, 57, 62, 20, 72,  3, 46, 33, 67, 46, 55, 12, 32, 63, 93, 53, 69},
	(unsigned[DIM]){ 4, 42, 16, 73, 38, 25, 39, 11, 24, 94, 72, 18,  8, 46, 29, 32, 40, 62, 76, 36},
	(unsigned[DIM]){20, 69, 36, 41, 72, 30, 23, 88, 34, 62, 99, 69, 82, 67, 59, 85, 74,  4, 36, 16},
	(unsigned[DIM]){20, 73, 35, 29, 78, 31, 90,  1, 74, 31, 49, 71, 48, 86, 81, 16, 23, 57,  5, 54},
	(unsigned[DIM]){ 1, 70, 54, 71, 83, 51, 54, 69, 16, 92, 33, 48, 61, 43, 52,  1, 89, 19, 67, 48}
};
