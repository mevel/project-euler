/* 
 *  Projet Euler  #12
 */
#include <stdio.h>

#define  NDIV  500



unsigned  triangle
  (unsigned n)
{
	return n * (n+1) / 2;
}



unsigned  count_divisors
  (unsigned n)
{
	unsigned count, p;
	unsigned d;
	
	count = 1;
	
	while(n % 2 == 0) {
		n /= 2;
		count++;
	}
	
	for(d = 3;  n != 1;  d += 2) {
		p = 1;
		while(n % d == 0) {
			n /= d;
			p++;
		}
		count *= p;
	}
	
	return count;
}



unsigned highly_divisible_triangular
  (unsigned nDiv)
{
	unsigned n;
	unsigned nOdd, nEven;
	
	nOdd = 1;
	for(n = 2;  ;  n += 2) {
		nEven = count_divisors(n/2);
		if(nOdd*nEven >= nDiv)
			return n-1;
		nOdd = count_divisors(n+1);
		if(nEven*nOdd >= nDiv)
			return n;
	}
}



int main(void)
{
	unsigned n;
	
	n = highly_divisible_triangular(NDIV);
	printf("First triangular number to have over %u divisors: T(%u) = %u\n",
	  NDIV, n, triangle(n));
	
	return 0;
}
