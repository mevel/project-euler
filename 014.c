/* 
 *  Projet Euler  #14
 */
#include <stdio.h>

#define  MAX  1000000



unsigned  syracuse_sequence_length
  (unsigned n)
{
	/* Optimisation : on mémorise les longueurs déjà calculées. */
	static unsigned lengths[MAX+1] = {0};
	
	if(n == 1)
		return 1;
	else if(n <= MAX && lengths[n])
		return lengths[n];
	else {
		unsigned count = 1 + syracuse_sequence_length((n % 2) ?  3*n + 1 : n/2);
		if(n <= MAX)
			lengths[n] = count;
		return count;
	}
}



unsigned longest_sequence
  (unsigned max)
{
	unsigned n, nmax;
	unsigned len, lenmax;
	
	lenmax = 0;
	for(n = 1;  n <= max;  n++) {
		len = syracuse_sequence_length(n);
		if(len > lenmax) {
			nmax = n;
			lenmax = len;
		}
	}
	
	return nmax;
}



int main(void)
{
	unsigned n;
	
	n = longest_sequence(MAX);
	printf("Number under %u producing the longest Syracuse sequence: %u"
	  " (%u terms in the sequence)\n",
	  MAX, n, syracuse_sequence_length(n));
	
	return 0;
}
