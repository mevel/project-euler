/* 
 *  Projet Euler  #16
 */
#include <stdio.h>
#include <math.h>
#include "bignum.h"
#include <ctype.h>


#define  P  1000



unsigned  digitsSum
  (char const* s)
{
	unsigned sum;
	
	for(sum = 0;  isdigit(*s);  s++)
		sum += *s-'0';
	
	return sum;
}



int main(void)
{
	char buf[SIZEOF_BIGINT_BUFFER];
	BigInt n;
	BigInt_pow2_(&n, P);
	BigInt_toStr(&n, buf);
	
	printf(
	  "2^%u = %s.\n"
	  "The sum of its %u digits is:\n"
	  "    %u\n",
	  P, buf, strlen(buf), digitsSum(buf)
	);
	return 0;
}
