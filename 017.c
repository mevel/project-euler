/*
 *  Project Euler  #17
 */
#include <stdio.h>

#define  MAX 1000



unsigned  count_letters
  (unsigned n)
{
	static const unsigned below20[20] =
	    /* Zero, one, two, three, four, five, six, seven, eight, nine. */
	  { 0, 3, 3, 5, 4, 4, 3, 5, 5, 4,
	    /* Ten, eleven, twelve, thirteen, fourteen, fifteen, sixteen, seventeen,
	       eighteen, nineteen. */
	    3, 6, 6, 8, 8, 7, 7, 9, 8, 8 };
	static const unsigned *digits = below20;
	/* Ten, twenty, thirty, forty, fifty, sixty, seventy, eighty, ninety. */
	static const unsigned tens[10] = {0, 3, 6, 6, 5, 5, 5, 7, 6, 6};
	static const unsigned hundred = 7,  thousand = 8,  and = 3;
	
	unsigned count;
	unsigned d;
	
	if(!n)
		return 4;    /* “zero” */
	
	count = 0;
	
	/* Units and tens. */
	d = n % 100;
	n /= 100;
	if(d) {
		if(d < 20)
			count = below20[d];
		else
			count = tens[d/10] + digits[d%10];
		/* “and”. */
		if(n)
			count += and;
	}
	
	/* Hundreds. */
	d = n % 10;
	if(d)
		count += digits[d] + hundred;
	
	/* Thousands. */
	d = n / 10;
	if(d)
		count += digits[d] + thousand;
	
	return count;
}



int main(void)
{
	unsigned n;
	unsigned count;
	
	count = 0;
	for(n = 1;  n <= MAX;  n++)
		count += count_letters(n);
	
	printf(
	  "Number of letters needed to write (in English) numbers from 1 to %u: "
	  "%u\n", MAX, count);
	
	return 0;
}
