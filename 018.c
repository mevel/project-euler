/* 
 *  Projet Euler  #18
 */
#include <stdio.h>



unsigned  max
  (unsigned a, unsigned b)
{
	return (a > b) ? a : b;
}



unsigned  maxPathSum
  (unsigned tree[], unsigned h)
{
	unsigned j;
	
	h--;    /* do not inspect the last row (since its nodes have no children) */
	j = h * (h+1) / 2;
	
	/* `h` represent the row above the one being inspected (starting from 1),
	   and the number of nodes is this row (ie. the number of nodes in the 
	   current row minus 1).
	   `j` is the number of nodes before the current row.
	   `i` is the index of the inspected node plus 1. */
	for(unsigned i = j;  i;  i--) {
		if(i == j) {    /* We move to the row above if needed. */
			j -= h;
			h--;
		}
		tree[i-1] += max(tree[i+h], tree[i+h+1]);
	}
	
	return tree[0];
}



//#define  HEIGHT   4
#define  HEIGHT  15

unsigned tree[HEIGHT * (HEIGHT+1) / 2];



int main(void)
{
	printf(
	  "Maximal path sum in the %u-row high tree:\n"
	  "    %u\n",
	  HEIGHT, maxPathSum(tree, HEIGHT)
	);
	return 0;
}



unsigned tree[HEIGHT * (HEIGHT+1) / 2] = {
/*	3,
	7, 4,
	2, 4, 6,
	8, 5, 9, 3*/
	75,
	95, 64,
	17, 47, 82,
	18, 35, 87, 10,
	20,  4, 82, 47, 65,
	19,  1, 23, 75,  3, 34,
	88,  2, 77, 73,  7, 63, 67,
	99, 65,  4, 28,  6, 16, 70, 92,
	41, 41, 26, 56, 83, 40, 80, 70, 33,
	41, 48, 72, 33, 47, 32, 37, 16, 94, 29,
	53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14,
	70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57,
	91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48,
	63, 66,  4, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31,
	 4, 62, 98, 27, 23,  9, 70, 98, 73, 93, 38, 53, 60,  4, 23
};
