/*
 *  Project Euler  #19
 */
#include <stdbool.h>
#include <stdio.h>



bool  is_leap_year
  (unsigned year)
{
	return year % 4 ?  false : year % 100 ? true : year % 16 ? true : false;
}



unsigned  count_days_first_of_month
  (unsigned endyear, unsigned d)
{
	static const unsigned months[12] =
	  { 31, 28, 31, 30, 31, 30,
	    31, 31, 30, 31, 30, 31 };
	
	unsigned count;
	unsigned day, month;
	
	count = 0;
	/* Jump to 1 Jan 1901 (does not inspect 1900). */
	day = 365;
	for(month = 12;  1900 + month/12 <= endyear;  month++) {
		/* If this a a Sunday. */
		if(day % 7 == d)
			count++;
		/* If this is February. */
		if(month % 12 == 1)
			day += is_leap_year(1900 + month/12) ? 29 : 28;
		else
			day += months[month%12];
	}
	
	return count;
}



int main(void)
{
	printf(
	  "Number of Sundays felling on the first of the month during the twentieth"
	  " century (1 Jan 1901 to 31 Dec 2000): %u\n",
	  count_days_first_of_month(2000, 6));
	
	return 0;
}
