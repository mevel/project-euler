/* 
 *  Projet Euler  #20
 */
#include <stdio.h>
#include "bignum.h"


#define  N  100



void  fact
  (BigInt* dest, unsigned n)
{
	BigInt_set_(dest, 1);
	for(;  n > 1;  n--)
		BigInt_mul_(dest, dest, n);
}



unsigned  digitsSum
  (char const* s)
{
	unsigned sum;
	
	for(sum = 0;  isdigit(*s);  s++)
		sum += *s-'0';
	
	return sum;
}



int main(void)
{
	char buf[SIZEOF_BIGINT_BUFFER];
	BigInt f;
	unsigned n = N;
	fact(&f, n);
	BigInt_toStr(&f, buf);
	
	printf(
	  "%u! = %s.\n"
	  "The sum of its %u digits is:\n"
	  "    %u\n",
	  N, buf, strlen(buf), digitsSum(buf)
	);
	return 0;
}
