/* 
 *  Projet Euler  #21
 */
#include <stdio.h>



unsigned  divisors_sum
  (unsigned n)
{
	unsigned i, sum;
	
	sum = 0;
	for(i = 2;  i*i < n;  ++i) {
		if(n % i == 0)
			sum += i + n/i;
	}
	if(i*i == n)
		sum += i;
	
	return 1 + sum;
}



#define  MAX  10000

int main(void)
{
	unsigned sum, n, a, b;
	
	sum = 0;
	for(n = 0;  n <= MAX;  ++n) {
		a = divisors_sum(n);
		b = divisors_sum(a);
		if(b == n) {
			if(a == n)
				printf("perfect number:  %i\n", n);
			else if(a > n) {
				printf(" amicable pair:  %i, %i\n", n, a);
				sum += a + n;
			}
		}
	}
	
	printf("\nsum of amicable numbers under %i: %i\n", MAX, sum);
	
	return 0;
}
