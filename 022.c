/*
 *  Project Euler  #22
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>



int compare_names
  (const void* s, const void* t)
{
	return strcmp(*(const char* const *)s, *(const char* const *)t);
}



unsigned  name_score
  (const char* name)
{
	unsigned score;
	
	for(score = 0;  *name;  name++)
		score += *name - 'A' + 1;
	
	return score;
}



unsigned  names_score
  (const char* names[], size_t n)
{
	unsigned score;
	
	qsort(names, n, sizeof(char*), compare_names);
	
	score = 0;
	for(size_t i = 0;  i < n;  i++)
		score += (i+1) * name_score(names[i]);
	
	return score;
}



#define  N  ( sizeof(names)/sizeof(*names) )

const char* names[] = {
	#include "022-names"
};



int main(void)
{
	printf("Total score of the %u names: %u\n", N, names_score(names, N));
	
	return 0;
}
