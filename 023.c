/* 
 *  Projet Euler  #23
 */
#include <stdbool.h>
#include <stdio.h>

#define  UPPER  28123



unsigned  divisors_sum
  (unsigned n)
{
	unsigned i, sum;
	
	sum = 1;
	for(i = 2;  i*i < n;  ++i) {
		if(n % i == 0)
			sum += i + n/i;
	}
	if(i*i == n)
		sum += i;
	
	return sum;
}



bool  is_in_array
  (const unsigned t[], unsigned n, unsigned a)
{
	while(n)
		if(t[--n] == a)
			return true;
	return false;
}



int main(void)
{
	unsigned abundants[UPPER];
	size_t abundants_count = 0;
	
	unsigned sum, n;
	
	sum = UPPER * (UPPER+1) / 2;    /* La somme des entiers de 1 à UPPER */
	for(n = 1;  n <= UPPER;  n++) {
		/* Si n peut s’écrire comme somme de deux abondants. */
		for(size_t i = 0;  i < abundants_count;  i++)
			if(is_in_array(abundants, abundants_count, n - abundants[i])) {
				sum -= n;
				break;
			}
		/* Si n est abondant. */
		if(n < divisors_sum(n))
			abundants[abundants_count++] = n;
	}
	
	printf(
	  "Sum of numbers which are not the sum of two abundant numbers: %i"
	  " (there are %u abundant numbres).\n", sum, abundants_count);
	
	return 0;
}
