/* 
 *  Project Euler  #24
 */
#include <stdio.h>
#include <string.h>    /* memmove */



#define  E  "0123456789"    /* set of elements */
#define  N  10              /* number of elements */
#define  I  1000000         /* nº of the permutation (starting with 1) */



/* i-th permutation of p elements in n (i starting with 0). */
void  permutation_of_subset
  (char set[], unsigned n, unsigned p, unsigned i)
{
	/* Basic case. */
	if(!p)
		return;
	
	unsigned j = i / (n-p+1);    /* index of the permutation of p-1 in n */
	unsigned k = i % (n-p+1);    /* index of the element to be chosen in the available elements */
	
	/* Recursion. */
	permutation_of_subset(set, n, p-1, j);
	
	/* Adding the p-th element and updating the set of available elements. The
	   p-1 first elements are the permutation already done, the available set
	   starts after that. */
	char e = set[p-1 + k];    /* selected element */
	memmove(&set[p], &set[p-1], k);    /* shift in order to insert e and update the available set */
	set[p-1] = e;
}



/* i-th permutation of n elements (in n) (i starting with 0). */
void  permutation_of_set
  (char set[], unsigned n, unsigned i)
{
	permutation_of_subset(set, n, n, i);
}



int  main
  (void)
{
	/* A poor joke is hidden in those lines, can you find it? */
	char set[] = E;
	char perm[] = E;
	permutation_of_set(perm, N, I-1);
	
	printf(
	  "%uth permutation of \"%s\" (%u elements):\n"
	  "    \"%s\"\n",
	  I, set, N, perm
	 );
	return 0;
}
