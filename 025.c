/*
 *  Project Euler  #25
 */
#include <math.h>
#include <stdio.h>

#define  NDIGITS  10000




unsigned  first_fib_with_d_digits
  (unsigned ndigits)
{
	unsigned n;
	double a, b;
	
	a = 0;
	b = 1;
	
	for(n = 1;  log10(b)+1 < ndigits;  n++) {
		b = a + b;
		a = b - a;
		/* On enlève 10^300 pour éviter l’overflow (la capacité du type double
		   étant plafonnée vers 10^308). */
		if(log10(b) > 300) {
			a /= 1e300;
			b /= 1e300;
			ndigits -= 300;
		}
	}
	
	return n;
}



int main(void)
{
	unsigned n;
	
	n = first_fib_with_d_digits(NDIGITS);
	printf(
	  "First term in the Fibonacci sequence to contain %u digits: fib(%u).\n",
	  NDIGITS, n);
	
	return 0;
}
