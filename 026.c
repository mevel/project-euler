/*
 *  Project Euler #26
 */
#include <stdio.h>

#define  BASE  10
#define  UPPER  1000



unsigned  recurring_cycle_length
  (unsigned n)
{
	unsigned len;
	unsigned rem;
	
	while(n%2 == 0)
		n /= 2;
	while(n%5 == 0)
		n /= 5;
	if(n == 1)
		return 0;
	
	rem = BASE % n;
	for(len = 1;  rem != 1;  len++)
		rem = (BASE*rem) % n;
	
	return len;
}


int main(void)
{
	unsigned n, nmax, len, lenmax;
	
	lenmax = 0;
	for(n = 1;  n <= UPPER;  n++) {
		len = recurring_cycle_length(n);
		if(len > lenmax) {
			lenmax = len;
			nmax = n;
		}
	}
	
	printf(
	  "Number n below %u for which 1/n has the longest recurring cycle"
	  " (in base %u): %u (length of the cycle: %u)\n",
	  UPPER, BASE, nmax, lenmax);
	
	return 0;
}
