/*
 *  Project Euler  #27
 */
#include <math.h>
#include <stdbool.h>
#include <stdio.h>

#define  AMAX  1000
#define  BMAX  1000



bool  is_prime
   (unsigned n)
{
	unsigned d, root;
	
	if(n % 2 == 0)
		return n == 2;
	
	root = floor(sqrt(n));
	for(d = 3;  d <= root;  d += 2)
		if(n % d == 0)
			return false;
	
	return true;
}




unsigned  prime_sequence_length
  (int a, int b)
{
	unsigned n, P;
	
	/* Avec P(n) = n²+an+b, on a P(0) = b, et P(n+1) = P(n) + 2n+a+1. */
	P = b;
	for(n = 0;  is_prime(P);  n++)
		P += 2*n + a + 1;
	
	return n;
}



int main(void)
{
	unsigned a, b;
	unsigned len, len2, lenmax;
	int amax, bmax;
	int prod;
	
	lenmax = 0;
	/* a doit être impair, car pour passer de P(n) à P(n+1) on ajoute 2n+1+a,
	   donc si n était pair, la parité du polynôme serait alternée, ce qui est
	   contre-productif. */
	for(a = 1;  a <= AMAX;  a += 2) {
		/* b doit être premier (car P(0) = b…), donc positif et impair (b=2 est
		   exclus car a est impair donc P(1)=1+a+b serait pair). */
		for(b = 3;  b <= BMAX;  b += 2) {
			len = prime_sequence_length(a, b);
			len2 = prime_sequence_length(-a, b);
			if(len > lenmax  ||  len2 > lenmax) {
				lenmax = len;
				amax = a;
				bmax = b;
				prod = a*b;
				if(len < len2) {
					lenmax = len2;
					amax = -amax;
					prod = -prod;
				}
			}
		}
	}
	
	printf(
	  "Product of the coefficients a and b for which n²+an+b produces the"
	  " longest sequence of prime numbers: %i×%i = %i (primes produced: %u)",
	  amax, bmax, prod, lenmax);
	
	return 0;
}
