let bi = Big_int.big_int_of_int
and ( ** ) = Big_int.mult_int_big_int
and cmp = Big_int.compare_big_int;;

let create_list max =
    let rec create_rec a b acc =
        if a > max then
            []
        else if b > max then
            create_rec (a+1) 2 (bi ((a+1)*(a+1)))
        else
            acc :: create_rec a (b+1) (a**acc)
    in
    create_rec 2 2 (bi 4)
;;

let count_unique l =
    let rec count_rec last = function
     | []   -> 0
     | e::q ->
        if cmp e last = 0 then
            count_rec last q
        else
            1 + count_rec e q
    in
    let l = List.sort cmp l  in
    1 + count_rec (List.hd l) l
;;

count_unique (create_list 100);;
