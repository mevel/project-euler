/*
 *  Project Euler  #30
 */
#include <stdio.h>



unsigned  pow5_digits_sum
  (unsigned n)
{
	static const unsigned pow5[10] =
	  { 0, 1, 32, 243, 1024, 3125, 7776, 16807, 32768, 59049 };
	
	unsigned sum = 0;
	while(n) {
		sum += pow5[n%10];
		n /= 10;
	}
	return sum;
}



int main(void)
{
	unsigned n, p, sum;
	
	sum = 0;
	/* Les nombres cherchés peuvent être majorés par :
	 *      6×9⁵ = 354 294         (pour 999 999)
	 * puis par :
	 *      2⁵ + 5×9⁵ = 295 277    (pour 299 999)
	 * (299 999 étant le nombre inférieur à 354 294 qui produit la plus grande
	 * valeur), et enfin par :
	 *      1⁵ + 5×9⁵ = 295 246    (pour 199 999). */
	for(n = 2;  n <= 295246;  n++) {
		p = pow5_digits_sum(n);
		if(n == p)
			printf("%u\n", p),
			sum += n;
	}
	
	printf(
	  "\nSum of numbers which are equal to the sum of the fifth power of their"
	  " digits: %u\n", sum);
	return 0;
}
