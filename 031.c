/*
 *  Project Euler  #31
 */
#include <stdio.h>



/* Donne le nombre de combinaisons linéaires possibles pour obtenir val.
 * Prend en entrée la liste des valeurs distinctes à combiner (tableau ‘coins’)
 * et leur nombre ‘n’. */
unsigned  count_ways
  (unsigned val, const unsigned coins[], unsigned n)
{
	if(n == 1)
		return (val % *coins) ? 0 : 1;
	
	unsigned count;
	
	count = count_ways(val, coins, n-1);
	while(val >= coins[n-1]) {
		val -= coins[n-1];
		count += count_ways(val, coins, n-1);
	}
	
	return count;
}



int main(void)
{
	const unsigned coins[] = {1, 2, 5, 10, 20, 50, 100, 200};
	
	printf("Number of ways to form £2 = 200p: %u\n", count_ways(200, coins, 8));
	
	return 0;
}
