#include <stdio.h>
#include <stdbool.h>

#define  TEST(i)        \
	if (digits_seen[i]) \
		continue;       \
	else                \
		digits_seen[i] = true

int main(void)
{
	bool digits_seen[10] = {true,};
	
	for (unsigned n = 1000; n <= 9999; ++n) {
		for (unsigned d = 2; d*d < n; ++d) {
			if (n % d)
				continue;
			for (size_t i = 1; i <= 9; ++i)
				digits_seen[i] = false;
			TEST(n % 10);
			TEST(n/10 % 10);
			TEST(n/100 % 10);
			TEST(n/1000 % 10);
			unsigned p = n / d;
			if (1 <= d && d <= 9 && 1000 <= p && p <= 9999) {
				TEST(d);
				TEST(p % 10);
				TEST(p/10 % 10);
				TEST(p/100 % 10);
				TEST(p/1000 % 10);
				printf("%u × %u = %u\n", d, p, n);
			}
			else if (10 <= d && d <= 99 && 100 <= p && p <= 999) {
				TEST(d % 10);
				TEST(d/10 % 10);
				TEST(p % 10);
				TEST(p/10 % 10);
				TEST(p/100 % 10);
				printf("%u × %u = %u\n", d, p, n);
			}
			else
				continue;
		}
	}
	
	return 0;
}
