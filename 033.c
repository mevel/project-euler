/*
 *  Project Euler  #33
 */
#include <stdio.h>



/*
 *  Let D be the simplifiable digit. Basically, there are four possible cases:
 *       BD    DA    AD    DA                                A
 *       —— ,  —— ,  —— ,  ——    which all must be equal to  — and lesser than 1
 *       BD    DB    DB    BD                                B
 *  
 *  In all cases, D = 0 is not possible (with the trivial case A0/B0 = A/B being
 *  ignored, as well as 0A/0B because we want two digits exactly). We also have
 *  A ≠ 0 and B ≠ 0 (otherwise, A/B would be null or undefined). Furthermore, we
 *  want a fraction lesser than 1, so A<B.
 *  So, we have 1 ≤ D ≤ 9, and 1 ≤ A < B ≤ 9.
 *  
 *  By mathematics (doing the cross-multiplication) we show that the first two
 *  cases are checked if and only if D = 0 or A = B, which is excluded by what
 *  precedes. The second two cases remain.
 *  
 *  We notice than the last case is similar to the third, by just reversing the
 *  fractions and exchanging the role of A and B. So, let’s consider only the
 *  third case: if we get a solution with A < B, we actually fell in that case;
 *  with A > B, we fell in the last case.
 *  
 *  By cross-multiplication and simplification, we have the third case if and
 *  only if b = (10ad) / (9a+d), so if and only if 10ad is divisible by 9a+d
 *  and their quotient is below 9.
 *  By studying the function f(d,a) = (10ad) / (9a+d), it can be shown that:
 *       a < b ⇔ a < d  (a solution in the third case);
 *       a = b ⇔ a = d  (unwanted);
 *       a > b ⇔ a > d  (a solution in the fourth case, a and b to be swapped).
 *  In all case, we get 1 ≤ b ≤ 9.
 */



int main(void)
{
	unsigned d, a, b;
	unsigned prod_num;
	unsigned prod_denom;
	
	prod_num = prod_denom = 1;
	
	for(d = 1;  d <= 9;  d++) {
		/* Third case (a < d). The fraction is a/b. */
		for(a = 1;  a < d;  a++)
			if((10*a*d) % (9*a+d) == 0) {
				b = (10*a*d) / (9*a+d);
				printf("%u/%u = %u/%u\n", 10*a+d, 10*d+b, a, b);
				prod_num *= a;
				prod_denom *= b;
			}
		/* Fourth case (a > d). The fraction is b/a. */
		for(a = d+1;  a <= 9;  a++)
			if((10*a*d) % (9*a+d) == 0) {
				b = (10*a*d) / (9*a+d);
				printf("%u/%u = %u/%u\n", 10*d+b, 10*a+d, b, a);
				prod_num *= b;
				prod_denom *= a;
			}
	}
	
	printf("\nProduct of the above fractions: %u/%u\n", prod_num, prod_denom);
	
	return 0;
}
