/* 
 *  Projet Euler  #34
 */
#include <stdio.h>



unsigned  digits_factorial_sum
  (unsigned n)
{
	static const unsigned digit_fact[10] =
		{1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880};
	
	unsigned sum;
	
	sum = 0;
	for(;  n;  n /= 10)
		sum += digit_fact[n%10];
	
	return sum;
}



int main(void)
{
	unsigned sum, n;
	
	sum = 0;
	/* The searched numbers can not exceed 2540160. */
	for(n = 10;  n < 2540160;  ++n)
		if(digits_factorial_sum(n) == n) {
			printf("%i\n", n);
			sum += n;
		}
	
	printf("\nsum of all numbers which are equal to the sum of the factorial of their digits: %i\n", sum);
	
	return 0;
}
