let number_length n =
  int_of_float (ceil (log10 (float (n+1))))

exception Break

let circular_primes n =
  let sieve = Array.init n (fun i -> i mod 2 <> 0) in
  let count = ref 1 in    (* ne pas oublier de compter 2 *)
  Printf.printf "2\n";
  let i = ref 1 in
  while i := !i+2; !i < n do
    if sieve.(!i) then begin
      (* On calcule le crible d’Ératosthène *)
      let j = ref !i in
      while j := !j+ !i; !j < n do
        sieve.(!j) <- false
      done;
      (* On sait que i est premier, on regarde si ses rotations le sont aussi. *)
      let len = number_length !i in
      let pow10 = int_of_float (10.0 ** (float (len-1))) in
      let rot = ref !i in
      try
        for k = 1 to len-1 do
          rot := (!rot mod 10) * pow10 + !rot/10;
          (* Si la rotation n’est pas première, on arrête. Si on n’est pas encore
             arrivé jusqu’à elle dans le crible, on arrête aussi, ce cycle sera
             de nouveau testé quand on l’atteindra. Ainsi, seul le test à partir
             de l’élément maximal du cycle aboutira. *)
          if !rot > !i || not (sieve.(!rot)) then raise Break;
        done;
        (* En cas de succès du test, on compte tous les éléments du cycle ;
           — le cycle est de taille < ‘len’ ssi un des éléments y apparaît plusieurs,
             fois, ssi l’écriture décimale de cet élément présente un facteur répété
             au moins deux fois (u = vv…v). Dans ce cas, c’est un multiple d’un nombre
             de la forme 101, 10101, 1001, etc.
             —— si c’est un multiple strict, alors il n’est pas premier donc le cas
                ne se présente pas ;
             —— sinon,
                ——— soit il comporte un zéro et dans ce cas n’est pas circulairement
                    premier (car une rotation est multiple de 10),
                ——— soit il est de la forme 11…11, et alors le cycle est de taille 1
           — sinen, il est de taille ‘len’.
           Le seul cas à distinguer est donc celui du cycle de longueur 1, ce qui est
           caractérisé par le fait que i soit égal à la dernière (ou la première)
           rotation calculée. *)
        count := !count + (if !rot = !i then 1 else len);
        Printf.printf "%u\t(len = %u)\n" !i len
      with Break -> ()
    end
  done;
  !count

let () =
  circular_primes 1_000_000 |> Printf.printf "answer; %u\n"
