/*
 *  Project Euler  #36
 */
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>



unsigned  rev_and_concat
  (unsigned base, unsigned m, unsigned n)
{
	unsigned r, p;
	
	r = 0;
	p = 1;
	for(;  n;  n /= base) {
		r = base*r + (n%base);
		p *= base;
	}
	
	return m*p + r;
}

unsigned  make_even_palindrome
  (unsigned base, unsigned n)
{
	return rev_and_concat(base, n, n);
}

unsigned  make_odd_palindrome
  (unsigned base, unsigned n)
{
	return rev_and_concat(base, n, n/base);
}



bool  is_palindromic2
  (unsigned n)
{
	unsigned log2, i;
	
	if(!n)
		return true;
	
	log2 = sizeof(unsigned)*CHAR_BIT - 1;
	for(;  (n & (1<<log2)) == 0;  log2--);
	
	for(i = 0;  i <= log2/2;  i++)
		if( 1  &  ((n>>(log2-i)) ^ (n>>i)) )
			return false;
	
	return true;
}



int main(void)
{
	unsigned n, i;
	unsigned sum;
	
	sum = 0;
	for(i = 1;  i < 1000;  i++) {
		n = make_even_palindrome(10, i);
		if(is_palindromic2(n))
			printf("%u\n", n),
			sum += n;
		n = make_odd_palindrome(10, i);
		if(is_palindromic2(n))
			printf("%u\n", n),
			sum += n;
	}
	
	printf(
	  "\nSum of numbers below 1 000 000 which are palindromes both in bases 10"
	  " and 2: %u.\n", sum);
	
	return 0;
}
