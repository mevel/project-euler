/*
 *  Project Euler  #37
 */
#include <math.h>
#include <stdbool.h>
#include <stdio.h>



bool  is_prime
   (unsigned n)
{
	unsigned d, root;
	
	if(n % 2 == 0)
		return n == 2;
	if(n == 1)
		return false;
	
	root = floor(sqrt(n));
	for(d = 3;  d <= root;  d += 2)
		if(n % d == 0)
			return false;
	
	return true;
}



/* Teste si un nombre est tronquable à droite. */
bool  is_right_truncatable
  (unsigned n)
{
	for(;  n;  n /= 10)
		if(!is_prime(n))
			return false;
	return true;
}



/* Calcule la somme des nombres qui sont tronquables à gauche et à droite.
 * Pour cela, construit récursivement les nombres tronquables à gauche (car
 * si un nombre est tronquable, alors sa troncature l’est aussi) et teste s’ils
 * sont tronquables à droite.
 * La construction récursive consiste à prendre en entrée un nombre n tronquable
 * à gauche, à lui ajouter un chiffre (entre 1 et 9) à gauche puis à vérifier si
 * le nombre formé est lui-même premier. Si c’est le cas, alors le nombre formé
 * est tronquable à gauche, et il reste à tester s’il est tronquable à droite.
 * p est la puissance de 10 supérieure à n (correspondant au chiffre à ajouter).
 */
unsigned  sum_both_truncatable
  (unsigned n, unsigned p)
{
	unsigned d;
	unsigned sum;
	
	sum = 0;
	
	/* On ne peut pas ajouter de chiffre pair car le nombre formé ne serait pas
	 * tronquable à droite (car l’une de ses tronquatures serait paire donc non
	 * première). Seule exception, on peut éventuellement ajouter un 2 (car 2
	 * est premier), à condition de ne plus ajouter d’autre chiffre après. */
	n += 2*p;        // On ajoute le chiffre 2.
	if(is_prime(n) && is_right_truncatable(n/10))
		printf("%7u\n", n),
		sum += n;
	
	/* On ne peut ajouter qu’un chiffre impair (sauf cas spécial ci-dessus). */
	n -= p;          // On ajoute le chiffre 1 (on avait déjà le chiffre 2).
	for(d = 1;  d <= 9;  d += 2) {
		if(is_prime(n)) {
			if(is_right_truncatable(n/10))
				printf("%7u\n", n),
				sum += n;
			sum += sum_both_truncatable(n, p*10);
		}
		n += 2*p;    // On passe au chiffre impair suivant.
	}
	
	return sum;
}



int main(void)
{
	printf(
	  "\nSum of prime numbers which are left and right truncatable : %u.\n",
	  sum_both_truncatable(0, 1));
	
	return 0;
}
