/* 
 *  Projet Euler  #39
 */
#include <stdio.h>



#define  MAX  1000



/* For explanations of how this function works, see problem #9. */
unsigned  count_pythagorean_triplets_with_perimeter
  (unsigned n)
{
	unsigned a/*, b, c*/;
	unsigned count;
	
	if(n%2)
		return 0;
	
	count = 0;
	/* While b is not an integer… */
	for(a = 1;  a < n/2 - n/2 * a/(n-a);  a++) {
		if((n/2 * a % (n-a)) == 0)
			/*b = n/2 - n/2 * a/(n-a),
			c = n - a - b,
			printf("| %u² + %u² = %u²\n", a, b, c),
			printf("  %u + %u + %u = %u\n", a, b, c, n),*/
			count++;
	}
	
	return count;
}



int main(void)
{
	unsigned p, pmax;
	unsigned n, nmax;
	
	nmax = 0;
	for(p = 1;  p <= MAX;  p++) {
		n = count_pythagorean_triplets_with_perimeter(p);
		if(n > nmax) {
			nmax = n;
			pmax = p;
		}
	}
	
	printf(
	  "Perimeter below %u for which it exists the maximal number of Pythagorean"
	  " triplets: %u (the are %u matching triplets).\n",
	  MAX, pmax, nmax
	);
	return 0;
}
