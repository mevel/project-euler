/*
 *  Project Euler  #40
 */
#include <stdio.h>



unsigned  d
  (unsigned n)
{
	unsigned log10, p10;
	unsigned num, i;
	
	for(log10 = 1, p10 = 1;  n > 9*p10*log10;  log10++, p10*=10)
		n -= 9*p10*log10;
	
	num = (n-1)/log10 + p10;
	for(i = (n-1) % log10;  i < log10-1;  i++)
		num /= 10;
	
	return num % 10;
}


int main(void)
{
	unsigned n, p, dig, prod;
	
	prod = 1;
	p = 1;
	for(n = 1;  n <= 6;  n++) {
		dig = d(p);
		printf("d(%u) = %u\n", p, dig);
		prod *= dig;
		p *= 10;
	}
	
	printf("The product of those digits is: %u.\n", prod);
	
	return 0;
}
