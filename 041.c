/* 
 *  Project Euler  #41
 */
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>



/* i-th permutation of p elements in n (i starting with 0). */
void  permutation_of_subset
  (char set[], unsigned n, unsigned p, unsigned i)
{
	if(!p)
		return;
	
	unsigned j = i / (n-p+1);
	unsigned k = i % (n-p+1);
	
	permutation_of_subset(set, n, p-1, j);
	
	char e = set[p-1 + k];
	memmove(&set[p], &set[p-1], k);
	set[p-1] = e;
}

/* i-th permutation of n elements (in n) (i starting with 0). */
void  permutation_of_set
  (char dst[], const char set[], unsigned n, unsigned i)
{
	strcpy(dst, set);
	permutation_of_subset(dst, n, n, i);
}



bool  is_prime
   (uintmax_t n)
{
	uintmax_t d, root;
	
	if(n % 2 == 0)
		return n == 2;
	
	root = floor(sqrt(n));
	for(d = 3;  d <= root;  d += 2)
		if(n % d == 0)
			return false;
	
	return true;
}

bool is_prime_seq
  (const char seq[], unsigned n)
{
	uintmax_t num;
	
	if((seq[n-1]-'0') % 2 == 0)
		return false;
	
	for(num = 0;  *seq;  seq++)
		num = 10*num + (*seq-'0');
	
	return is_prime(num);
}



/*
bool is_pandigital
  (uintmax_t n)
{
	bool d[10] = {true, false};
	unsigned log10;
	unsigned sum;
	
	sum = 0;
	for(log10 = 1;  n;  n/=10) {
		if(d[n%10])
			return false;
		d[n%10] = true;
		sum += n%10;
		log10++;
	}
	return 2*sum == log10*(log10-1);
}
*/



int  main
  (void)
{
	const char digits[8] = "7654321";
	char seq[8] = {0};
	unsigned i;
	
	/* The pandigital prime numbers have 1, 4 or 7 digits (in any of the other
	   cases, the sum of the digits from 1 to n is a multiple of 3 so any
	   pandigital of that much digit is divisible by 3.
	   There are good chances the maximal pandigital prime number has 7 digits. */
	i = 0;
	do
		permutation_of_set(seq, digits, 7, i++);
	while(!is_prime_seq(seq, 7));
	
	printf("Greatest prime pandigital number: %s.\n", seq);
	return 0;
}
