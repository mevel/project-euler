/*
 *  Project Euler  #42
 */
#include <math.h>
#include <stdbool.h>
#include <stdio.h>



unsigned  word_value
  (const char* word)
{
	unsigned score;
	
	for(score = 0;  *word;  word++)
		score += *word - 'A' + 1;
	
	return score;
}



bool  is_triangle
  (unsigned n)
{
	/* The reverse function of n(n+1)/2 being (√(8n+1)-1) / 2… */
	unsigned root = floor(sqrt(8*n+1));
	return root*root == 8*n+1;
}



unsigned  count_triangle_words
  (const char* words[], size_t n)
{
	unsigned count;
	
	count = 0;
	for(size_t i = 0;  i < n;  i++)
		if(is_triangle(word_value(words[i])))
			count++;
	
	return count;
}



#define  N  ( sizeof(words)/sizeof(*words) )

const char* words[] = {
	#include "042-words"
};



int main(void)
{
	printf(
	  "Number of triangle words : %u (among %u).\n",
	  count_triangle_words(words, N), N);
	
	return 0;
}
