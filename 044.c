/*
 *  Project Euler #44
 */
#include <math.h>
#include <stdbool.h>
#include <stdio.h>



bool is_pentagonal
  (unsigned n)
{
	unsigned root = floor(sqrt(24*n+1));
	return root*root == 24*n+1  &&  root % 3 == 2;
}



int main(void)
{
	unsigned n, pentn, p, pentp, diff;
	
	diff = 0;
	
	pentp = 1;
	pentn = 5;
	for(n = 2;  !diff;  n++) {
		for(p = n-1;  p && !diff;  p--) {
			if(is_pentagonal(pentn+pentp) && is_pentagonal(pentn-pentp))
				printf("Sum and difference of P(%u) = %u and P(%u) = %u are"
				  " pentagonal.\n", p, pentp, n, pentn),
				diff = pentn-pentp;
			pentp -= 3*p - 2;
		}
		pentp = pentn;
		pentn += 3*n + 1;
	}
	
	printf("Their difference is: %u.\n", diff);
	return 0;
}
