/*
 *  Project Euler #45
 */
#include <math.h>
#include <stdint.h>
#include <limits.h>
#include <stdio.h>



/* Returns 0 if n is not a pentagonal antecedent, its antecedent otherwise. */
unsigned pentagonal_antecedent
  (uintmax_t n)
{
	uintmax_t root = floor(sqrt(24*n+1));
	return (root*root == 24*n+1  &&  root % 3 == 2)  ?  (root+1)/6  :  0;
}



#define  UPPER  99999999LL

int main(void)
{
	uintmax_t n, hn, p;
	
	/* Let’s enumerate the hexagonal numbers instead of triangular numbers,
	 * since the hexagonals are always triangular (H(n) = T(2n-1))… */
	hn = 1;
	for(n = 1;  n <= UPPER;  n++) {
		p = pentagonal_antecedent(hn);
		if(p)
			printf("T(%ju) = P(%ju) = H(%ju) = %ju\n", 2*n-1, p, n, hn);
		hn += 4*n + 1;
	}
	
	return 0;
}
