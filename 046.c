/*
 *  Project Euler  #46
 */
#include <math.h>
#include <stdbool.h>
#include <stdio.h>



bool  is_prime
   (unsigned n)
{
	unsigned d, root;
	
	if(n % 2 == 0)
		return n == 2;
	
	root = floor(sqrt(n));
	for(d = 3;  d <= root;  d += 2)
		if(n % d == 0)
			return false;
	
	return true;
}



int main(void)
{
	unsigned n, p, i;
	bool found;
	
	/* ans is the first number found (0 while searching). */
	found = false;
	for(n = 9;  !found;  n+=2) {
		p = n;
		//found = true;
		/* p == n - 2i². */
		for(i = 0;  !is_prime(p)  &&  p >= 2*(2*i+1)/*p >= 0*//*2*(2*i+1)*/;  i++) {
			/*if(is_prime(p)) {
				found = false;
				break;
			}*/
			p -= 2*(2*i+1);
		}
		if(p < 2*(2*i+1))
			found = true;
	}
	
	printf("First number negating Goldbach’s conjecture: %u.\n", n);
	return 0;
}
