#include <stdio.h>

unsigned count_prime_factors(unsigned n)
{
	unsigned count = 0;
	if (n % 2 == 0) {
		++count;
		do n /= 2; while (n % 2 == 0);
	}
	
	for (unsigned p = 3; p*p <= n; p += 2)
		if (n % p == 0) {
			++count;
			do n /= p; while (n % p == 0);
		}
	
	if (n > 1)
		++count;
	
	return count;
}

unsigned first_k_consecutive_numbers_with_k_prime_factors(unsigned k)
{
	unsigned n = 1;
	unsigned count[k];
	for (int i = 0; i < k; ++i) {
		printf(" %u", n);
		count[i] = count_prime_factors(n+i);
		printf("   -> %u\n", count[i]);
		if (count[i] != k) {
			n += i+1;
			i = -1;
		}
	}
	return n;
}

int main(void)
{
	printf("%u\n", first_k_consecutive_numbers_with_k_prime_factors(4));
	
	return 0;
}
