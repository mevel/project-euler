let pow ( * ) neutral a n =
    let rec powr acc a n =
        if n = 0 then
            acc
        else if n mod 2 = 1 then
            powr (acc*a) (a*a) (n/2)
        else
            powr acc (a*a) (n/2)
    in
    powr neutral a n
;;

let mul a b = Int64.(rem (mul a b) 10000000000L);;
let add a b = Int64.(rem (add a b) 10000000000L);;

let sum max =
    let rec sumr acc n =
        if n > max then
            acc
        else
            sumr (add acc (pow mul Int64.one (Int64.of_int n) n)) (n+1)
    in
    sumr Int64.zero 1
;;

let trunc = Big_int.big_int_of_int64 10000000000L;;
let mul a b = Big_int.(mod_big_int (mult_big_int a b) trunc);;
let add a b = Big_int.(mod_big_int (add_big_int a b) trunc);;

let sum max =
    let rec sumr acc n =
        if n > max then
            acc
        else
            sumr (add acc (pow mul Big_int.unit_big_int (Big_int.big_int_of_int n) n)) (n+1)
    in
    Big_int.string_of_big_int (sumr Big_int.zero_big_int 1)
;;(**)

sum   10;;
sum  100;;
sum 1000;;
