
let perm_digits digits n p i =
    let rec extract_digit digits = function
     | 0 -> (digits mod 10, digits/10)
     | i ->
        let cur = digits mod 10  in
        let (d,digits') = extract_digit (digits/10) (i-1)  in
        (d, 10*digits' + cur)
    in
    let rec perm_rec p i = match p with
     | 0 -> (0,digits)
     | _ ->
        let j = i / (n-p+1)
        and k = i mod (n-p+1)  in
        let (perm,leaving) = perm_rec (p-1) j  in
        let (d,leaving') = extract_digit leaving (n-p-k)  in
        (10*perm + d, leaving')
    in
    fst (perm_rec p i)
;;

let perm_all_digits digits n i = perm_digits digits n n i;;



let is_prime n =
    if (n <> 2) && (n mod 2 = 0) then
        false
    else
        let rec is_prime_rec d =
            if d*d > n then
                true
            else if n mod d = 0 then
                false
            else
                is_prime_rec (d+2)
        in
        is_prime_rec 3
;;

let enumerate () =
    (*let digits_set mask =
        let rec set_rec i = function
         | 0    -> 0
         | mask ->
            let s = set_rec (i+1) (mask/2)  in
            if mask mod 2 = 0 then
                s
            else
                10*s + i
        in set_rec 0 mask
    in*)
    let test digits =
        for i = 0 to 21 do
            let m = perm_digits digits 4 4 i  in
            if is_prime m then
                for j = i+1 to 22 do
                    let n = perm_digits digits 4 4 j  in
                    if is_prime n then
                        for k = j+1 to 23 do
                            let p = perm_digits digits 4 4 k  in
                            if (n < p) && (p-n = n-m) && (is_prime p) then
                                Format.printf "\r%4u, %4u, %4u\n" m n p
                        done
                done
        done
    in
    for n = 1000 to 9999 do
        test n
    done
;;

enumerate ();;
