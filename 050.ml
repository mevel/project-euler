let primes_sieve n =
  let sieve = Array.init n (fun i -> i mod 2 <> 0) in
  let i = ref 1 in
  while i := !i+2; !i < n do
    if sieve.(!i) then
      let j = ref !i in
      while j := !j+ !i; !j < n do
        sieve.(!j) <- false
      done
  done;
  sieve

let primes_sieve_and_list n =
  let sieve = primes_sieve n in
  let rec aux i =
    if i >= n then []
    else if sieve.(i) then i :: aux (i+2)
    else                        aux (i+2)
  in
  (sieve, 2 :: aux 3)

exception Break

let longest_consecutive_primes_sum n =
  let (sieve, primes_list) = primes_sieve_and_list n in
  let primes = Array.of_list primes_list in
  let nb_primes = Array.length primes in
  Printf.printf "There are %u primes below %u.\n" nb_primes n;
  let max_len = ref 1
  and max_sum = ref 2 in
  let sum = ref 0 in
  for i = 0 to nb_primes-1 do
    sum := 0;
    try
    for j = i to nb_primes-1 do
      sum := !sum + primes.(j);
      if !sum >= n then
        raise Break;
      if sieve.(!sum) && j-i+1 > !max_len then begin
        max_len := j-i+1;
        max_sum := !sum
      end
    done
    with Break -> ()
  done;
  Printf.printf "max length: %u, sum = %u\n" !max_len !max_sum

let () =
  longest_consecutive_primes_sum 1_000_000
