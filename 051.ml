module Multiset = struct
  type 'a t = ('a,int) Hashtbl.t
  let create size = Hashtbl.create size
  let add ms e =
    try
      let count = Hashtbl.find ms e in
      Hashtbl.replace ms e (count+1);
      count+1
    with Not_found ->
      Hashtbl.add ms e 1;
      1
end

let enumerate_parts f s =
  let n = String.length s in
  let s = Bytes.unsafe_of_string s in
  let rec  explore i c =
    if i = n then
      f @@ Bytes.to_string s
    else begin
      explore (i+1) c;
      let d = Bytes.get s i in
      if c = '\000' || d = c then begin
        Bytes.set s i '*';
        explore (i+1) d;
        Bytes.set s i d
      end
    end
  in
  explore 0 '\000'

let primes_sieve n =
  let sieve = Array.init n (fun i -> i mod 2 <> 0) in
  let i = ref 1 in
  while i := !i+2; !i < n do
    if sieve.(!i) then
      let j = ref !i in
      while j := !j+ !i; !j < n do
        sieve.(!j) <- false
      done
  done;
  sieve

exception Break of string

let pb51 k guess_max =
  let sieve = primes_sieve guess_max in
  let ms = Multiset.create (guess_max/10) in
  let f x =
    enumerate_parts
      (fun s ->
         (*Printf.printf "adding “%s”…" s;*)
         if let p = Multiset.add ms s in (*Printf.printf " %u" p;*) p = k then raise @@ Break s;
         (*Printf.printf ", %u\n" @@ Multiset.count ms s*)
      )
      (string_of_int x)
  in
  f 2;
  try
    for i = 1 to guess_max/2-1 do
      if sieve.(2*i+1) then
        f (2*i+1)
    done
  with Break s ->
    Printf.printf "family counting %u primes: %s\n" k s

let () =
  pb51 8 1_000_000
