let count_digits n =
  let count = Array.make 10 0 in
  let n = ref n in
  while !n > 0 do
    let d = !n mod 10 in
    count.(d) <- count.(d) + 1;
    n := !n / 10
  done;
  count

let () =
  let n = ref 9 in
  let continue = ref true in
  while !continue do
    let count = count_digits !n in
    if count = count_digits (2* !n)
    && count = count_digits (3* !n)
    && count = count_digits (4* !n)
    && count = count_digits (5* !n)
    && count = count_digits (6* !n) then
      continue := false
    else
      n := !n + 9
  done;
  Printf.printf "%u\n" !n
