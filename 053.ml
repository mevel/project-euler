let pb53 treshold nmax =
  let n = ref 0
  and k = ref 0
  and comb = ref 1 in
  let count = ref 0 in
  (* Les plus grandes valeurs se trouvent au centre du triangle de Pascal.
   * On reste donc au centre en descendant ligne par ligne (en incrémentant n)
   * tant qu’on n’a pas dépassé le seuil. *)
  while !comb <= treshold && !n <= nmax do
    if !n mod 2 = 0 then    (* On va « en bas à gauche » (k inchangé). *)
      comb := !comb * (!n+1) / (!n+1- !k)
    else begin              (* On va « en bas à droite » (k incrémenté). *)
      comb := !comb * (!n+1) / (!k+1);
      incr k
    end;
    incr n
  done;
  (* On continue d’augmenter n jusqu’à la valeur maximale, en comptant les
   * valeurs dépassant le seuil sur chaque ligne. *)
  while !n <= nmax do
    (* On va à la dernière valeur inférieure ou égale au seuil sur la ligne
     * courante (k = -1 si le seuil est < 0). Pour ça, on peut partir de la
     * position précédente et se décaler à gauche autant de fois que requis,
     * car les valeurs sont dans l’ordre croissant dans la moitié gauche du 
     * triangle. *)
    while !comb > treshold && !k >= 0 do
      comb := !comb * !k / (!n- !k+1);    (* On va « à gauche ». *)
      decr k
    done;
    (* Le nombre de valeurs supérieures au seuil est alors donné par n-2k-1,
     * en tenant compte de la symétrie du triangle. *)
    count := !count + !n - 2* !k - 1;
    (* On continue « en bas à gauche » (« en bas à droite » est forcément
     * supérieur au seuil). *)
    comb := !comb * (!n+1) / (!n+1- !k);
    incr n
  done;
  !count
 
let _ =
  let t = Sys.time () in
  Printf.printf "%u\n" (pb53 1_000_000 100);
  Printf.printf "%fs\n" (Sys.time () -. t)
