type color = C | H | S | D

type rank =
  | High_card      of unit
  | Pair           of int
  | Two_pairs      of int*int
  | Triple         of int
  | Straight       of unit
  | Flush          of unit
  | Full_house     of int
  | Quadruple      of int
  | Straight_flush of unit
  | Royal_flush    of unit

let search_from array from elt =
  let module Ret = struct exception Urn of int end in
  try
  for i = from to Array.length array - 1 do
    if array.(i) = elt then
      raise (Ret.Urn i)
  done;
  -1
  with Ret.Urn x -> x

let search_straight counts =
  let module Ret = struct exception Urn of bool end in
  try
  for i = 14 downto 4 do
    if counts.(i) = 1
    && counts.(i-1) = 1
    && counts.(i-2) = 1
    && counts.(i-3) = 1
    && counts.(i-4) = 1 then
      raise (Ret.Urn true)
  done;
  false
  with Ret.Urn x -> x

let hand_rank hand =
  let counts = Array.make 15 0 in
  let flush = ref true in
  let first_color = ref (snd hand.(0)) in
  Array.iter
   (fun (value,color) ->
     counts.(value) <- counts.(value) + 1;
     if color <> !first_color then
       flush := false
   )
   hand;
  let flush = !flush
  and straight = search_straight counts in
  if straight && flush then
    if counts.(14) = 1 then
      Royal_flush ()
    else
      Straight_flush ()
  else
    let quad =   search_from counts 0 4
    and triple = search_from counts 0 3
    and pair =   search_from counts 0 2 in
    let pair2 =  search_from counts (pair+1) 2 in
    if quad >= 0 then
      Quadruple quad
    else if triple >= 0 && pair >= 0 then
      Full_house triple
    else if flush then
      Flush ()
    else if straight then
      Straight ()
    else if triple >= 0 then
      Triple triple
    else if pair2 >= 0 then
      Two_pairs (pair2,pair)
    else if pair >= 0 then
      Pair pair
    else
      High_card ()

let complete_rank hand =
  let rank = hand_rank hand in
  let values = Array.map fst hand in
  Array.sort (fun x y -> y-x) values;
  (rank, values)

let read_card file =
(*
  Scanf.fscanf file "%c%c "
   (fun cvalue ccolor ->
     let value =
      match cvalue with
      | '2'..'9' -> Char.code cvalue - Char.code '0'
      | 'T' -> 10
      | 'J' -> 11
      | 'Q' -> 12
      | 'K' -> 13
      | 'A' -> 14
     and color =
      match ccolor with
      | 'C' -> C
      | 'H' -> H
      | 'S' -> S
      | 'D' -> D
     in
     (value,color)
   )
*)
  let cvalue = input_char file in
  let ccolor = input_char file in
  let _ = input_char file in
  (*Printf.printf "[%c][%c](%c)\n" cvalue ccolor foo;*)
  let value =
   match cvalue with
   | '2'..'9' -> Char.code cvalue - Char.code '0'
   | 'T' -> 10
   | 'J' -> 11
   | 'Q' -> 12
   | 'K' -> 13
   | 'A' -> 14
  and color =
   match ccolor with
   | 'C' -> C
   | 'H' -> H
   | 'S' -> S
   | 'D' -> D
  in
  (value,color)

let read_hand file =
  let hand = Array.make 5 (0,C) in
  for i = 0 to 4 do
    hand.(i) <- read_card file
  done;
  hand

let string_of_crank (rank,values) =
  let r = match rank with
   | High_card () -> "high card"
   | Pair i       -> "pair of " ^ string_of_int i
   | Two_pairs (i,j)  -> "two pairs of " ^ string_of_int i ^ " and " ^ string_of_int i
   | Triple i     -> "triple of " ^ string_of_int i
   | Straight ()  -> "straight"
   | Flush ()     -> "flush"
   | Full_house i -> "full house of " ^ string_of_int i
   | Quadruple i  -> "quadruple of " ^ string_of_int i
   | Straight_flush () -> "straight flush"
   | Royal_flush ()    -> "royal flush"
  and v = (Array.fold_left (fun s v -> s ^ " " ^ string_of_int v) "[" values) ^ " ]" in
  r ^ " " ^ v

let () =
  let count = ref 0 in
  try
  while true do
    let hand1 = read_hand stdin in
    let hand2 = read_hand stdin in
    let rank1 = complete_rank hand1
    and rank2 = complete_rank hand2 in
    Printf.printf "%s\t| %s\n" (string_of_crank rank1) (string_of_crank rank2);
    if rank1 > rank2 then
      incr count
  done
  with End_of_file ->
  Printf.printf "Player 1 wins %u times.\n" !count
