/* 
 *  Projet Euler  #55
 */
#include <stdio.h>
#include <stdbool.h>
#include "bignum.h"

#define  MAX  10000
#define  ITER_MAX  50



void  reverse
  (BigInt* res, const BigInt* n)
{
	BigInt tmp;
	unsigned digit;
	
	BigInt_set(&tmp, n);
	BigInt_zero(res);
	while(!BigInt_isZero(&tmp)) {
		digit = BigInt_div_(&tmp, &tmp, 10);
		BigInt_mul_(res, res, 10);
		BigInt_add_(res, res, digit);
	}
}



bool  is_lychrel_number
  (unsigned n)
{
	unsigned i;
	BigInt a, r;
	
	BigInt_set_(&a, n);
	reverse(&r, &a);
	
	for(i = 1;  i < ITER_MAX;  ++i) {
		BigInt_add(&a, &a, &r);
		reverse(&r, &a);
		if(BigInt_cmp(&a, &r) == 0)
			return false;
	}
	
	return true;
}



int main(void)
{
	unsigned count, n;
	
	count = 0;
	for(n = 0;  n <= MAX;  ++n)
		if(is_lychrel_number(n)) {
			/*printf("%i\n", n);*/
			++count;
		}
	
	printf("\nnumber of Lychrel numbers below %i: %i\n", MAX, count);
	
	return 0;
}



/*
unsigned  reverse
  (unsigned n)
{	
	unsigned res;
	
	res = 0;
	for(;  n;  n /= 10)
		res = res*10 +  n%10;
	
	return res;
}



bool  is_lychrel_number
  (unsigned n)
{
	unsigned i;
	unsigned r;
	
	r = reverse(n);
	for(i = 1;  i < ITER_MAX;  ++i) {
		n += r;
		r = reverse(n);
		if(n == r)
			return false;
	}
	
	return true;
}
*/
