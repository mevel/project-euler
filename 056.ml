let a_max = 99
let b_max = 99

open Big_int

let sum_of_digits big_n =
  let sum = ref 0 in
  String.iter (fun d -> sum := !sum + Char.code d - Char.code '0') (string_of_big_int big_n);
  !sum

let () =
  let max_sum = ref 0 in
  for a = 1 to 99 do
    let x = ref unit_big_int in
    for b = 1 to 99 do
      x := mult_int_big_int a !x;
      let sum = sum_of_digits !x in
      if sum > !max_sum then
        max_sum := sum
    done
  done;
  Printf.printf "maximal digits sum of a^b for a < %u, b < %u:\n%u\n" a_max b_max !max_sum
