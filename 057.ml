open Big_int

let continued_fraction seq =
  List.fold_right
   (fun d (a,b) -> (add_big_int (mult_int_big_int d a) b, a))
   seq
   (unit_big_int, zero_big_int)

let list_init n f =
  let rec aux i = if i = n then [] else f i :: aux (i+1)
  in aux 0

let make_seq_sqrt2 n =
  1 :: list_init (n-1) (fun _ -> 2)

let number_of_times_when_numerator_is_longer_than_denominator n =
  let count = ref 0 in
  for i = 1 to n do
    let (a,b) = continued_fraction (make_seq_sqrt2 i) in
    let (sa,sb) = (string_of_big_int a, string_of_big_int b) in
    if String.length sa > String.length sb then
      incr count
  done;
  !count

let () =
   number_of_times_when_numerator_is_longer_than_denominator 1000 |> Printf.printf "%u\n"
