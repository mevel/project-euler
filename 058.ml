let is_prime n =
  if n mod 2 = 0 then
    n = 2
  else begin
    let d = ref 3 in
    let found = ref false in
    while not !found && !d * !d <= n do
      if n mod !d = 0 then
        found := true
      else
        d := !d + 2
    done;
    not !found
  end

let side_of_first_square_spiral_under percentage =
  let side = ref 3
  and i = ref 9
  and count = ref 3 in
  while !count*100 > (!side*2-1)*percentage(* !side < 7*) do
    side := !side + 2;
    for k = 1 to 4 do
      i := !i + !side - 1;
      if is_prime !i then
        incr count
    done
    (*;Printf.printf "-- %i: %i\n" !side !count*)
  done;
  !side

let () =
  side_of_first_square_spiral_under 10 |> Printf.printf "%u\n"
