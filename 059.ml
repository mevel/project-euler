(*
 *  Project Euler #59
 *)

let read_integers file =
    let rec aux () =  try Scanf.fscanf file "%_[ \n,]%d" (fun x -> x :: aux ()) with End_of_file -> []
    in aux ()

let is_alphanum x = match Char.chr x with
  | 'A'..'Z' | 'a'..'z' | '0'..'9'
  | '\n' | ' ' | '.' | ',' | '-'
      -> true
  | _ -> false

let list_create f =
  let li = ref [] in
  let add = fun e -> li := e :: !li in
  f add;
  List.rev !li

let keys =
  let ascii_a = 97
  and ascii_z = 122 in
  list_create (fun add ->
     for i = ascii_a to ascii_z do
       for j = ascii_a to ascii_z do
         for k = ascii_a to ascii_z do
           add [|i;j;k|]
         done
       done
     done
   )

let probability key codes =
  let key_len = Array.length key in
  let count = ref 0 in
  Array.iteri (fun i x ->
     if is_alphanum @@ x lxor key.(i mod key_len) then
       incr count
   )
   codes;
  !count

let most_probable_key keys codes =
  List.fold_left (fun (max_key,max_p) key ->
     let p = probability key codes in
     if p > max_p then
       (key,p)
     else
       (max_key,max_p)
   )
   ([||],0)
   keys
  |> fst

let decipher key codes =
  let key_len = Array.length key in
  let msg = String.make (Array.length codes) '\000' in
  Array.iteri (fun i x -> msg.[i] <- Char.chr (x lxor key.(i mod key_len))) codes;
  msg

let ascii_sum str =
  BatString.fold_left (fun sum c -> sum + Char.code c) 0 str

let () =
  let codes = read_integers stdin |> Array.of_list in
  let key = most_probable_key keys codes in
  let msg = decipher key codes in
  let sum = ascii_sum msg in
  Printf.printf "key:";
  Array.iter (Printf.printf " %X") key;
  Printf.printf "\ntext:\n-----\n";
  print_endline msg;
  Printf.printf "-----\nsum: %u\n" sum
