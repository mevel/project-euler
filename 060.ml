type 'a graph = {
    label: 'a array;
    incidence: int array; (* -1 if not a node of the graph *)
    edges: int list array; (* neighbours sorted by ascending ID *)
}

let link graph i j =
  if graph.incidence.(i) >= 0 && graph.incidence.(j) >= 0 then begin
    graph.incidence.(i) <- graph.incidence.(i) + 1;
    graph.edges    .(i) <- j :: graph.edges.(i)
  end

let search_clique k graph =
  let module B = struct exception Reak end in
  let n = Array.length graph.incidence in
  let nodes = Array.make k (-1) in
  let rec is_connected k' i =
    k' = 0 || List.mem i graph.edges.(nodes.(k'-1)) && is_connected (k'-1) i
  in
  let rec explore k' i available edges =
    if graph.incidence.(i) >= k-k'-1 && is_connected k' i then begin
      nodes.(k') <- i;
      let k'' = k'+1 in
      if k'' = k then
        raise B.Reak;
      let edges = ref edges in
      for a = available downto k-k'' do (* inv: a = |edges| *)
        let j::e = !edges in
        edges := e;
        explore k'' j (a-1) e
      done
    end
  in
  try
    for i = 0 to n-1 do
      explore 0 i graph.incidence.(i) graph.edges.(i)
    done;
    None
  with B.Reak ->
    let nodes = Array.map (Array.get graph.label) nodes in
    Some nodes

let primes_graph n min_incidence =
  (* graph.incidence is the Erathostenes sieve: -1 if not prime, >=0 otherwise
   * (the exact value giving the incidence of the associated node). *)
  let graph = {
    label     = [||];
    incidence = Array.init n (fun i -> i mod 2 - 1);
    edges     = Array.make n [];
  } in
  graph.incidence.(1) <- -1;
  (* We do not regard 2 as being prime (useless for our goal). *)
  let i = ref 1 in
  while i := !i+2; !i < n do
    if graph.incidence.(!i) >= 0 then
      (*  We just found a new prime: let’s remove its multiples from the
       *  sieve… *)
      let j = ref !i in
      while j := !j+ !i; !j < n do
        graph.incidence.(!j) <- -1
      done;
      (* … and compute the edges it can produce between two smaller primes. *)
      let k = ref !i
      and l = ref 0
      and p = ref 1 in
      while l := !l + !p*(!k mod 10); k := !k/10; p := !p*10; !k <> 0 do
        if !l >= !p/10 then (* do not create an edge if the digit is 0 *)
          link graph !k !l
      done
  done;
  (* The computed graph is huge with many useless nodes; for a much more
   * lightweight memory footprint and ways more efficient treatment, we remove a
   * vast majority of useless nodes: numbers which are not prime or whose
   * incidence (number of edges) is inferior to some threshold. Since we no more
   * have the trivial mapping between numbers and node IDs, we need to keep
   * track of it; this is the role of arrays ‘mapping’ and ‘graph'.label’ (the
   * first one reuse the memory space allocated for ‘graph.incidence’; this is a
   * trick to avoid creating another heavy array that will for a while exist
   * in memory concurently with the two others).
   * Also, the graph we will build will contain only edges (i,j) for i<j, not
   * their symmetric. It will reduce the memory footprint and save some
   * operations during the symmetrisation of the graph and the lexicographic
   * searching of a clique. *)
  (* determine which nodes to keep (and the mapping between numbers and node
   * IDs): *)
  let mapping = graph.incidence
  and useful = ref []
  and count_useful = ref 0
  and i = ref 1 in
  while i := !i+2; !i < n do
    if graph.incidence.(!i) >= min_incidence then begin
      mapping.(!i) <- !count_useful;
      let edges = List.rev graph.edges.(!i) in
      useful := (!i, edges) :: !useful;
      incr count_useful
    end
    else
      mapping.(!i) <- -1
  done;
  (* build the new graph’s nodes: *)
  let (useful_label, useful_edges) = List.split @@ List.rev !useful in
  let graph' = {
    label     = Array.of_list useful_label;
    incidence = Array.make !count_useful 0;
    edges     = Array.of_list useful_edges;
  } in
  (* build the new edges (one list sorted per ascending ID for each node), keep
   * only the symmetric ones and remove the edges (i,j) with i>j: *)
  let edge_exists i' j' =
    let i = graph'.label.(i') in
    let rec aux = function
      | e::q when e <= i -> if e = i then (true,q) else aux q
      | li               -> (false,li)
    in
    j' >= 0 &&
      let (found,new_edges) = aux graph'.edges.(j') in
      graph'.edges.(j') <- new_edges;
      found
  in
  for i' = 0 to !count_useful-1 do
    ignore @@ edge_exists i' i'; (* suppress unmatched edges with lower nodes *)
    graph'.edges.(i') <-
      graph'.edges.(i')
      (*|> List.map    (Array.get mapping)    (* fails because map is not tail-rec, although filter is *)
      |> List.filter (edge_exists i');*)
      |> List.filter (fun j -> edge_exists i' mapping.(j))
      |> List.map    (Array.get mapping);
    graph'.incidence.(i') <- List.length graph'.edges.(i');
  done;
  graph'

let pb60 k guess_max =
  primes_graph guess_max (k-1)
  |> search_clique k
  |> function
     | None        -> Printf.printf "no result\n"
     | Some clique ->
         Printf.printf "found a %u-clique:" k;
         Array.iter (Printf.printf " %u") clique;
         Printf.printf "\n"

(* run on a 64-bit machine for sufficiently large arrays *)
let () = pb60 5 100_000_000
