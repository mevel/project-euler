(*
 *  Project Euler #61
 *)

let str = function
  | `Tri  -> "tri"
  | `Sqr  -> "sqr"
  | `Pent -> "pent"
  | `Hex  -> "hex"
  | `Hept -> "hept"
  | `Oct  -> "oct"

let gens =
  [ `Tri,  (fun n -> n*(n+1)/2);
    `Sqr,  (fun n -> n*n);
    `Pent, (fun n -> n*(3*n-1)/2);
    `Hex,  (fun n -> n*(2*n-1));
    `Hept, (fun n -> n*(5*n-3)/2);
    `Oct,  (fun n -> n*(3*n-2));
  ]

let list_init n f =
  let rec init i = if i = n then [] else f i :: init (i+1) in init 0

let lists =
  let filter x = 999 < x && x <= 9999 && x mod 100 > 9 && x mod 10 > 0 in
  List.map (fun (label,gen) -> label, List.filter filter @@ list_init 200 gen) gens

let nodes =
  let nodes = Array.make 100 [] in
  List.iter (fun (label,elts) ->
     List.iter (fun e ->
        let i = e/100 in nodes.(i) <- (label, e mod 100) :: nodes.(i)
      )
      elts
   )
   lists;
  nodes

let first    = List.hd
let last  li = List.nth li (List.length li - 1)

exception Fund

let rec run count labels res i =
  if count = 6 then begin
    if first res = last res then begin
      List.iter (print_endline % str) labels;
      List.iter (Printf.printf "%u\n") res;
      raise Fund
    end
  end else
    List.iter (fun (label,j) ->
      if not @@ List.mem label labels then
        run (count+1) (label::labels) (j::res) j
     )
    nodes.(i)

let () =
  Array.iteri (fun i elts ->
     if elts <> [] then
       run 0 [] [i] i
   )
   nodes

(* result:
 *  oct 1281
 *  hex   8128
 *  pent    2882
 *  tri       8256
 *  sqr         5625
 *  hept          2512
 *)
