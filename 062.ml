(*
 *  Project Euler #62
 *)

(* to run on a 64-bit machine *)

let list_init n f =
  let rec init i = if i = n then [] else f i :: init (i+1) in init 0

let cubes = list_init 10_000 (fun n -> n*n*n)

let masks = List.map (fun c ->
   let m = Array.make 10 0 in
   let x = ref c in
   while !x <> 0 do
     let d = !x mod 10 in
     m.(d) <- m.(d) + 1;
     x := !x / 10
   done;
   (m,c)
 )
 cubes

let count m max =
  let rec count_rec count = function
    (*| _ when count > max  -> count*)
    | []                  -> count
    | (m',_)::q when m=m' -> count_rec (count+1) q
    | _::q                -> count_rec count q
  in
  count_rec 0 masks

exception Fund

let () =
  List.iter (fun (m,c) ->
     if count m 5 = 5 then begin
       Printf.printf "%u\n" c;
       raise Fund
     end
   )
   masks
