(*
 *  Project Euler #64
 *)

(* In order to simplify the research of a cycle, emitted the conjecture that
 * the period always extends over the entire developpement but the first number
 * (the integral part); thus, we just have to compare the current (a,b) with
 * the initial one to detect a cycle.
 * Did not manage to prove that. *)

let continued_fraction_sqrt d =
  let r = (int_of_float % sqrt % float) d in
  if r*r = d then (r,[]) else
  (*let seen_ab = Array.make_matrix r d false in*)
  let (a0,b0) = (r,1) in
  let rec continued res a b =
    (*if seen_ab.(a).(b) then*)
    if res <> [] && (a,b) = (a0,b0) then
      (r, List.rev res)
    else
      let b' = (d - a*a) / b (* exact integer division *) in
      let x  = (a + r) / b'  (* quotient of euclidian division *) in
      let a' = b'*x - a in
      continued (x::res) a' b'
  in
  continued [] a0 b0

let () =
  let count = ref 0 in
  for d = 0 to 10_000 do
    let (r,cycle) = continued_fraction_sqrt d in
    if (List.length cycle) mod 2 = 1 then
      incr count
  done;
  Printf.printf "%u\n" !count
