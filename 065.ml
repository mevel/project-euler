open Big_int

let continued_fraction seq =
  List.fold_right
   (fun d (a,b) -> (add_big_int (mult_int_big_int d a) b, a))
   seq
   (unit_big_int, zero_big_int)

let list_init n f =
  let rec aux i = if i = n then [] else f i :: aux (i+1)
  in aux 0

let make_seq_e n =
  2 :: list_init (n-1) (fun i -> if i mod 3 = 1 then 2*(i/3+1) else 1)

let sum_of_digits_in_numerator n =
  let (a,b) = continued_fraction (make_seq_e n) in
  let (sa,sb) = (string_of_big_int a, string_of_big_int b) in
  Printf.printf "%uth convergent of the continued fraction for e:\n  %s / %s\n" n sa sb;
  let sum = ref 0 in
  String.iter (fun d -> sum := !sum + Char.code d - Char.code '0') sa;
  Printf.printf "sum of the digits of the numerator: %u\n" !sum

let () =
  sum_of_digits_in_numerator 100
