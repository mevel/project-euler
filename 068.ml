(*
 *  Project Euler #68
 *)

(*
 *         α.
 *           ·.        β
 *             ·.      :
 *              .a.    :
 *            .·   ·.  :
 *          .·       ·.:
 *        .e           b
 *      .· :           :
 *    .·   :           :
 *   ε     :           :
 *         :           :
 *         d···········c···········γ
 *         :
 *         :
 *         :
 *         :
 *         δ
 *
 *  représentation : « αab βbc γcd δde εea »
 *
 *  α, β, γ, δ, ε, a, b, c, d, e sont les entiers de 1 à 10 (tous distincts).
 *  l’unicité de la représentation signifie :
 *    α < β, γ, δ, ε                                                         (A)
 *  (en particulier, α ⩽ 6). la contrainte que la représentation fasse 16
 *  chiffres et non 17 signifie que 10 est l’une des variables β, γ, δ, ε.
 *
 *  soit s la somme de chaque ligne, et :
 *    s₁ = α + β + γ + δ + ε
 *    s₂ = a + b + c + d + e
 *  on a les équations suivantes :
 *    s = α + a + b                                                          (1)
 *    s = β + b + c                                                          (2)
 *    s = γ + c + d                                                          (3)
 *    s = δ + d + e                                                          (4)
 *    s = ε + e + a                                                          (5)
 *    55 = s₁ + s₂                                                           (6)
 *  d’où l’on déduit entre autres :
 *    5s = s₁ + 2s₂ = 55 + s₂
 *  (donc 5 ∣ s₁, s₂). de plus, 1 + 2 + 3 + 4 + 5 = 15 donc 15 ⩽ s₁, s₂ ; comme
 *  s₁ + s₂ = 55, on a en fait :
 *    15 ⩽ s₁, s₂ ⩽ 40
 *  d’où :
 *    s = 11 + s₂/5
 *    14 ⩽ s ⩽ 19                                                            (B)
 *
 *  une fois s fixé, les équations (1) à (6) forment un système linéaire de six
 *  équations indépendantes à dix variables ; on a donc quatre degrés de
 *  liberté. on choisit les variables libres α, a, β et γ (car ainsi on peut
 *  chercher directement la chaîne maximale pour l’ordre lexicographique), les
 *  autres variables s’en déduisent.
 *)

let numbers = Array.make 11 true

let try_available n f =
  if 1 <= n && n <= 10 && numbers.(n) then begin
    numbers.(n) <- false ;
    f n ;
    numbers.(n) <- true
  end

let try_all max min f =
  for n = max downto min do
    try_available n f
  done

let enumerate s =
  Printf.printf "s = %i\n%!" s ;
  try_all  6 1                       @@ fun a' ->
  try_all  9 1                       @@ fun a  ->
  try_available (s - a' - a)         @@ fun b  ->  if b <> 10 then
  try_all 10 (a'+1)                  @@ fun b' ->
  try_available (s - b' - b)         @@ fun c  ->  if c <> 10 then
  try_available (55 - 3*s - b' + a)  @@ fun d' ->  if d' > a' then
  try_all 10 (a'+1)                  @@ fun c' ->
  try_available (s - c' - c)         @@ fun d  ->  if d <> 10 then
  try_available (3*s - 55 + a' + c') @@ fun e  ->  if e <> 10 then
  try_available (s - a - e)          @@ fun e' ->  if e' > a' then
  begin
    Printf.printf
{|
	%i%i%i%i%i%i%i%i%i%i%i%i%i%i%i
	      %i.
	        ·.        %i
	          ·.      :
	           .%i.    :
	         .·   ·.  :
	       .·       ·.:
	     .%i           %i
	   .· :           :
	 .·   :           :
	%i     :           :
	      :           :
	      %i···········%i···········%i
	      :
	      :
	      :
	      :
	      %i

|}
      a' a b   b' b c   c' c d   d' d e   e' e a
      a' b' a e b e' d c c' d'
  end

let () =
  List.iter enumerate [ 14 ; 15 ; 16 ; 17 ; 18 ; 19 ]
