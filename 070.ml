let nmax = 10_000_000

let is_permutation m n =
  if (m - n) mod 9 <> 0 then (* quick check *)
    false
  else begin
    let s = string_of_int m
    and t = string_of_int n in
    if String.length s <> String.length t then
      false
    else begin
      begin try
        s |> String.iter begin fun digit ->
          t.[String.index t digit] <- '\000'
        end ;
        true
      with Not_found ->
        false
      end
    end
  end

let file = Legacy.open_in "data/eulerphi-under-141319297.data"

let () =
  let last_n = ref 0
  and n_min = ref 0
  and ratio_min = ref 2. in
  while !last_n < nmax do
    Scanf.fscanf file "φ(%i) = %i\n" begin fun n phi_n ->
      last_n := n ;
      if n <> 1 && is_permutation n phi_n then begin
        (*Printf.printf "φ(%i) = %i\n" n phi_n ;*)
        let ratio = float n /. float phi_n in
        if ratio < !ratio_min then begin
          ratio_min := ratio ;
          n_min := n
        end
      end
    end
  done ;
  Printf.printf "min : n = %i, ratio = %g\n" !n_min !ratio_min
