#!/bin/env ocaml

(*
 *  Project Euler #072
 *)

(* efficient code by “notfancy” https://projecteuler.net/thread=72;post=5578 *)

(* fixpoint combinator with memoization: *)
let rec memoized_fix (f : ('a -> 'b) -> ('a -> 'b)) : 'a -> 'b =
  let mem = Hashtbl.create 1 in
  let rec fx n =
    try Hashtbl.find mem n with Not_found ->
      let r = f fx n in
      Hashtbl.add mem n r ;
      r
  in
  fx

let length_of_farey_sequence = memoized_fix@@fun f n ->
  let s = ref 0. in
  for k = 2 to n do
    s := !s +. f (n / k)
  done ;
  0.5 *. float n *. float (n + 3) -. !s

let () =
  Printf.printf "%f\n" (length_of_farey_sequence 1_000_000 -. 2.)
