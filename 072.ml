#!/bin/env ocaml

(*
 *  Project Euler #72
 *)

let () =
  let file = open_in "data/eulerphi-under-1_000_000.data" in
  let sum = ref ~-1 in (* −1 compensates for φ(1) = 1, which must not be accounted for *)
  for i = 1 to 1_000_000 do
    Scanf.fscanf file "φ(%u) = %u " @@ fun i' phi_i ->
    assert (i' = i) ;
    sum := !sum + phi_i
  done ;
  close_in file ;
  Printf.printf "%u\n" !sum
