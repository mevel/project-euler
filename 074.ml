#!/bin/env ocaml

(*
 *  Project Euler #074
 *)

let facto =
  [| 1 ; 1 ; 2 ; 6 ; 24 ; 120 ; 720 ; 5_040 ; 40_320 ; 362_880 |]

let rec f n =
  assert (n >= 0) ;
  if n = 0 then
    0
  else
    facto.(n mod 10) + f (n / 10)

let () =
  assert (f 145 = 145) ;
  assert (f @@ f 871 = 871) ;
  assert (f @@ f 872 = 872) ;
  assert (f @@ f @@ f 169 = 169) ;
  ()

let () =
  (* [chain_length.(i)] will be the length of the chain that starts from [i].
   * Any number below imax = 2_200_000 has its image below imax (because
   * 2! + 6×9! < imax), so this is enough for computing the length of any chain
   * that starts below 1_000_000. *)
  let chain_length = Array.make 2_200_000 0 in
  (* Initializing with the only cycles: *)
  chain_length.(1) <- 1 ;
  chain_length.(2) <- 1 ;
  chain_length.(145) <- 1 ;
  chain_length.(40_585) <- 1 ;
  chain_length.(871) <- 2 ; chain_length.(45_361) <- 2 ;
  chain_length.(872) <- 2 ; chain_length.(45_362) <- 2 ;
  chain_length.(169) <- 3 ; chain_length.(363_601) <- 3 ; chain_length.(1_454) <- 3 ;
  (* Then, computing with dynamic programming / memoization: *)
  let rec get i =
    let len = chain_length.(i) in
    if len = 0 then begin
      let len = 1 + get (f i) in
      chain_length.(i) <- len ;
      len
    end else
      len
  in
  let count = ref 0 in
  for i = 1 to 1_000_000 do
    if get i = 60 then
      incr count
  done ;
  assert (chain_length.(540) = 2) ;
  assert (chain_length.(78) = 4) ;
  assert (chain_length.(69) = 5) ;
  assert (
    chain_length |> Array.mapi (fun i x -> i,x) |> Array.to_list
      |> List.filter (fun (_,x) -> x = 1) |> List.map fst
    = [ 1 ; 2 ; 145 ; 40_585 ]
  ) ;
  Printf.printf "%u\n" !count
