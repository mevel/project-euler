#!/usr/bin/env ocaml

(*
 *  Project Euler #075
 *)

let rec gcd a b =
  if b = 0 then
    abs a
  else
    gcd b (a mod b)

exception Break

(* We simply enumerate the pythagorean triples as follows: any pythagorean
 * triple is of the form (k·a, k·b, k·c) where k ≥ 1 and (a, b, c) is
 * a primitive pythagorean triple.
 *
 * For any primitive triple (a, b, c), either a is odd and b is even, or the
 * converse. We break symmetry by imposing that a be odd.
 *
 * The primitive pythagorean triples (a, b, c) where a is odd are uniquely
 * obtained from 2 parameters (d, e) such that:
 *
 *     . 0 < d < e
 *     . d and e are odd
 *     . d and e are coprime
 *
 * by the following equation:
 *
 *     . a = d·e
 *     . b = (e² − d²) / 2
 *     . c = (e² + d²) / 2
 *
 * Then:  a + b + c = (d + e) · e
 *
 *)

let pe75 lmax =
  let counts = Array.make (lmax + 1) 0 in
  begin try for d' = 0 to max_int do
    if 4 * (d' + 1) * (2*d' + 3) > lmax then raise Break ;
    begin try for e' = d' + 1 to max_int do
      let l = 2 * (d' + e' + 1) * (2*e' + 1) in
      if l > lmax then raise Break ;
      if gcd (2*e' + 1) (2*d' + 1) = 1 then begin
        begin try for k = 1 to max_int do
          let kl = k * l in
          if kl > lmax then raise Break ;
          counts.(kl) <- counts.(kl) + 1 ;
        done with Break -> () end ;
      end ;
    done with Break -> () end ;
  done with Break -> () end ;
  if lmax >= 48 then begin
    assert (counts.(12) = 1) ;
    assert (counts.(24) = 1) ;
    assert (counts.(30) = 1) ;
    assert (counts.(36) = 1) ;
    assert (counts.(40) = 1) ;
    assert (counts.(48) = 1) ;
  end ;
  if lmax >= 120 then begin
    assert (counts.(120) = 3) ;
  end ;
  let count_unique = ref 0 in
  for l = 1 to lmax do
    if counts.(l) = 1 then incr count_unique ;
  done ;
  !count_unique

let () =
  Printf.printf "%i\n%!" (pe75 120) ;
  Printf.printf "%i\n%!" (pe75 1_500_000) ;
