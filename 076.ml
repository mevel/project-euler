#!/bin/env ocaml

(*
 *  Project Euler #76
 *)

(* NOTE: This code can be at the same time generalized and improved (reduce
 * space from O(n_max²) to O(n_max)), see code for #77.
 *
 * The non-generalized version can be improved even further (reduce time from
 * O(n_max²) to O(n_max^1.5)), see code for #78. *)

(* Returns a square matrix [mat] such that [mat.(n).(p)] is the number of
 * partitions of the integer [n] in parts of value at most [p]. *)
let build_mat ~n_max =
  let mat = Array.init (n_max+1) (fun n -> Array.make (n+1) 0) in
  let get n p =
    mat.(n).(min n p)
  in
  mat.(0).(0) <- 1 ;
  for n = 1 to n_max do
    mat.(n).(0) <- 0 ;
    for p = 1 to n do
      mat.(n).(p) <- mat.(n).(p-1) + get (n-p) p
    done
  done ;
  get

let number_of_partitions n =
  build_mat ~n_max:n n n

let number_of_strict_partitions n =
  number_of_partitions n - 1

let () =
  Printf.printf "%u\n" (number_of_strict_partitions 1000)
