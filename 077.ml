#!/usr/bin/env ocaml

(*
 *  Project Euler #77
 *)

(* Returns a map (as a function) which, for each integer in [0; n_max], gives
 * the number of ways of partitioning [n] into a sum whose all terms are in the
 * set [terms]. The terms are only required to be distinct positive integers.
 *
 * Complexity:
 * - time O(n_max × |terms|), then reading the map is constant time
 * - space O(n_max), which lives as long as the map lives
 *
 * Dynamic programming: we compute a matrix [mat] such that [mat.(i).(n)] is the
 * number of ways of partitioning [n] into a sum whose all terms are in
 * [{ terms.(0) ; terms.(1) ; … ; terms.(i-1) }]. Computing row [i] only depends
 * on row [i-1] and values earlier in row [i], hence we optimize space usage by
 * storing only the current row instead of the entire matrix.
 *)
let generalized_number_of_partitions ~terms ~n_max =
  assert (n_max >= 0) ;
  assert (ArrayLabels.for_all terms ~f:(fun t -> t > 0)) ;
  let count = Array.make (n_max+1) 0 in
  count.(0) <- 1 ;
  ArrayLabels.iter terms ~f:begin fun t ->
    for n = t to n_max do
      count.(n) <- count.(n) + count.(n-t) ;
    done ;
  end ;
  Array.get count

let n_max = 100

let primes_under_100 =
  [|  2 ;  3 ;  5 ;  7 ; 11 ; 13 ; 17 ; 19 ; 23 ; 29 ; 31 ; 37 ; 41 ; 43 ; 47 ;
     53 ; 59 ; 61 ; 67 ; 71 ; 73 ; 79 ; 83 ; 89 ; 97 |]

exception Break

let () =
  let p = generalized_number_of_partitions ~terms:primes_under_100 ~n_max in
  assert (p 10 = 5) ;
  begin try for n = 0 to n_max do
    if p n >= 5_000 then begin
      Printf.printf "%u can be written as %u different sums of primes\n"
        n (p n) ;
      raise Break
    end
  done with Break -> () end
