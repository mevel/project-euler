#!/usr/bin/env ocaml

(*
 *  Project Euler #78
 *)

(* load findlib (to be able to #require libraries) *)
try Topdirs.dir_directory (Sys.getenv "OCAML_TOPLEVEL_PATH") with Not_found -> () ;;
#use "topfind" ;;

#require "zarith" ;;

(* short name to put [int] operators back in scope when [Z] is in scope: *)
module I = Stdlib

(* use this operator as [a +- s @@ b];
 * the middle operand is the sign of the plus-or-minus operation: *)
let (+-) a s b =
  if s >= 0 then Z.(a + b) else Z.(a - b)

(* Returns a map (as a function) which, for each integer in [0; n_max], gives
 * the number of ways of partitioning [n] into a sum.
 *
 * Complexity:
 * - time O(n_max × √n_max), then reading the map is constant time
 * - space O(n_max), which lives as long as the map lives
 *
 * We might have re-used function [generalized_number_of_partitions] from PE #77
 * with the set { 1 ; 2 ; … ; n_max } as terms. However, that would have taken
 * time O(n_max²). By contrast, in the special case when the allowed terms are
 * all the integers: we can improve the time complexity by using a recurrence
 * relation due to Euler. See:
 *     https://mathworld.wolfram.com/PartitionFunctionP.html
 *
 * We use arbitrary-precision numbers for the result because it grows fast.
 * Note, that for the sake of answering the problem, we could more simply have
 * computed modulo 1_000_000, which would have been faster.
 *)
let number_of_partitions ~n_max =
  let count = Array.make (n_max+1) Z.zero in
  count.(0) <- Z.one ;
  for n = 1 to n_max do
    let k = ref 1 in
    let s = ref 1 in
    let mk' = ref (n - 2) in
    (* invariant:
     *   s = −(−1)^k
     *   mk  = n − k × (3k−1) / 2
     *   mk' = n − k × (3k+1) / 2
     * (n−mk and n−mk' are the generalized pentagon numbers in ascending order) *)
    while !mk' >= 0 do
      let mk  = !mk' + !k in
      count.(n) <- Z.(count.(n) +- !s @@ (count.(mk) + count.(!mk')));
      incr k ;
      s := ~- !s ;
      mk' := !mk' - 3* !k + 1 ;
    done ;
    let mk = !mk' + !k in
    if mk >= 0 then
      count.(n) <- Z.(count.(n) +- !s @@ count.(mk))
  done ;
  Array.get count

exception Break

let n_max = 100_000
let modulus = Z.of_int 1_000_000

let () =
  let p = number_of_partitions ~n_max in
  assert (p 4 = Z.of_int 5) ;
  assert (p 5 = Z.of_int 7) ;
  begin try for n = 0 to n_max do
    if Z.divisible (p n) modulus then begin
      Format.printf "%u can be written as %a different sums\n"
        n Z.pp_print (p n) ;
      raise Break
    end
  done with Break -> () end

(* Earlier attempt at solving the problem (this is exactly what we get by using
 * [generalized_number_of_partitions]):
 *
 * define p(n) as the number of partitions of the natural integer n.
 * define p(n,k) as the number of partitions of n in terms of value at most k,
 * so that p(n) = p(n,n).
 *
 * we can compute p(n,k) in time O(n×k) and space O(n) with these equations:
 *     p(0, 0)  =  1
 *     p(n, 0)  =  0    if n > 0
 *     p(n, k)  =  p(n, k−1)  =  …  =  p(n, n)    if n < k
 *     p(n, k)  =  p(n, k−1) + p(n−k, k)          if n ⩾ k
 * the two last equations are subsumed by this one:
 *     p(n, k)  =  ∑_{i = 0 to n ÷ k} p(n−i×k, k−1)
 * some additional equations derivable from those above:
 *     p(0, k)  =  1
 *     p(n, 1)  =  1
 *     p(n, 2)  =  n ÷ 2 + 1
 *
 * first values of p(n,k):
 *
 *      → n
 *     ↓     1  1  2  3  4  5  6  7
 *     k  +------------------------
 *      0 |  1  0  0  0  0  0  0  0
 *      1 |  :  1  1  1  1  1  1  1
 *      2 |  :  :  2  2  3  3  4  4
 *      3 |  :  :  :  3  4  5  7  9
 *      4 |  :  :  :  :  5  6  9 11
 *      5 |  :  :  :  :  :  7 10 13
 *      6 |  :  :  :  :  :  : 11 14
 *      7 |  :  :  :  :  :  :  : 15
 *
 * p(n) is read on the diagonal.
 *)
