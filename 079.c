/*
 *  Project Euler  #79
 */
#include <stdbool.h>
#include <stdio.h>


bool  satisfies
  (unsigned code, unsigned entry)
{
	while(entry) {
		if(!code)
			return false;
		if(code%10 == entry%10)
			entry /= 10;
		code /= 10;
	}

	return true;
}



bool  satisfies_all
  (unsigned code, const unsigned entries[], size_t n)
{
	for(size_t i = 0;  i < n;  i++)
		if(!satisfies(code, entries[i]))
			return false;
	return true;
	
}



#define  N  ( sizeof(entries)/sizeof(*entries) )

const unsigned entries[] = {
  #include "079-keylog"
};



int main(void)
{
	unsigned code;

	for(code = 0;  !satisfies_all(code, entries, N);  code++);
	
	printf("Smallest code which satisfies all the %u entries: %u.\n", N, code);
	return 0;
}
