#!/usr/bin/env ocaml

(*
 *  Project Euler #80
 *)

(* load findlib (to be able to #require libraries) *)
try Topdirs.dir_directory (Sys.getenv "OCAML_TOPLEVEL_PATH") with Not_found -> () ;;
#use "topfind" ;;

#require "zarith" ;;

(* Returns the sum of the first decimal digits (here the “decimals” include the
 * integral part as well as the fractional part) of the square root of [n]. *)
let sum_of_first_decimals_of_sqrt ~nb_decimals =
  let ten = Z.of_int 10 in
  let m = Z.pow ten nb_decimals in
  let p = Z.(m * m) in
fun n ->
  let (root, r) = Z.sqrt_rem Z.(Z.of_int n * p) in
  (* shortcut for perfect squares (superfluous optimization): *)
  if r = Z.zero then
    0
  else begin
    (* throw digits beyond the required precision: *)
    let decimals = ref root in
    while !decimals >= m do
      decimals := Z.ediv !decimals ten
    done ;
    (* sum the remaining digits: *)
    let sum = ref 0 in
    for _ = 1 to nb_decimals do
      let (q, r) = Z.ediv_rem !decimals ten in
      decimals := q ;
      sum := !sum + Z.to_int r ;
    done ;
    !sum
  end

let nb_decimals = 100
let n_max = 100

let () =
  let f = sum_of_first_decimals_of_sqrt ~nb_decimals in
  assert (f 2 = 475) ;
  let sum = ref 0 in
  for n = 0 to n_max - 1 do
    sum := !sum + f n
  done ;
  Printf.printf "%u\n" !sum ;
