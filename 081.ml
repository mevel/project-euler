let minimal_path_sum matrix =
  let n = Array.length matrix in
  let sums = Array.make_matrix n n 0 in
  sums.(0).(0) <- matrix.(0).(0);
  for j = 1 to n-1 do
    sums.(0).(j) <- matrix.(0).(j) + sums.(0).(j-1)
  done;
  for i = 1 to n-1 do
    sums.(i).(0) <- matrix.(i).(0) + sums.(i-1).(0)
  done;
  for i = 1 to n-1 do
    for j = 1 to n-1 do
        sums.(i).(j) <- matrix.(i).(j) + min sums.(i-1).(j) sums.(i).(j-1)
    done
  done;
  sums.(n-1).(n-1)
