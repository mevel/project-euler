#!/usr/bin/env ocaml

(**
 **  Project Euler #84
 **)

(*
 * General-purpose utilities
 *)

let array_findi : 'a. ('a -> bool) -> 'a array -> int =
  let rec findi f ar i n =
    if i < n then
      if f ar.(i) then i
      else findi f ar (i+1) n
    else raise Not_found
  in
  fun f ar -> findi f ar 0 (Array.length ar)

let array_find_alli : 'a. ('a -> bool) -> 'a array -> int list =
  let rec findi f ar i n acc =
    if i < n then
      if f ar.(i) then findi f ar (i+1) n (i :: acc)
      else findi f ar (i+1) n acc
    else List.rev acc
  in
  fun f ar -> findi f ar 0 (Array.length ar) []

let string_starts_with ?(from : int = 0) (s : string) (sub : string) : bool =
  let len_s = String.length s in
  let len_sub = String.length sub in
  if from < 0 || len_s < from then
    raise @@ Invalid_argument "string_starts_with" ;
  if len_s < from + len_sub then
    false
  else begin
    let exception Break in
    begin try
      for i = 0 to len_sub - 1 do
        if s.[from + i] <> sub.[i] then
          raise Break
      done ;
      true
    with Break ->
      false
    end
  end

let array_shuffle : 'a. 'a array -> unit =
  fun ar ->
    for i = Array.length ar - 1 downto 1 do
      let j = Random.int i in (* FIXME: use [Random.full_int] starting from OCaml 4.13 *)
      let x = ar.(i) in
      ar.(i) <- ar.(j) ;
      ar.(j) <- x ;
    done
let list_shuffle : 'a. 'a list -> 'a list =
  fun xs ->
    let ar = Array.of_list xs in
    array_shuffle ar ;
    Array.to_list ar

let rec list_take : 'a. int -> 'a list -> 'a list =
  fun n xs ->
    begin match n, xs with
    | n, x :: xs' when n > 0 -> x :: list_take (n-1) xs'
    | _                      -> []
    end

(*
 * Specific code
 *)

let board =
  [|
    "GO"; "A1"; "CC1"; "A2"; "T1"; "R1"; "B1"; "CH1"; "B2"; "B3";
    "JAIL"; "C1"; "U1"; "C2"; "C3"; "R2"; "D1"; "CC2"; "D2"; "D3";
    "FP"; "E1"; "CH2"; "E2"; "E3"; "R3"; "F1"; "F2"; "U2"; "F3";
    "G2J"; "G1"; "G2"; "CC3"; "G3"; "R4"; "CH3"; "H1"; "T2"; "H2";
  |]

let board_size = Array.length board

let pos_of_name name =
  array_findi ((=) name) board

type movement =
  | Abs of int
  | Rel of int
  | Next of int list

let abs name =
  Abs (pos_of_name name)
let rel k =
  Rel k
let next kind =
  Next (array_find_alli (fun name -> string_starts_with name kind) board)

let cc_cards =
  abs "GO" :: abs "JAIL" :: List.init (16-2) (fun _ -> rel 0)
let ch_cards =
  abs "GO" :: abs "JAIL" :: abs "C1" :: abs "E3" :: abs "H2" :: abs "R1"
  :: next "R" :: next "R" :: next "U" :: rel ~-3
  :: List.init (16-10) (fun _ -> rel 0)

let find_next =
  let rec find_next ~first js i =
    begin match js with
    | j::js' -> if i < j then j else find_next ~first js' i
    | []     -> first
    end
  in
  fun js i -> find_next ~first:(List.hd js) js i

let move m i =
  begin match m with
  | Abs j   -> j
  | Rel k   -> (i+k) mod board_size
  | Next js -> find_next js i
  end

let pp_movement outc m =
  begin match m with
  | Abs j            -> Printf.fprintf outc "go to %i (%s)" j board.(j)
  | Rel 0            -> Printf.fprintf outc "-"
  | Rel k when k > 0 -> Printf.fprintf outc "advance by %i squares" k
  | Rel k            -> Printf.fprintf outc "go back %i squares" ~-k
  | Next xs          -> Printf.fprintf outc "go to next among %i, ..." (List.hd xs) ;
  end

let jail = pos_of_name "JAIL"
let g2j = pos_of_name "G2J"
let cc3 = pos_of_name "CC3"

let run ~pos_counts ~die ~turns =
  let position = ref 0 in
  let past_doubles = ref 0 in
  let cc_cards = Array.of_list cc_cards in
  let ch_cards = Array.of_list ch_cards in
  array_shuffle cc_cards ;
  array_shuffle ch_cards ;
  let cur_cc_card = ref 0 in
  let cur_ch_card = ref 0 in
  assert (Array.length cc_cards = 16 && Array.length ch_cards = 16) ;
  (*! Printf.eprintf "pos = %i (%s)\n" !position board.(!position) ; !*)
  for _ = 1 to turns do
    (* draw two dice: *)
    let d1 = die ()
    and d2 = die () in
    (*! Printf.eprintf "  dice = %i+%i\n" d1 d2 ; !*)
    (* apply the rule for doubles: *)
    if !past_doubles = 2 && d1 = d2 then begin
      (*! Printf.eprintf "  go to jail!\n" ; !*)
      position := jail ;
    end
    else begin
      if d1 = d2 then incr past_doubles else past_doubles := 0 ;
      (* advance regularly as given by the dice: *)
      let j = move (Rel (d1 + d2)) !position in
      (* apply the rules for Go-To-Jail, Community Cards and CHance Cards:
       * NOTE: the only possible combo between these is:
       *   - move onto CH3;
       *   - draw the chance card “go back 3 squares”, moving onto CC3. *)
      if j = g2j then begin
        position := jail ;
      end
      else if string_starts_with board.(j) "CC" then begin
        (*! Printf.eprintf "  CC: %a\n" pp_movement cc_cards.(!cur_cc_card land 0xF) ; !*)
        position := move cc_cards.(!cur_cc_card land 0xF) j ;
        incr cur_cc_card ;
      end
      else if string_starts_with board.(j) "CH" then begin
        (*! Printf.eprintf "  CH: %a\n" pp_movement ch_cards.(!cur_ch_card land 0xF) ; !*)
        position := move ch_cards.(!cur_ch_card land 0xF) j ;
        incr cur_ch_card ;
        if !position = cc3 then begin
          (*! Printf.eprintf "  *** CH3→CC3! rare move! ***\n" ; !*)
          (*! Printf.eprintf "  CC: %a\n" pp_movement cc_cards.(!cur_cc_card land 0xF) ; !*)
          position := move cc_cards.(!cur_cc_card land 0xF) j ;
          incr cur_cc_card ;
        end
      end
      else
        position := j ;
    end ;
    (*! Printf.eprintf "pos = %i (%s)\n" !position board.(!position) ; !*)
    pos_counts.(!position) <- pos_counts.(!position) + 1 ;
  done

let runs ~die ~turns ~games =
  let pos_counts = Array.make board_size 0 in
  for _ = 1 to games do
    run ~pos_counts ~die ~turns ;
  done ;
  pos_counts

let die4 () = 1 + Random.int 4
let die6 () = 1 + Random.int 6

let die = die4
let turns = 1_000_00
let games = 1_000

let () =
  Random.self_init () ;
  runs ~die ~turns ~games
  |>  Array.to_list
  |>  List.mapi (fun i count ->
        (100. *. float count /. float turns /. float games, i))
  |>  List.sort (Fun.flip compare)
  |>  list_take 6
  |>  List.iter begin fun (p, i) ->
        Printf.printf "  %.2f%%:  %02i (%s)\n" p i board.(i)
      end
