let count a b  = a * (a+1) * b * (b+1) / 4;;

let nearest goal =
    let min = ref goal  in
    let abmin = ref (0,0)  in
    let test a b =
        let d = abs (goal - (count a b)) in
        if d < !min then (
            min := d;
            abmin := (a,b);
        )
    in
    let stop = int_of_float (ceil ((sqrt (1.0 +. 8.0 *. ceil (sqrt
    (float_of_int goal))))
      -. 1.0) /. 2.0)  in
    let stopb = int_of_float (ceil ((sqrt (float_of_int (1 + 8*goal)) -. 1.0)
    /. 2.0))  in
    for a = 1 to stop do
        for b = 1 to stopb do
            let d = abs (goal - (count a b)) in
            if d < !min then (
                min := d;
                abmin := (a,b);
            )
        done
    done;
    !abmin
;;

nearest 2000000;;
