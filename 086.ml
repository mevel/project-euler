(*
 *  Project Euler #86
 *)

(*
 * soit un pavé de côtés a × b × c. par symétrie, on a trois chemins candidats,
 * chacun courant sur deux faces et coupant une arête :
 *   — un qui coupe une arête de longueur a (passe par des faces a×b et a×c),
 *   — un qui coupe une arête de longueur b (passe par des faces a×b et b×c),
 *   — un qui coupe une arête de longueur c (passe par des faces a×c et b×c).
 *
 *             a
 *     +-------------------+
 *    /               . · /|
 *   /           . ·     / | b
 *  +----------x--------+  |
 *  |        .·         |  |
 *  |      .·           |  +
 *  |    .·             | /
 *  |  .·               |/ c
 *  +-------------------+
 *
 * dans le premier cas (les autres sont analogues), la distance minimale est
 * √(a² + (b+c)²), elle est obtenue par une ligne droite quand on « déplie »
 * les faces a×b et a×c du pavé.
 *
 * le minimum de cette valeur quand on permute a, b et c est obtenu quand
 * b, c ⩽ a (ce qui se prouve directement en développant a² + (b+c)²).
 *
 * en notant s = b+c, il suffit donc d’énumérer 1 ⩽ a ⩽ m et 1 ⩽ s ⩽ 2a. pour
 * chaque (a, s) tel que a² + s² soit carré. le nombre de pavés correspondants
 * est le nombre de paires {b ; c} telles que 1 ⩽ b, c ⩾ a et b+c = s. ce nombre
 * vaut :
 *   si s ⩽ b : ceil[(s−1)/2]    = floor[s/2]
 *   si s > b : ceil[(2b−s+1)/2] = b − ceil[(s−1)/2]
 *)

let perfect_square x =
  let r = int_of_float @@ sqrt @@ float x in r*r = x

let solve nb_solutions =
  let count = ref 0
  and a = ref 1 in
  while !count < nb_solutions do
    incr a ;
    let a2 = !a * !a in
    for s = 2 to !a do
      if perfect_square (a2 + s * s) then
        count := !count + s asr 1
    done ;
    for s = succ !a to 2 * !a do
      if perfect_square (a2 + s * s) then
        count := !count + !a - pred s asr 1
    done ;
    (*Printf.printf "m = %i:  count = %i\n" !a !count*)
  done ;
  !a

let () =
  Printf.printf "%i\n" @@ solve 1_000_000
