(*
 *  Project Euler #87
 *)

let primes =
  let primes = Array.make 2464 0 in
  let rec is_prime ?(i = 0) n =
    let p = primes.(i) in
    if p = 0 || p * p > n then
      true
    else if n mod p = 0 then
      false
    else
      is_prime ~i:(succ i) n
  in
  primes.(0) <- 2 ;
  let count_primes = ref 1 in
  for n = 3 to 22_000 (* ~ √(50 000 000) *) do
    if is_prime n then begin
      primes.(!count_primes) <- n ;
      incr count_primes
    end
  done ;
  primes

let reached =
  let table = Hashtbl.create 2_000 in
  object
    method add n = Hashtbl.replace table n ()
    method count = Hashtbl.length table
  end

let () =
  let rec loop3 ?(k = 0) s =
    let r = primes.(k) in
    let s' = s + r * r in
    if s' < 50_000_000 then begin
      reached#add s' ;
      loop3 ~k:(succ k) s
    end
  in
  let rec loop2 ?(j = 0) s =
    let q = primes.(j) in
    let s' = s + q * q * q in
    if s' < 50_000_000 then begin
      loop3 s' ;
      loop2 ~j:(succ j) s
    end
  in
  let rec loop1 ?(i = 0) () =
    let p = primes.(i) in
    let s' = p * p * p * p in
    if s' < 50_000_000 then begin
      loop2 s' ;
      loop1 ~i:(succ i) ()
    end
  in
  loop1 () ;
  Printf.printf "%i\n" reached#count
