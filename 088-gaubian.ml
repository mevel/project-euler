(*
 *  Project Euler #88 — Guillaume Aubian’s solution
 *)

(* returns all the sequences with all terms at least x and whose product is at
 * most max_p ; a sequence is returned as a tuple (j,s,p) where j is the length,
 * s the sum and p the product. *)
let rec all_sequences x max_p =
   if x > max_p then
     [ 0,0,1 ]
   else
     List.map (fun (j,s,p) -> 1+j,x+s,x*p) (all_sequences x (max_p/x))
     @ all_sequences (x+1) max_p

(* given an ordered list of tuples (k,p), keeps only the first occurrence of each
 * k (that is, the one with p minimal). *)
let rec simplify ?(acc = []) = function
   | (k,_ as e)::(k',_)::q when k = k' -> simplify ~acc (e::q)
   | e::q                              -> simplify ~acc:(e::acc) q
   | []                                -> (*List.rev*) acc

(* given a sequence (j,s,p) where all terms are at least 2, we have s ⩽ p, so we
 * get a sequence (k,p,p) by adding p−s terms of value 1; thus k = j+p−s. *)
let () =
  let max = read_int () in
  all_sequences 2 (2*max)
  |> List.map (fun (j,s,p) -> j+p-s, p)
  |> List.sort compare
  |> simplify
  |> List.filter (fun (k,_) -> k <= max)
  |> List.map snd
  |> List.sort_uniq compare
  |> List.fold_left (+) ~-1
  |> print_int
