(*
 *  Project Euler #94
 *)

let rec ilog2 ?(k = 0) x =
  if x = 1 then
    k
  else
    ilog2 ~k:(succ k) (x / 2)

let minimal_product_sum_of_length =
  (* m is the minimum found so far;
   * xs is the list of terms (useless);
   * p is their product;
   * s is their sum;
   * j is the number of terms to add;
   * x is the minimal value of the terms to add.
   *)
  let rec explore ~m (*~xs*) ~p ~s ~j ~x =
    if j = 1 then begin
      let den = pred p in
      if den <> 0 && s mod den = 0 then
        (*let () = List.iter (Printf.printf " %i") @@ List.rev @@ s/den :: xs in
        let () = print_newline () in*)
        min m (s + s / den)
      else
        m
    end
    else if p >= s || s + x * j >= m (* || p * x**j >= m *) then
      m
    else begin
      explore
        ~m:(explore ~m (*~xs:(x :: xs)*) ~p:(p * x) ~s:(s + x) ~j:(pred j) ~x)
        (*~xs*) ~p ~s ~j ~x:(succ x)
    end
  in
  function
  | 1 -> 1
  | 2 -> 4
  | k ->
      (* for k ⩾ 3, we can show that there is at least i = k−1 − ilog2 (k−1)
       * terms of value 1, hence only j = 1 + ilog2 (k−1) terms to determine. *)
      let pred_k = pred k in
      let i = pred_k - ilog2 pred_k in
      explore ~m:(2 * k) (*~xs:(List.make i 1)*) ~p:1 ~s:i ~j:(k - i) ~x:1

let max = 12_000

module IntSet = struct
  include Set.Make (struct type t = int let compare = compare end)
  let add_left set x = add x set
  let fold_left f x0 set = fold (fun acc x -> f x acc) set x0
end

let () =
  Sequence.int_range ~start:2 ~stop:max
  |> Sequence.map minimal_product_sum_of_length
  |> Sequence.fold IntSet.add_left IntSet.empty
  |> IntSet.fold_left (+) 0
  |> Printf.printf "sum = %i\n"
