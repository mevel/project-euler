/* 
 *  Projet Euler  #55
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define  FILE_PATH  "089-roman"



static const unsigned values[7] = {1, 5, 10, 50, 100, 500, 1000};
static const char letters[] = "IVXLCDM";



unsigned  roman_to_int
  (unsigned* n, const char* s)
{
	unsigned i;
	const char* p;
	unsigned val, prev;
	
	*n = 0;
	prev = 1000;
	for(i = 0;  s[i]  &&  (p = strchr(letters, s[i])) != NULL;  ++i) {
		val = values[p-letters];
		*n += val;
		if(prev < val)
			*n -= 2*prev;
		prev = val;
	}
	
	return i;
}



size_t  write_roman_digit
  (char* s, size_t i, unsigned* n, unsigned p, char one, char five, char ten)
{
	unsigned d;
	
	d = *n/p;
	*n -= d*p;
	if(d == 9) {
		s[i++] = one;
		s[i++] = ten;
	}
	else if(d == 4) {
		s[i++] = one;
		s[i++] = five;
	}
	else {
		if(d >= 5) {
			s[i++] = five;
			d -= 5;
		}
		while(d--)
			s[i++] = one;
	}
	
	return i;
}



unsigned  int_to_roman
  (char* s, unsigned n)
{
	unsigned i;
	unsigned d;
	
	i = 0;
	
	/* Thousands digit */
	d = n/1000;
	n -= d*1000;
	while(d--)
		s[i++] = 'M';
	
	/* Other digits */
	i = write_roman_digit(s, i, &n, 100, 'C', 'D', 'M');
	i = write_roman_digit(s, i, &n, 10,  'X', 'L', 'C');
	i = write_roman_digit(s, i, &n, 1,   'I', 'V', 'X');
	
	s[i] = '\0';
	
	return i;
}



unsigned  read_roman_number
  (FILE* f)
{
	char buf[32];
	unsigned d;
	unsigned n;
	
	if(!fgets(buf, sizeof(buf), f))
		return 0;
	
	d = roman_to_int(&n, buf);
	d -= int_to_roman(buf, n);
	
	return d;
}



int main(void)
{
	FILE* f;
	unsigned d;
	
	f = fopen(FILE_PATH, "r");
	if(!f)
		perror(FILE_PATH);
	
	d = 0;
	while(!feof(f))
		d += read_roman_number(f);
	fclose(f);
	
	printf("number of characters that could be saved in file ‘%s’: %i\n",
	  FILE_PATH , d);
	
	return 0;
}
