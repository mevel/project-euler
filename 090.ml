(*
 *  Project Euler #90
 *)

let relatives =
  [|
    (*0*) [ 1 ; 4 ; 6 ] ;
    (*1*) [ 0 ; 6 ; 8 ] ;
    (*2*) [ 5 ] ;
    (*3*) [ 6 ] ;
    (*4*) [ 0 ; 6 ] ;
    (*5*) [ 2 ] ;
    (*6*) [ 0 ; 1 ; 3 ; 4 ] ;
    (*7*) [] ;
    (*8*) [ 1 ] ;
    (*9*) [] ;
  |]

let choice_order = [ 6 ; 0 ; 1 ; 8 ; 4 ; 3 ; 2 ; 5 ; 7 ; 9 ]

type dice = {
  digits : bool array ;
  mutable count_in : int ;
  mutable count_notin : int ;
}

let set_notin' dice d f =
  if not dice.digits.(d) && dice.count_notin < 4 then begin
    dice.count_notin <- dice.count_notin + 1 ;
    f () ;
    dice.count_notin <- dice.count_notin - 1
  end

let set_notin dice d f =
  if d = 6 then
    set_notin' dice 6 @@ fun () ->
    set_notin' dice 9 f
  else
    set_notin' dice d f

let set_in dice d f =
  if dice.digits.(d) then
    f ()
  else if dice.count_in < 6 then begin
    dice.digits.(d) <- true ;
    dice.count_in <- dice.count_in + 1 ;
    f () ;
    dice.digits.(d) <- false ;
    dice.count_in <- dice.count_in - 1
  end

let rec set_all_in dice li f =
  begin match li with
  | []       -> f ()
  | d :: li' -> set_in dice d (fun () -> set_all_in dice li' f)
  end

let rec explore dice_a dice_b choice_digits f =
  if dice_a.count_notin + dice_a.count_in = 10 then
    f ()
  else begin
    begin match choice_digits with
    | []                  -> ()
    | d :: choice_digits' ->
        if dice_a.digits.(d) then
          explore dice_a dice_b choice_digits' f
        else begin
          begin
            set_in dice_a d @@ fun () ->
            explore dice_a dice_b choice_digits' f
          end ;
          if relatives.(d) = [] then begin
            set_notin dice_a d @@ fun () ->
            explore dice_a dice_b choice_digits' f
          end
          else begin
            set_notin  dice_a d             @@ fun () ->
            set_in     dice_b d             @@ fun () ->
            set_all_in dice_a relatives.(d) @@ fun () ->
            explore dice_a dice_b choice_digits' f
          end
        end
    end
  end

let count_possible_dices dice =
  if dice.digits.(6) = dice.digits.(9) then
    1
  else
    2

let () =
  let dice_a = { digits = Array.make 10 false ; count_in = 0 ; count_notin = 0 }
  and dice_b = { digits = Array.make 10 false ; count_in = 0 ; count_notin = 0 }
  and count = ref 0 in
  begin
    explore dice_a dice_b choice_order @@ fun () ->
    explore dice_b dice_a choice_order @@ fun () ->
    count := !count + count_possible_dices dice_a * count_possible_dices dice_b
  end ;
  Printf.printf "%i\n" @@ !count / 2
