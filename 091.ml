(*
 *  Project Euler #68
 *)

let count n =
  (* triangles OPQ directs (et non plats) rectangles en P(x,y) : *)
  let count = ref 0 in
  for x = 0 to n do
    for y = 0 to n do
      for x' = 0 to n do
        for y' = 0 to n do
          if (*x * y' > x' * y &&*) x * (x' - x) + y * (y' - y) = 0 then
            incr count
        done
      done
    done
  done ;
  (* on multiplie par 2 pour avoir aussi les triangles indirects, et on ajoute
   * n² pour avoir aussi les triangles rectangles en O (tous ceux de la forme
   * OPQ avec P(x,0) et Q(0,y')) : *)
  2 * !count + n * n

let () =
  count 50
  |> Printf.printf "%i\n"
