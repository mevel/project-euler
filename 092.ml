let nmax = 10_000_000

(* Donne le prochain nombre de la séquence (somme des carrés des chiffres de n). *)
let next n =
  let sum = ref 0
  and n = ref n in
  while !n <> 0 do
    let d = !n mod 10 in
    sum := !sum + d * d;
    n := !n / 10
  done;
  !sum

(* Mémoïsation : pour un entier n, mem.(n) vaut “Some b” avec b un booléen indiquant
 * si n conduit à 89, ou alors “None” si on ne le sait pas encore. *)
let mem_max = max 90 (min (nmax+1) Sys.max_array_length)
let mem = Array.make mem_max None
let _ = mem.(1) <- Some false
and _ = mem.(89) <- Some true

(* Donne un booléen disant si n conduit à 89. Mémoïse les résultats intermédiaires.
 *)
let rec seq n =
  if n <= mem_max-1 then match mem.(n) with
  | None    -> let b = seq (next n) in mem.(n) <- Some b; b
  | Some b  -> b
  else seq (next n)

(* Compte le nombre d’entiers entre 1 et nmax qui conduisent à 89. *)
let _ =
  let count = ref 0 in
  for n = 1 to nmax do
    if seq n then incr count
  done;
  Printf.printf "%u\n" !count
