(*
 *  Project Euler #93
 *)

(* require zarith *)

(*
 * generic iterators
 *)

let enumerate_sublists =
  let rec enumerate acc s = function
    | []    -> s :: acc
    | x::tl ->
        enumerate (enumerate acc (x::s) tl) s tl
  in
  fun li -> enumerate [] [] @@ List.rev li

let enumerate_sublists_of_length =
  let rec enumerate acc s = function
    | 0 -> fun _ -> s :: acc
    | m ->
        function
        | []    -> acc
        | x::tl -> enumerate (enumerate acc (x::s) (pred m) tl) s m tl
  in
  fun m li -> enumerate [] [] m @@ List.rev li

(*
let enumerate_list_partitions li =
  let sublists = enumerate_sublists li in
  List.combine sublists (List.rev sublists)
*)

let enumerate_strict_list_partitions li =
  let sublists = List.tl @@ List.rev @@ List.tl @@ enumerate_sublists li in
  List.combine sublists (List.rev sublists)

(*
let enumerate_strict_symetric_list_partitions =
  let rec enumerate acc s1 s2 = function
    | []    -> (s1, s2) :: acc
    | x::tl ->
        enumerate (enumerate acc (x::s1) s2 tl) s1 (x::s2) tl
  in
  (* all partitions: *)
  (*fun li -> enumerate [] [] [] @@ List.rev li*)
  (* strict partitions, modulo symetry ((X,Y) ≡ (Y,X)): *)
  fun li -> match List.rev li with
  | []     -> []
  | x0::tl -> List.tl @@ enumerate [] [] [x0] tl
*)

(*
 * set of values reachable from a given set of digits
 *)

module ReachableSet = Set.Make (Q)

let rec get_reachable_set =
  let reachable_sets = Hashtbl.create 16 in
  fun digits ->
    begin try
      Hashtbl.find reachable_sets digits
    with Not_found ->
      let set =
        begin match digits with
        | []     -> ReachableSet.empty
        | [d]    -> ReachableSet.singleton d
        | digits ->
            List.fold_right begin fun (digits1, digits2) ->
              ReachableSet.fold begin fun r1 ->
                ReachableSet.fold begin fun r2 ->
                  List.fold_right begin fun op ->
                    ReachableSet.add (op r1 r2)
                  end
                    Q.[ (+) ; (-) ; ( * ) ; (/) ]
                end
                  (get_reachable_set digits2)
              end
                (get_reachable_set digits1)
            end
              (enumerate_strict_list_partitions digits)
              ReachableSet.empty
        end
      in
      Hashtbl.add reachable_sets digits set ;
      set
    end

(*
 * main program
 *)

let sequence_length =
  let rec seq_len m = function
    | n::tl when n = Z.succ m -> seq_len n tl
    | _                       -> m
  in
  seq_len Z.zero

let pp_digits output =
  List.iter (Q.output output)

let () =
  let max_digits, max_len =
    List.fold_left begin fun (max_digits, max_len) digits ->
      digits
      |> get_reachable_set
      |> ReachableSet.filter (fun q -> Z.equal Z.one (Q.den q) && Q.lt Q.zero q)
      |> ReachableSet.elements
      |> List.map Q.to_bigint
      |> sequence_length
      |> fun len ->
        if Z.lt max_len len then
          (digits, len)
        else
          (max_digits, max_len)
    end
      ([], Z.zero)
      (enumerate_sublists_of_length 4
         Q.[ ~$0 ; ~$1 ; ~$2 ; ~$3 ; ~$4 ; ~$5 ; ~$6 ; ~$7 ; ~$8 ; ~$9 ])
  in
  Printf.printf "max sequence: 1..%a\n" Z.output max_len ;
  Printf.printf "reached with digits %a\n" pp_digits max_digits
