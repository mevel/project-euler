(*
 *  Project Euler #94
 *)

(*
 * the area of the triangle of sides (s, s±1, s±1) is:
 *     A = s √((s±1)² − (s∕2)²) ∕ 2
 * for A to be an integer, we need s to be even, say s = 2x; then:
 *     A = x √(3x² ± 4x + 1)
 *       = x √((x ± 1)(3x ± 1))
 * moreover, gcd (x ± 1, 3x ± 1) = { 1 when x is even,
 *                                 { 2 when x is odd.
 * hence two cases to study:
 *
 * (2) either x is even. in that case, for the square root to be an integer,
 *     both (x ± 1) and (3x ± 1) need to be square numbers:
 *          x ± 1 = a²                       (a is odd)
 *         3x ± 1 = b² = 3a² ∓ 2
 *
 * (1) or x is odd. in that case, both factors need to be twice a square number:
 *          (x ± 1) ∕ 2 = a²                 (a can be odd or even)
 *         (3x ± 1) ∕ 2 = b² = 3a² ∓ 1
 *
 * and all four cases ((1) or (2), + or −) are exclusive (for a ⩾ 2, the set
 * { 3a²−2 ; 3a²−1 ; 3a²+1 ; 3a²+2 } contains at most one square number because
 * the distance between two of them is at least 7). thus, instead of enumerating
 * possible x, we can enumerate possible a and reduce the algorithmic complexity
 * from O(m) to O(√m) where m is the bound on the perimeter (the perimeter being
 * P = 6x ± 2).
 *
 * for a given a:
 *
 * (1) if B = 3a² ∓ 1 is square, then we get an almost‐equilateral triangle of
 *     perimeter P = 4B (and x = 2a² ∓ 1, but we do not need it)
 *
 * (2) otherwise, if (a is odd and) B = 3a² ∓ 2 is square (what can’t occur when
 *     a is even anyway), then we get a triangle with P = 2B (and x = a² ∓ 1)
 *
 * -----
 *
 * note than we can do much better (see euler’s post on the thread): by
 * considering Pell’s equation:
 *     b² - 3a² = k           k ∈ { ±1 ; ±2 }
 * for which we know how to enumerate the solutions, we can directly get the
 * almost‐equilateral triangles.
 *
 * more precisely, the fundamental solution (as given by the convergents of the
 * continued fraction of √3) is r = (b, a) = (2, 1), that we write r = 2 + 1√3.
 * it yields k = 1, thus the solutions to k = 1 are all given by
 *     b + a√3 = r^n        for n ∈ ℕ,
 * and there is no solution for k = −1.
 *
 * by a bounded exhaustive research, we can also show that there is no solution
 * for k = 2, and that there is only one primitive solution for k = −2, which is
 * p = 1 + 1√3; thus the solutions for k = −2 are all given by
 *     b + a√3 = r^n × p    for n ∈ ℕ
 *
 * as a conclusion, two of the four cases above are useless (B = 3a² − 1 and B =
 * 3a² + 2), and we know all the solutions of the two others:
 *     for b² = 3a² + 1:  the solutions are  a + b√3 = r^n        for n ∈ ℕ
 *     for b² = 3a² − 2:  the solutions are  a + b√3 = r^n × p    for n ∈ ℕ
 * where:
 *     p = 1 + √3
 *     r = 2 + √3
 *
 * algorithmically:
 *     the solutions to b² − 3a² = 1 are:
 *         b_n + a_n√3 = (2 + √3)^n
 *     hence, by developping (2 + √3)(b + a√3), we get the iterative calculus:
 *         b_0 ≔ 1
 *         a_0 ≔ 0
 *         b' ≔ 2b + 3a
 *         a' ≔  b + 2a
 *     and the perimeter is P = 4b².
 *     the solutions to d² − 3c² = −2 are deduced from the previous values by:
 *         d_n + c_n√3 = (2 + √3)^n × (1 + √3) = (b_n + a_n√3) × (1 + √3)
 *     so, again developping:
 *         d ≔ b + 3a
 *         c ≔ b +  a
 *     and the perimeter is P = 2d².
 *
 * we can notice than these are exactly given by the successive convergents c_i
 * of the continued fraction of √3 = [1, (1, 2)] (the period is (1, 2)):
 *     c_{−1} = 1 ∕ 0  →  1 + 0√3 = 1
 *     c_0    = 1 ∕ 1  →  1 +  √3 = p
 *     c_1    = 2 ∕ 1  →  2 +  √3 = r
 *     c_2    = 5 ∕ 3  →  5 + 3√3 = r × p
 *     c_3    = 7 ∕ 4  →  7 + 4√3 = r²
 *     …
 *     c_{2n−1} = r^n
 *     c_{2n}   = r^n × p
 *)

(* require packages <sequence> and <zarith> *)

open Z

let max_perimeter = ~$1_000_000_000

(*
 * first solution:
 *   O(√max_perimeter)
 *)

module Solution1 = struct

  let ( |?| ) f1 f2 x =
    begin match f1 x with
    | None   -> f2 x
    | some_y -> some_y
    end

  let case_1 (+-) a2 =
    let b2 = ~$3 * a2 +- ~$(-1) in
    begin match perfect_square b2 with
    | true  -> Some (~$4 * b2)
    | false -> None
    end

  let case_2 (+-) a2 =
    let b2 = ~$3 * a2 +- ~$(-2) in
    begin match perfect_square b2 with (* implies that a² is odd *)
    | true  -> Some (~$2 * b2)
    | false -> None
    end

  let get_triangles max_perimeter =
    let max_1 = Z.to_int @@ sqrt ((max_perimeter + ~$4) / ~$12)
    and max_2 = Z.to_int @@ sqrt ((max_perimeter + ~$4) /  ~$6) in
    let seq1 =
      Sequence.int_range ~start:1 ~stop:max_1
      |> Sequence.map Z.of_int
      |> Sequence.filter_map begin fun a ->
           (case_1 (+) |?| case_1 (-) |?| case_2 (+) |?| case_2 (-)) (a * a)
         end
    and seq2 =
      Sequence.int_range ~start:(Pervasives.succ max_1) ~stop:max_2
      |> Sequence.map Z.of_int
      |> Sequence.filter_map begin fun a ->
           (case_2 (+) |?| case_2 (-)) (a * a)
         end
    in
    Sequence.append seq1 seq2

end (* module Solution1 *)

(*
 * second solution, based on the Pell’s equation:
 *   O(number_of_triangles)
 *)

module Solution2 = struct

  let get_triangles max_perimeter =
    (~$2, one) (* start with (1, 0) to get the flat triangles 2‐1‐1 and 0‐1‐1 *)
    |> Sequence.iterate begin fun (b, a) ->
         let b = ~$2*b + ~$3*a
         and a =     b + ~$2*a in
         b, a
       end
    |> Sequence.flat_map begin fun (b, a) ->
         let d = b + ~$3*a
         and c = b +     a in (* note than c is useless *)
         assert (b*b = ~$3*a*a + ~$1) ;
         assert (d*d = ~$3*c*c - ~$2) ;
         Sequence.doubleton (~$4 * b*b) (~$2 * d*d)
       end
    |> Sequence.take_while (geq max_perimeter)

end (* module Solution2 *)

(* just to print the triangles found, given their perimeter (p = 6x ± 2): *)
let print_triangle p =
  let open Z in
  let x, r = div_rem (p + ~$2) ~$6 in
  let y = ~$2 * x in
  let z = if r = zero then pred y else succ y in
  Printf.printf "\t%9i %9i %9i [ p = %9i ]\n"
    (Z.to_int y) (Z.to_int z) (Z.to_int z) (Z.to_int p)

let solve get_triangles =
  let t0 = Sys.time () in
  max_perimeter
  |> get_triangles
  |> Sequence.map (fun p -> print_triangle p ; p)
  |> Sequence.fold (+) zero
  |> Printf.printf "sum = %a\n" Z.output ;
  let t1 = Sys.time () in
  Printf.printf "time: %g s\n" (t1 -. t0)

let () =
  solve Solution1.get_triangles ;
  solve Solution2.get_triangles
