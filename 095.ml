(*
 *  Project Euler #95
 *)

let primes =
  let primes = Array.make 78_499 0 in
  let rec is_prime ?(i = 0) n =
    let p = primes.(i) in
    if p = 0 || p * p > n then
      true
    else if n mod p = 0 then
      false
    else
      is_prime ~i:(succ i) n
  in
  primes.(0) <- 2 ;
  let count_primes = ref 1 in
  for n = 3 to 1_000_000 do
    if is_prime n then begin
      primes.(!count_primes) <- n ;
      incr count_primes
    end
  done ;
  primes

let sum_of_proper_divisors =
  let rec decompose ?(acc = 1) p n =
    if n mod p = 0 then
      decompose ~acc:(acc * p) p (n / p)
    else
      acc, n
  in
  let rec loop ?(acc = 1) i n =
    let p = primes.(i) in
    if n = 1 then
      acc
    else if p * p > n then (* n is prime *)
      acc * succ n
    else
      let pow_p, n' = decompose p n in
      loop ~acc:(acc * pred (p * pow_p) / pred p) (succ i) n'
  in
  fun n ->
    loop 0 n - n

(* in practice we do not need to keep track of all those states, but still: *)
type state =
  | Initial
  | Exploring
  | Cycle of int ref * int ref (* representative, length *)
  | To_cycle of int (* representative *)
  | Overflow

let states = Array.make 1_000_001 Initial

let max_cycle_len  = ref 0
let max_cycle_repr = ref ~-1

let unexplore path state =
  List.iter (fun n -> states.(n) <- state) path

let unexplore_cycle path =
  let repr = ref max_int
  and len  = ref 0 in
  let state = Cycle (repr, len) in
  let rec unexplore_cycle = function
    | n :: tl when states.(n) = Exploring ->
        states.(n) <- state ;
        if n < !repr then
          repr := n ;
        incr len ;
        unexplore_cycle tl
    | _ :: tl ->
        Printf.eprintf "cycle from %i of length %i\n" !repr !len ;
        if !max_cycle_len < !len then begin
          max_cycle_len  := !len ;
          max_cycle_repr := !repr
        end ;
        unexplore tl (To_cycle !repr)
    | _ -> assert false
  in
  unexplore_cycle path

let rec explore ?(acc = []) n =
  begin match states.(n) with
  | Initial ->
      states.(n) <- Exploring ;
      let s = sum_of_proper_divisors n in
      if s > 1_000_000 then
        unexplore (n :: acc) Overflow
      else
        explore ~acc:(n :: acc) s
  | Exploring       -> unexplore_cycle (n :: acc)
  | Cycle (repr, _) -> unexplore acc (To_cycle !repr)
  | state           -> unexplore acc state
  end

let () =
  for n = 1 to 1_000_000 do
    explore n
  done ;
  Printf.printf "longest cycle: from %i, of length %i\n"
    !max_cycle_repr
    !max_cycle_len
