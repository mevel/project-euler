#!/usr/bin/env swipl

%%%
%%% Project Euler #096
%%%

% this code is known to work with SWI-Prolog 7.6.4

:- use_module(library(clpfd)).

%%
%% Reading the file.
%%

get_nl_or_eof(Stream) :-
	at_end_of_stream(Stream), !.
get_nl_or_eof(Stream) :-
	get_char(Stream, '\n'), !.

get_digit(Stream, _) :-
	peek_char(Stream, '0'),
	!, get_char(Stream, _).
get_digit(Stream, D) :-
	peek_code(Stream, X),
	49 =< X, X =< 57, % 48, 49, …, 57 are the ASCII codes for '0', '1', …, '9'
	!, get_code(Stream, _),
	D is X - 48.

get_row(Stream, Row) :-
	length(Row, 9),
	maplist(get_digit(Stream), Row),
	get_nl_or_eof(Stream).

get_grid(Stream, Rows) :-
	skip(Stream, '\n'), % we ignore the header of the grid (“Grid NN\n”)
	length(Rows, 9),
	maplist(get_row(Stream), Rows).

get_grids(Stream, []) :-
	at_end_of_stream(Stream), !.
get_grids(Stream, [Grid|Grids]) :-
	get_grid(Stream, Grid), !,
	get_grids(Stream, Grids).

%%
%% Solving sudoku grids.
%%

% code shamelessly stolen from
%     https://michal.muskala.eu/2017/01/25/sudoku-solver-in-prolog.html

blocks([], [], [], []).
blocks([A,B,C|Bs1],[D,E,F|Bs2],[G,H,I|Bs3], [Block|Blocks]) :-
	Block = [A,B,C,D,E,F,G,H,I],
	blocks(Bs1, Bs2, Bs3, Blocks).

blocks([A,B,C,D,E,F,G,H,I], Blocks) :-
	blocks(A,B,C,Block1), blocks(D,E,F,Block2), blocks(G,H,I,Block3),
	append([Block1, Block2, Block3], Blocks).

sudoku(Puzzle) :-
	flatten(Puzzle, Tmp), Tmp ins 1..9,
	Rows = Puzzle,
	transpose(Rows, Columns),
	blocks(Rows, Blocks),
	maplist(all_distinct, Rows),
	maplist(all_distinct, Columns),
	maplist(all_distinct, Blocks),
	maplist(label, Rows).

%%
%% Main program.
%%

:- initialization(main, main).

hash_grid(SolvedGrid, Hash) :-
	SolvedGrid = [ FirstRow | _ ],
	FirstRow = [ A, B, C | _ ],
	Hash is 100 * A + 10 * B + C.

main() :-
	open("096-sudoku", read, Stream),
	get_grids(Stream, Grids),
	close(Stream),
	%length(Grids, 50),
	maplist(sudoku, Grids),
	maplist(hash_grid, Grids, Hashes),
	foldl(plus, Hashes, 0, Sum),
	format("sum of top-left corners of each solved sudoku grid: ~d\n", [Sum]),
	!.
