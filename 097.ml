#!/bin/env ocaml

(*
 *  Project Euler #97
 *)

let m = 10_000_000_000

let ( +: ) x y = (x + y) mod m
let ( *: ) x y = (x * y) mod m

(* Using fast exponentiation with 63-bit signed integers will overflow, so let’s
 * resort to linear exponentiation… *)
let rec exp acc b n =
  if n = 0 then
    acc
  else
    exp (acc *: b) b (pred n)

let exp = exp 1

let () =
  Printf.printf "%u\n" (28433 *: exp 2 7_830_457 +: 1)
