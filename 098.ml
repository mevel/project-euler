#!/usr/bin/env ocaml

(*
 *  Project Euler #98
 *)

let words = [|
  "A";"ABILITY";"ABLE";"ABOUT";"ABOVE";"ABSENCE";"ABSOLUTELY";"ACADEMIC";"ACCEPT";"ACCESS";"ACCIDENT";"ACCOMPANY";"ACCORDING";"ACCOUNT";"ACHIEVE";"ACHIEVEMENT";"ACID";"ACQUIRE";"ACROSS";"ACT";"ACTION";"ACTIVE";"ACTIVITY";"ACTUAL";"ACTUALLY";"ADD";"ADDITION";"ADDITIONAL";"ADDRESS";"ADMINISTRATION";"ADMIT";"ADOPT";"ADULT";"ADVANCE";"ADVANTAGE";"ADVICE";"ADVISE";"AFFAIR";"AFFECT";"AFFORD";"AFRAID";"AFTER";"AFTERNOON";"AFTERWARDS";"AGAIN";"AGAINST";"AGE";"AGENCY";"AGENT";"AGO";"AGREE";"AGREEMENT";"AHEAD";"AID";"AIM";"AIR";"AIRCRAFT";"ALL";"ALLOW";"ALMOST";"ALONE";"ALONG";"ALREADY";"ALRIGHT";"ALSO";"ALTERNATIVE";"ALTHOUGH";"ALWAYS";"AMONG";"AMONGST";"AMOUNT";"AN";"ANALYSIS";"ANCIENT";"AND";"ANIMAL";"ANNOUNCE";"ANNUAL";"ANOTHER";"ANSWER";"ANY";"ANYBODY";"ANYONE";"ANYTHING";"ANYWAY";"APART";"APPARENT";"APPARENTLY";"APPEAL";"APPEAR";"APPEARANCE";"APPLICATION";"APPLY";"APPOINT";"APPOINTMENT";"APPROACH";"APPROPRIATE";"APPROVE";"AREA";"ARGUE";"ARGUMENT";"ARISE";"ARM";"ARMY";"AROUND";"ARRANGE";"ARRANGEMENT";"ARRIVE";"ART";"ARTICLE";"ARTIST";"AS";"ASK";"ASPECT";"ASSEMBLY";"ASSESS";"ASSESSMENT";"ASSET";"ASSOCIATE";"ASSOCIATION";"ASSUME";"ASSUMPTION";"AT";"ATMOSPHERE";"ATTACH";"ATTACK";"ATTEMPT";"ATTEND";"ATTENTION";"ATTITUDE";"ATTRACT";"ATTRACTIVE";"AUDIENCE";"AUTHOR";"AUTHORITY";"AVAILABLE";"AVERAGE";"AVOID";"AWARD";"AWARE";"AWAY";"AYE";"BABY";"BACK";"BACKGROUND";"BAD";"BAG";"BALANCE";"BALL";"BAND";"BANK";"BAR";"BASE";"BASIC";"BASIS";"BATTLE";"BE";"BEAR";"BEAT";"BEAUTIFUL";"BECAUSE";"BECOME";"BED";"BEDROOM";"BEFORE";"BEGIN";"BEGINNING";"BEHAVIOUR";"BEHIND";"BELIEF";"BELIEVE";"BELONG";"BELOW";"BENEATH";"BENEFIT";"BESIDE";"BEST";"BETTER";"BETWEEN";"BEYOND";"BIG";"BILL";"BIND";"BIRD";"BIRTH";"BIT";"BLACK";"BLOCK";"BLOOD";"BLOODY";"BLOW";"BLUE";"BOARD";"BOAT";"BODY";"BONE";"BOOK";"BORDER";"BOTH";"BOTTLE";"BOTTOM";"BOX";"BOY";"BRAIN";"BRANCH";"BREAK";"BREATH";"BRIDGE";"BRIEF";"BRIGHT";"BRING";"BROAD";"BROTHER";"BUDGET";"BUILD";"BUILDING";"BURN";"BUS";"BUSINESS";"BUSY";"BUT";"BUY";"BY";"CABINET";"CALL";"CAMPAIGN";"CAN";"CANDIDATE";"CAPABLE";"CAPACITY";"CAPITAL";"CAR";"CARD";"CARE";"CAREER";"CAREFUL";"CAREFULLY";"CARRY";"CASE";"CASH";"CAT";"CATCH";"CATEGORY";"CAUSE";"CELL";"CENTRAL";"CENTRE";"CENTURY";"CERTAIN";"CERTAINLY";"CHAIN";"CHAIR";"CHAIRMAN";"CHALLENGE";"CHANCE";"CHANGE";"CHANNEL";"CHAPTER";"CHARACTER";"CHARACTERISTIC";"CHARGE";"CHEAP";"CHECK";"CHEMICAL";"CHIEF";"CHILD";"CHOICE";"CHOOSE";"CHURCH";"CIRCLE";"CIRCUMSTANCE";"CITIZEN";"CITY";"CIVIL";"CLAIM";"CLASS";"CLEAN";"CLEAR";"CLEARLY";"CLIENT";"CLIMB";"CLOSE";"CLOSELY";"CLOTHES";"CLUB";"COAL";"CODE";"COFFEE";"COLD";"COLLEAGUE";"COLLECT";"COLLECTION";"COLLEGE";"COLOUR";"COMBINATION";"COMBINE";"COME";"COMMENT";"COMMERCIAL";"COMMISSION";"COMMIT";"COMMITMENT";"COMMITTEE";"COMMON";"COMMUNICATION";"COMMUNITY";"COMPANY";"COMPARE";"COMPARISON";"COMPETITION";"COMPLETE";"COMPLETELY";"COMPLEX";"COMPONENT";"COMPUTER";"CONCENTRATE";"CONCENTRATION";"CONCEPT";"CONCERN";"CONCERNED";"CONCLUDE";"CONCLUSION";"CONDITION";"CONDUCT";"CONFERENCE";"CONFIDENCE";"CONFIRM";"CONFLICT";"CONGRESS";"CONNECT";"CONNECTION";"CONSEQUENCE";"CONSERVATIVE";"CONSIDER";"CONSIDERABLE";"CONSIDERATION";"CONSIST";"CONSTANT";"CONSTRUCTION";"CONSUMER";"CONTACT";"CONTAIN";"CONTENT";"CONTEXT";"CONTINUE";"CONTRACT";"CONTRAST";"CONTRIBUTE";"CONTRIBUTION";"CONTROL";"CONVENTION";"CONVERSATION";"COPY";"CORNER";"CORPORATE";"CORRECT";"COS";"COST";"COULD";"COUNCIL";"COUNT";"COUNTRY";"COUNTY";"COUPLE";"COURSE";"COURT";"COVER";"CREATE";"CREATION";"CREDIT";"CRIME";"CRIMINAL";"CRISIS";"CRITERION";"CRITICAL";"CRITICISM";"CROSS";"CROWD";"CRY";"CULTURAL";"CULTURE";"CUP";"CURRENT";"CURRENTLY";"CURRICULUM";"CUSTOMER";"CUT";"DAMAGE";"DANGER";"DANGEROUS";"DARK";"DATA";"DATE";"DAUGHTER";"DAY";"DEAD";"DEAL";"DEATH";"DEBATE";"DEBT";"DECADE";"DECIDE";"DECISION";"DECLARE";"DEEP";"DEFENCE";"DEFENDANT";"DEFINE";"DEFINITION";"DEGREE";"DELIVER";"DEMAND";"DEMOCRATIC";"DEMONSTRATE";"DENY";"DEPARTMENT";"DEPEND";"DEPUTY";"DERIVE";"DESCRIBE";"DESCRIPTION";"DESIGN";"DESIRE";"DESK";"DESPITE";"DESTROY";"DETAIL";"DETAILED";"DETERMINE";"DEVELOP";"DEVELOPMENT";"DEVICE";"DIE";"DIFFERENCE";"DIFFERENT";"DIFFICULT";"DIFFICULTY";"DINNER";"DIRECT";"DIRECTION";"DIRECTLY";"DIRECTOR";"DISAPPEAR";"DISCIPLINE";"DISCOVER";"DISCUSS";"DISCUSSION";"DISEASE";"DISPLAY";"DISTANCE";"DISTINCTION";"DISTRIBUTION";"DISTRICT";"DIVIDE";"DIVISION";"DO";"DOCTOR";"DOCUMENT";"DOG";"DOMESTIC";"DOOR";"DOUBLE";"DOUBT";"DOWN";"DRAW";"DRAWING";"DREAM";"DRESS";"DRINK";"DRIVE";"DRIVER";"DROP";"DRUG";"DRY";"DUE";"DURING";"DUTY";"EACH";"EAR";"EARLY";"EARN";"EARTH";"EASILY";"EAST";"EASY";"EAT";"ECONOMIC";"ECONOMY";"EDGE";"EDITOR";"EDUCATION";"EDUCATIONAL";"EFFECT";"EFFECTIVE";"EFFECTIVELY";"EFFORT";"EGG";"EITHER";"ELDERLY";"ELECTION";"ELEMENT";"ELSE";"ELSEWHERE";"EMERGE";"EMPHASIS";"EMPLOY";"EMPLOYEE";"EMPLOYER";"EMPLOYMENT";"EMPTY";"ENABLE";"ENCOURAGE";"END";"ENEMY";"ENERGY";"ENGINE";"ENGINEERING";"ENJOY";"ENOUGH";"ENSURE";"ENTER";"ENTERPRISE";"ENTIRE";"ENTIRELY";"ENTITLE";"ENTRY";"ENVIRONMENT";"ENVIRONMENTAL";"EQUAL";"EQUALLY";"EQUIPMENT";"ERROR";"ESCAPE";"ESPECIALLY";"ESSENTIAL";"ESTABLISH";"ESTABLISHMENT";"ESTATE";"ESTIMATE";"EVEN";"EVENING";"EVENT";"EVENTUALLY";"EVER";"EVERY";"EVERYBODY";"EVERYONE";"EVERYTHING";"EVIDENCE";"EXACTLY";"EXAMINATION";"EXAMINE";"EXAMPLE";"EXCELLENT";"EXCEPT";"EXCHANGE";"EXECUTIVE";"EXERCISE";"EXHIBITION";"EXIST";"EXISTENCE";"EXISTING";"EXPECT";"EXPECTATION";"EXPENDITURE";"EXPENSE";"EXPENSIVE";"EXPERIENCE";"EXPERIMENT";"EXPERT";"EXPLAIN";"EXPLANATION";"EXPLORE";"EXPRESS";"EXPRESSION";"EXTEND";"EXTENT";"EXTERNAL";"EXTRA";"EXTREMELY";"EYE";"FACE";"FACILITY";"FACT";"FACTOR";"FACTORY";"FAIL";"FAILURE";"FAIR";"FAIRLY";"FAITH";"FALL";"FAMILIAR";"FAMILY";"FAMOUS";"FAR";"FARM";"FARMER";"FASHION";"FAST";"FATHER";"FAVOUR";"FEAR";"FEATURE";"FEE";"FEEL";"FEELING";"FEMALE";"FEW";"FIELD";"FIGHT";"FIGURE";"FILE";"FILL";"FILM";"FINAL";"FINALLY";"FINANCE";"FINANCIAL";"FIND";"FINDING";"FINE";"FINGER";"FINISH";"FIRE";"FIRM";"FIRST";"FISH";"FIT";"FIX";"FLAT";"FLIGHT";"FLOOR";"FLOW";"FLOWER";"FLY";"FOCUS";"FOLLOW";"FOLLOWING";"FOOD";"FOOT";"FOOTBALL";"FOR";"FORCE";"FOREIGN";"FOREST";"FORGET";"FORM";"FORMAL";"FORMER";"FORWARD";"FOUNDATION";"FREE";"FREEDOM";"FREQUENTLY";"FRESH";"FRIEND";"FROM";"FRONT";"FRUIT";"FUEL";"FULL";"FULLY";"FUNCTION";"FUND";"FUNNY";"FURTHER";"FUTURE";"GAIN";"GAME";"GARDEN";"GAS";"GATE";"GATHER";"GENERAL";"GENERALLY";"GENERATE";"GENERATION";"GENTLEMAN";"GET";"GIRL";"GIVE";"GLASS";"GO";"GOAL";"GOD";"GOLD";"GOOD";"GOVERNMENT";"GRANT";"GREAT";"GREEN";"GREY";"GROUND";"GROUP";"GROW";"GROWING";"GROWTH";"GUEST";"GUIDE";"GUN";"HAIR";"HALF";"HALL";"HAND";"HANDLE";"HANG";"HAPPEN";"HAPPY";"HARD";"HARDLY";"HATE";"HAVE";"HE";"HEAD";"HEALTH";"HEAR";"HEART";"HEAT";"HEAVY";"HELL";"HELP";"HENCE";"HER";"HERE";"HERSELF";"HIDE";"HIGH";"HIGHLY";"HILL";"HIM";"HIMSELF";"HIS";"HISTORICAL";"HISTORY";"HIT";"HOLD";"HOLE";"HOLIDAY";"HOME";"HOPE";"HORSE";"HOSPITAL";"HOT";"HOTEL";"HOUR";"HOUSE";"HOUSEHOLD";"HOUSING";"HOW";"HOWEVER";"HUGE";"HUMAN";"HURT";"HUSBAND";"I";"IDEA";"IDENTIFY";"IF";"IGNORE";"ILLUSTRATE";"IMAGE";"IMAGINE";"IMMEDIATE";"IMMEDIATELY";"IMPACT";"IMPLICATION";"IMPLY";"IMPORTANCE";"IMPORTANT";"IMPOSE";"IMPOSSIBLE";"IMPRESSION";"IMPROVE";"IMPROVEMENT";"IN";"INCIDENT";"INCLUDE";"INCLUDING";"INCOME";"INCREASE";"INCREASED";"INCREASINGLY";"INDEED";"INDEPENDENT";"INDEX";"INDICATE";"INDIVIDUAL";"INDUSTRIAL";"INDUSTRY";"INFLUENCE";"INFORM";"INFORMATION";"INITIAL";"INITIATIVE";"INJURY";"INSIDE";"INSIST";"INSTANCE";"INSTEAD";"INSTITUTE";"INSTITUTION";"INSTRUCTION";"INSTRUMENT";"INSURANCE";"INTEND";"INTENTION";"INTEREST";"INTERESTED";"INTERESTING";"INTERNAL";"INTERNATIONAL";"INTERPRETATION";"INTERVIEW";"INTO";"INTRODUCE";"INTRODUCTION";"INVESTIGATE";"INVESTIGATION";"INVESTMENT";"INVITE";"INVOLVE";"IRON";"IS";"ISLAND";"ISSUE";"IT";"ITEM";"ITS";"ITSELF";"JOB";"JOIN";"JOINT";"JOURNEY";"JUDGE";"JUMP";"JUST";"JUSTICE";"KEEP";"KEY";"KID";"KILL";"KIND";"KING";"KITCHEN";"KNEE";"KNOW";"KNOWLEDGE";"LABOUR";"LACK";"LADY";"LAND";"LANGUAGE";"LARGE";"LARGELY";"LAST";"LATE";"LATER";"LATTER";"LAUGH";"LAUNCH";"LAW";"LAWYER";"LAY";"LEAD";"LEADER";"LEADERSHIP";"LEADING";"LEAF";"LEAGUE";"LEAN";"LEARN";"LEAST";"LEAVE";"LEFT";"LEG";"LEGAL";"LEGISLATION";"LENGTH";"LESS";"LET";"LETTER";"LEVEL";"LIABILITY";"LIBERAL";"LIBRARY";"LIE";"LIFE";"LIFT";"LIGHT";"LIKE";"LIKELY";"LIMIT";"LIMITED";"LINE";"LINK";"LIP";"LIST";"LISTEN";"LITERATURE";"LITTLE";"LIVE";"LIVING";"LOAN";"LOCAL";"LOCATION";"LONG";"LOOK";"LORD";"LOSE";"LOSS";"LOT";"LOVE";"LOVELY";"LOW";"LUNCH";"MACHINE";"MAGAZINE";"MAIN";"MAINLY";"MAINTAIN";"MAJOR";"MAJORITY";"MAKE";"MALE";"MAN";"MANAGE";"MANAGEMENT";"MANAGER";"MANNER";"MANY";"MAP";"MARK";"MARKET";"MARRIAGE";"MARRIED";"MARRY";"MASS";"MASTER";"MATCH";"MATERIAL";"MATTER";"MAY";"MAYBE";"ME";"MEAL";"MEAN";"MEANING";"MEANS";"MEANWHILE";"MEASURE";"MECHANISM";"MEDIA";"MEDICAL";"MEET";"MEETING";"MEMBER";"MEMBERSHIP";"MEMORY";"MENTAL";"MENTION";"MERELY";"MESSAGE";"METAL";"METHOD";"MIDDLE";"MIGHT";"MILE";"MILITARY";"MILK";"MIND";"MINE";"MINISTER";"MINISTRY";"MINUTE";"MISS";"MISTAKE";"MODEL";"MODERN";"MODULE";"MOMENT";"MONEY";"MONTH";"MORE";"MORNING";"MOST";"MOTHER";"MOTION";"MOTOR";"MOUNTAIN";"MOUTH";"MOVE";"MOVEMENT";"MUCH";"MURDER";"MUSEUM";"MUSIC";"MUST";"MY";"MYSELF";"NAME";"NARROW";"NATION";"NATIONAL";"NATURAL";"NATURE";"NEAR";"NEARLY";"NECESSARILY";"NECESSARY";"NECK";"NEED";"NEGOTIATION";"NEIGHBOUR";"NEITHER";"NETWORK";"NEVER";"NEVERTHELESS";"NEW";"NEWS";"NEWSPAPER";"NEXT";"NICE";"NIGHT";"NO";"NOBODY";"NOD";"NOISE";"NONE";"NOR";"NORMAL";"NORMALLY";"NORTH";"NORTHERN";"NOSE";"NOT";"NOTE";"NOTHING";"NOTICE";"NOTION";"NOW";"NUCLEAR";"NUMBER";"NURSE";"OBJECT";"OBJECTIVE";"OBSERVATION";"OBSERVE";"OBTAIN";"OBVIOUS";"OBVIOUSLY";"OCCASION";"OCCUR";"ODD";"OF";"OFF";"OFFENCE";"OFFER";"OFFICE";"OFFICER";"OFFICIAL";"OFTEN";"OIL";"OKAY";"OLD";"ON";"ONCE";"ONE";"ONLY";"ONTO";"OPEN";"OPERATE";"OPERATION";"OPINION";"OPPORTUNITY";"OPPOSITION";"OPTION";"OR";"ORDER";"ORDINARY";"ORGANISATION";"ORGANISE";"ORGANIZATION";"ORIGIN";"ORIGINAL";"OTHER";"OTHERWISE";"OUGHT";"OUR";"OURSELVES";"OUT";"OUTCOME";"OUTPUT";"OUTSIDE";"OVER";"OVERALL";"OWN";"OWNER";"PACKAGE";"PAGE";"PAIN";"PAINT";"PAINTING";"PAIR";"PANEL";"PAPER";"PARENT";"PARK";"PARLIAMENT";"PART";"PARTICULAR";"PARTICULARLY";"PARTLY";"PARTNER";"PARTY";"PASS";"PASSAGE";"PAST";"PATH";"PATIENT";"PATTERN";"PAY";"PAYMENT";"PEACE";"PENSION";"PEOPLE";"PER";"PERCENT";"PERFECT";"PERFORM";"PERFORMANCE";"PERHAPS";"PERIOD";"PERMANENT";"PERSON";"PERSONAL";"PERSUADE";"PHASE";"PHONE";"PHOTOGRAPH";"PHYSICAL";"PICK";"PICTURE";"PIECE";"PLACE";"PLAN";"PLANNING";"PLANT";"PLASTIC";"PLATE";"PLAY";"PLAYER";"PLEASE";"PLEASURE";"PLENTY";"PLUS";"POCKET";"POINT";"POLICE";"POLICY";"POLITICAL";"POLITICS";"POOL";"POOR";"POPULAR";"POPULATION";"POSITION";"POSITIVE";"POSSIBILITY";"POSSIBLE";"POSSIBLY";"POST";"POTENTIAL";"POUND";"POWER";"POWERFUL";"PRACTICAL";"PRACTICE";"PREFER";"PREPARE";"PRESENCE";"PRESENT";"PRESIDENT";"PRESS";"PRESSURE";"PRETTY";"PREVENT";"PREVIOUS";"PREVIOUSLY";"PRICE";"PRIMARY";"PRIME";"PRINCIPLE";"PRIORITY";"PRISON";"PRISONER";"PRIVATE";"PROBABLY";"PROBLEM";"PROCEDURE";"PROCESS";"PRODUCE";"PRODUCT";"PRODUCTION";"PROFESSIONAL";"PROFIT";"PROGRAM";"PROGRAMME";"PROGRESS";"PROJECT";"PROMISE";"PROMOTE";"PROPER";"PROPERLY";"PROPERTY";"PROPORTION";"PROPOSE";"PROPOSAL";"PROSPECT";"PROTECT";"PROTECTION";"PROVE";"PROVIDE";"PROVIDED";"PROVISION";"PUB";"PUBLIC";"PUBLICATION";"PUBLISH";"PULL";"PUPIL";"PURPOSE";"PUSH";"PUT";"QUALITY";"QUARTER";"QUESTION";"QUICK";"QUICKLY";"QUIET";"QUITE";"RACE";"RADIO";"RAILWAY";"RAIN";"RAISE";"RANGE";"RAPIDLY";"RARE";"RATE";"RATHER";"REACH";"REACTION";"READ";"READER";"READING";"READY";"REAL";"REALISE";"REALITY";"REALIZE";"REALLY";"REASON";"REASONABLE";"RECALL";"RECEIVE";"RECENT";"RECENTLY";"RECOGNISE";"RECOGNITION";"RECOGNIZE";"RECOMMEND";"RECORD";"RECOVER";"RED";"REDUCE";"REDUCTION";"REFER";"REFERENCE";"REFLECT";"REFORM";"REFUSE";"REGARD";"REGION";"REGIONAL";"REGULAR";"REGULATION";"REJECT";"RELATE";"RELATION";"RELATIONSHIP";"RELATIVE";"RELATIVELY";"RELEASE";"RELEVANT";"RELIEF";"RELIGION";"RELIGIOUS";"RELY";"REMAIN";"REMEMBER";"REMIND";"REMOVE";"REPEAT";"REPLACE";"REPLY";"REPORT";"REPRESENT";"REPRESENTATION";"REPRESENTATIVE";"REQUEST";"REQUIRE";"REQUIREMENT";"RESEARCH";"RESOURCE";"RESPECT";"RESPOND";"RESPONSE";"RESPONSIBILITY";"RESPONSIBLE";"REST";"RESTAURANT";"RESULT";"RETAIN";"RETURN";"REVEAL";"REVENUE";"REVIEW";"REVOLUTION";"RICH";"RIDE";"RIGHT";"RING";"RISE";"RISK";"RIVER";"ROAD";"ROCK";"ROLE";"ROLL";"ROOF";"ROOM";"ROUND";"ROUTE";"ROW";"ROYAL";"RULE";"RUN";"RURAL";"SAFE";"SAFETY";"SALE";"SAME";"SAMPLE";"SATISFY";"SAVE";"SAY";"SCALE";"SCENE";"SCHEME";"SCHOOL";"SCIENCE";"SCIENTIFIC";"SCIENTIST";"SCORE";"SCREEN";"SEA";"SEARCH";"SEASON";"SEAT";"SECOND";"SECONDARY";"SECRETARY";"SECTION";"SECTOR";"SECURE";"SECURITY";"SEE";"SEEK";"SEEM";"SELECT";"SELECTION";"SELL";"SEND";"SENIOR";"SENSE";"SENTENCE";"SEPARATE";"SEQUENCE";"SERIES";"SERIOUS";"SERIOUSLY";"SERVANT";"SERVE";"SERVICE";"SESSION";"SET";"SETTLE";"SETTLEMENT";"SEVERAL";"SEVERE";"SEX";"SEXUAL";"SHAKE";"SHALL";"SHAPE";"SHARE";"SHE";"SHEET";"SHIP";"SHOE";"SHOOT";"SHOP";"SHORT";"SHOT";"SHOULD";"SHOULDER";"SHOUT";"SHOW";"SHUT";"SIDE";"SIGHT";"SIGN";"SIGNAL";"SIGNIFICANCE";"SIGNIFICANT";"SILENCE";"SIMILAR";"SIMPLE";"SIMPLY";"SINCE";"SING";"SINGLE";"SIR";"SISTER";"SIT";"SITE";"SITUATION";"SIZE";"SKILL";"SKIN";"SKY";"SLEEP";"SLIGHTLY";"SLIP";"SLOW";"SLOWLY";"SMALL";"SMILE";"SO";"SOCIAL";"SOCIETY";"SOFT";"SOFTWARE";"SOIL";"SOLDIER";"SOLICITOR";"SOLUTION";"SOME";"SOMEBODY";"SOMEONE";"SOMETHING";"SOMETIMES";"SOMEWHAT";"SOMEWHERE";"SON";"SONG";"SOON";"SORRY";"SORT";"SOUND";"SOURCE";"SOUTH";"SOUTHERN";"SPACE";"SPEAK";"SPEAKER";"SPECIAL";"SPECIES";"SPECIFIC";"SPEECH";"SPEED";"SPEND";"SPIRIT";"SPORT";"SPOT";"SPREAD";"SPRING";"STAFF";"STAGE";"STAND";"STANDARD";"STAR";"START";"STATE";"STATEMENT";"STATION";"STATUS";"STAY";"STEAL";"STEP";"STICK";"STILL";"STOCK";"STONE";"STOP";"STORE";"STORY";"STRAIGHT";"STRANGE";"STRATEGY";"STREET";"STRENGTH";"STRIKE";"STRONG";"STRONGLY";"STRUCTURE";"STUDENT";"STUDIO";"STUDY";"STUFF";"STYLE";"SUBJECT";"SUBSTANTIAL";"SUCCEED";"SUCCESS";"SUCCESSFUL";"SUCH";"SUDDENLY";"SUFFER";"SUFFICIENT";"SUGGEST";"SUGGESTION";"SUITABLE";"SUM";"SUMMER";"SUN";"SUPPLY";"SUPPORT";"SUPPOSE";"SURE";"SURELY";"SURFACE";"SURPRISE";"SURROUND";"SURVEY";"SURVIVE";"SWITCH";"SYSTEM";"TABLE";"TAKE";"TALK";"TALL";"TAPE";"TARGET";"TASK";"TAX";"TEA";"TEACH";"TEACHER";"TEACHING";"TEAM";"TEAR";"TECHNICAL";"TECHNIQUE";"TECHNOLOGY";"TELEPHONE";"TELEVISION";"TELL";"TEMPERATURE";"TEND";"TERM";"TERMS";"TERRIBLE";"TEST";"TEXT";"THAN";"THANK";"THANKS";"THAT";"THE";"THEATRE";"THEIR";"THEM";"THEME";"THEMSELVES";"THEN";"THEORY";"THERE";"THEREFORE";"THESE";"THEY";"THIN";"THING";"THINK";"THIS";"THOSE";"THOUGH";"THOUGHT";"THREAT";"THREATEN";"THROUGH";"THROUGHOUT";"THROW";"THUS";"TICKET";"TIME";"TINY";"TITLE";"TO";"TODAY";"TOGETHER";"TOMORROW";"TONE";"TONIGHT";"TOO";"TOOL";"TOOTH";"TOP";"TOTAL";"TOTALLY";"TOUCH";"TOUR";"TOWARDS";"TOWN";"TRACK";"TRADE";"TRADITION";"TRADITIONAL";"TRAFFIC";"TRAIN";"TRAINING";"TRANSFER";"TRANSPORT";"TRAVEL";"TREAT";"TREATMENT";"TREATY";"TREE";"TREND";"TRIAL";"TRIP";"TROOP";"TROUBLE";"TRUE";"TRUST";"TRUTH";"TRY";"TURN";"TWICE";"TYPE";"TYPICAL";"UNABLE";"UNDER";"UNDERSTAND";"UNDERSTANDING";"UNDERTAKE";"UNEMPLOYMENT";"UNFORTUNATELY";"UNION";"UNIT";"UNITED";"UNIVERSITY";"UNLESS";"UNLIKELY";"UNTIL";"UP";"UPON";"UPPER";"URBAN";"US";"USE";"USED";"USEFUL";"USER";"USUAL";"USUALLY";"VALUE";"VARIATION";"VARIETY";"VARIOUS";"VARY";"VAST";"VEHICLE";"VERSION";"VERY";"VIA";"VICTIM";"VICTORY";"VIDEO";"VIEW";"VILLAGE";"VIOLENCE";"VISION";"VISIT";"VISITOR";"VITAL";"VOICE";"VOLUME";"VOTE";"WAGE";"WAIT";"WALK";"WALL";"WANT";"WAR";"WARM";"WARN";"WASH";"WATCH";"WATER";"WAVE";"WAY";"WE";"WEAK";"WEAPON";"WEAR";"WEATHER";"WEEK";"WEEKEND";"WEIGHT";"WELCOME";"WELFARE";"WELL";"WEST";"WESTERN";"WHAT";"WHATEVER";"WHEN";"WHERE";"WHEREAS";"WHETHER";"WHICH";"WHILE";"WHILST";"WHITE";"WHO";"WHOLE";"WHOM";"WHOSE";"WHY";"WIDE";"WIDELY";"WIFE";"WILD";"WILL";"WIN";"WIND";"WINDOW";"WINE";"WING";"WINNER";"WINTER";"WISH";"WITH";"WITHDRAW";"WITHIN";"WITHOUT";"WOMAN";"WONDER";"WONDERFUL";"WOOD";"WORD";"WORK";"WORKER";"WORKING";"WORKS";"WORLD";"WORRY";"WORTH";"WOULD";"WRITE";"WRITER";"WRITING";"WRONG";"YARD";"YEAH";"YEAR";"YES";"YESTERDAY";"YET";"YOU";"YOUNG";"YOUR";"YOURSELF";"YOUTH"
|]

(**
 ** Misc util
 **)

let ( % ) g f x = g @@ f @@ x

let ( ~~ ) to_apply ~(f : _ -> _) = to_apply f
let ( ~~~ ) to_apply x ~(f : _ -> _) = to_apply x f
let ( ~> ) to_apply ~(f : _ -> _) ~init s = to_apply f init s (* fold_left *)
let ( ~< ) to_apply ~(f : _ -> _) s ~init = to_apply f s init (* fold_right *)

let seq_init len f =
  let rec seq_init_rec i () =
    if i < len then
      Seq.Cons (f i, seq_init_rec (i+1))
    else
      Seq.Nil
  in
  seq_init_rec 0

(**
 ** Classifiers
 **)

module ListClassifier (M : Map.S)
: sig
  val classify_into_rev_lists : f:('elt -> M.key) -> 'elt Seq.t -> 'elt list M.t
end
= struct
  let classify_into_rev_lists ~f s =
    ~>Seq.fold_left s ~init:M.empty ~f:begin fun m x ->
      ~~~M.update (f x) m ~f:begin function
        | None     -> Some [x]
        | Some xs' -> Some (x :: xs')
      end
    end
end

let classify_into_rev_list_seq (type key elt)
    ?(key_compare = Stdlib.compare)
    ~(f: elt -> key) (s : elt Seq.t)
  : (key * elt list) Seq.t =
  let module M = Map.Make (struct type t = key let compare = key_compare end) in
  let module C = ListClassifier (M) in
  s |> C.classify_into_rev_lists ~f |> M.to_seq

module SetClassifier (M : Map.S) (S : Set.S)
: sig
  val classify_into_sets : f:(S.elt -> M.key) -> S.elt Seq.t -> S.t M.t
end
= struct
  (*
  let classify_into_sets ~f s =
    ~>Seq.fold_left s ~init:M.empty ~f:begin fun m x ->
      ~~~M.update (f x) m ~f:begin function
        | None    -> Some (S.singleton x)
        | Some s' -> Some (S.add x s')
      end
    end
  *)
  (* faster? [S.of_list] is faster than repeated additions. *)
  module LC = ListClassifier (M)
  let classify_into_sets ~f s =
    s |> LC.classify_into_rev_lists ~f |> M.map S.of_list
end

let classify_into_set_seq (type key elt s)
    ?(key_compare = Stdlib.compare)
    (module S : Set.S with type elt = elt and type t = s)
    ~(f: elt -> key) (s : elt Seq.t)
  : (key * s) Seq.t =
  let module M = Map.Make (struct type t = key let compare = key_compare end) in
  let module C = SetClassifier (M) (S) in
  s |> C.classify_into_sets ~f |> M.to_seq

(**
 ** Specific code
 **)

let letter_vector word =
  (* compute a vector in ℕ^26 giving the number of occurrences of each letter A-Z: *)
  let count = Array.make 26 0 in
  StringLabels.iter word ~f:begin fun c ->
    assert ('A' <= c && c <= 'Z') ;
    let b = Char.code c - Char.code 'A' in
    count.(b) <- count.(b) + 1
  end ;
  (* (unnecessary but fun) return the result as a string with the same letters
   * as the original words but letters are sorted (more compact, faster to
   * compare and for obtaining the length, easier to print when debugging): *)
  let bytes = Bytes.create (String.length word) in
  let i = ref 0 in
  for b = 0 to 25 do
    BytesLabels.fill bytes ~pos:!i ~len:count.(b) (Char.chr (b + Char.code 'A')) ;
    i := !i + count.(b) ;
  done ;
  Bytes.unsafe_to_string bytes

module CharMap = Map.Make (Char)

let build_charmap ~src ~dst =
  assert (String.length src = String.length dst) ;
  let exception Contradiction in
  let digits_used = Array.make 10 false in
  begin try
    let m = ref CharMap.empty in
    StringLabels.iteri dst ~f:begin fun i dst_i ->
      assert ('0' <= dst_i && dst_i <= '9') ;
      let d = Char.code dst_i - Char.code '0' in
      if digits_used.(d) then raise Contradiction ;
      digits_used.(d) <- true ;
      m :=
        ~~~CharMap.update src.[i] !m ~f:begin function
          | None                          -> Some dst_i
          | Some dst_j when dst_j = dst_i -> Some dst_j
          | _                             -> raise Contradiction
        end
    end ;
    let m = !m in
    Some (fun c -> CharMap.find c m)
  with Contradiction ->
    None
  end

(* correct for n <= 2^26: *)
let isqrt n =
  truncate @@ sqrt @@ float n

module IntSet = Set.Make (Int)

let () =
  (* compute all anagram pairs, sorted by descending lengths: *)
  let ana_pairs_by_len =
    (* classify all words by descending lengths (order of words for a given
     * length is irrelevant): *)
    words
    |>  Array.to_seq
    |>  classify_into_rev_list_seq ~key_compare:(Fun.flip compare) ~f:String.length
    (* at each length… *)
    |>  Seq.filter_map begin fun (len, words_of_that_len) ->
          let pairs =
            (* distribute words of that length into anagram buckets (order
             * between buckets and within buckets are irrelevant): *)
            words_of_that_len
            |>  List.to_seq
            |>  classify_into_rev_list_seq ~f:letter_vector
            (* compute the set of all word pairs within each anagram bucket,
             * and join all buckets for that length: *)
            |>  ~>Seq.fold_left ~init:[] ~f:begin fun pairs (_v, ana_words) ->
                  ~>List.fold_left ana_words ~init:pairs ~f:begin fun pairs w0 ->
                    ~>List.fold_left ana_words ~init:pairs ~f:begin fun pairs w1 ->
                      (* NOTE: (w0,w1) is an anagram [square] pair iff (w0,w1)
                       * is; we keep the symmetry here, we’ll break it later
                       * when computing anagram squares, by imposing that
                       * number(w0) > number(w1). this is so that we can more
                       * easily sort the results by descending square values. *)
                      if w0 != w1 && w0 <> w1 then
                        (w0, w1) :: pairs
                      else
                        pairs
                    end
                  end
                end
          in
          (* discard that length if there are no anagram pairs: *)
          if pairs = [] then None else Some (len, pairs)
        end
    |>  List.of_seq
  in
  Printf.printf "Anagramic pairs sorted by descending lengths:\n" ;
  ListLabels.iter ana_pairs_by_len ~f:begin fun (len, pairs) ->
    ListLabels.iter pairs ~f:begin fun (w0, w1) ->
      Printf.printf "\t%u: %s ~ %s\n" len w0 w1
    end
  end ;
  Printf.printf "Anagramic square pairs sorted by descending values:\n" ;
  (* build the sequence of square numbers up to the required length: *)
  let max_len = fst @@ List.hd ana_pairs_by_len in
  seq_init (truncate @@ 10. ** (float max_len *. 0.5)) (fun r -> r * r)
  (* classify them by descending lengths; for a given length, squares are sorted
   * (we use sets for more efficient lookup, even though lists are fast enough
   * for solving the problem). *)
  |>  classify_into_set_seq ~key_compare:(Fun.flip compare) (module IntSet)
        ~f:(succ % truncate % log10 % float)
  (* for each length (in descending order): *)
  |>  Seq.iter begin fun (len, squares) ->
        (* select the word pairs of that length, if any:
         * NOTE: we might optimize this lookup by dropping greater lengths, but
         * given that we only consider small lengths, it wouldn’t bring much. *)
        begin match List.assoc_opt len ana_pairs_by_len with
        | None -> ()
        | Some pairs ->
            (* for each square of that length (in descending order)… *)
            ~~Seq.iter (IntSet.to_rev_seq squares) ~f:begin fun sq0 ->
              (* for each word pair of that length… *)
              ~~List.iter pairs ~f:begin fun (w0, w1) ->
                (* try to map letters to digits such that the first word is
                 * mapped to this square number: *)
                begin match build_charmap ~src:w0 ~dst:(string_of_int sq0) with
                | None   -> ()
                | Some m ->
                    (* with the same letter-digit mapping, compute the number
                     * associated to the second word:
                     * NOTE: I thought I’d never find a use for [String.map]. *)
                    let w1' = String.map m w1 in
                    let sq1 = int_of_string w1' in
                    (* test whether the resulting number is a lesser square not
                     * starting with '0': *)
                    if sq0 > sq1 && w1'.[0] <> '0' && IntSet.mem sq1 squares then
                      Printf.printf "\t%s (%u = %u²) ~ %s (%u = %u²)\n"
                        w0 sq0 (isqrt sq0)
                        w1 sq1 (isqrt sq1)
                end
              end
            end
        end
      end

(*
 * A version of the code using sets everywhere, even when we don’t care about
 * the order.
 *)

(*
module IntSet = Set.Make (Int)
module StrSet = Set.Make (String)
module StrPairSet = Set.Make (struct type t = string * string let compare = compare end)

let () =
  (* compute all anagram pairs, sorted by descending lengths: *)
  let ana_pairs_by_len =
    (* classify all words by descending lengths (order of words for a given
     * length is irrelevant): *)
    words
    |>  Array.to_seq
    |>  classify_into_set_seq ~key_compare:(Fun.flip compare) (module StrSet)
          ~f:String.length
    (* at each length… *)
    |>  Seq.filter_map begin fun (len, words_of_that_len) ->
          let pairs =
            (* distribute words of that length into anagram buckets (order
             * between buckets and within buckets are irrelevant): *)
            words_of_that_len
            |>  StrSet.to_seq
            |>  classify_into_set_seq (module StrSet) ~f:letter_vector
            (* compute the set of all word pairs within each anagram bucket,
             * and join all buckets for that length: *)
            |>  ~>Seq.fold_left ~init:StrPairSet.empty ~f:begin fun pairs (_v, ana_words) ->
                  ~<StrSet.fold ana_words ~init:pairs ~f:begin fun w0 pairs ->
                    ~<StrSet.fold ana_words ~init:pairs ~f:begin fun w1 pairs ->
                      (* NOTE: (w0,w1) is an anagram [square] pair iff (w0,w1)
                       * is; we keep the symmetry here, we’ll break it later
                       * when computing anagram squares, by imposing that
                       * number(w0) > number(w1). this is so that we can more
                       * easily sort the results by descending square values. *)
                      if w0 != w1 && w0 <> w1 then
                        StrPairSet.add (w0, w1) pairs
                      else
                        pairs
                    end
                  end
                end
          in
          (* discard that length if there are no anagram pairs: *)
          if StrPairSet.is_empty pairs then None else Some (len, pairs)
        end
    |>  List.of_seq
  in
  Printf.printf "Anagramic pairs sorted by descending lengths:\n" ;
  ListLabels.iter ana_pairs_by_len ~f:begin fun (len, pairs) ->
    ~~StrPairSet.iter pairs ~f:begin fun (w0, w1) ->
      Printf.printf "\t%u: %s ~ %s\n" len w0 w1
    end
  end ;
  Printf.printf "Anagramic square pairs sorted by descending values:\n" ;
  (* build the sequence of square numbers up to the required length: *)
  let max_len = fst @@ List.hd ana_pairs_by_len in
  seq_init (truncate @@ 10. ** (float max_len *. 0.5)) (fun r -> r * r)
  (* classify them by descending lengths; for a given length, squares are sorted
   * (we use sets for more efficient lookup, even though lists are enough for
   * solving the problem). *)
  |>  classify_into_set_seq ~key_compare:(Fun.flip compare) (module IntSet)
        ~f:(succ % truncate % log10 % float)
  (* for each length (in descending order): *)
  |>  Seq.iter begin fun (len, squares) ->
        (* select the word pairs of that length, if any:
         * NOTE: we might optimize this lookup by dropping greater lengths, but
         * given that we only consider small lengths, it wouldn’t bring much. *)
        begin match List.assoc_opt len ana_pairs_by_len with
        | None -> ()
        | Some pairs ->
            (* for each square of that length (in descending order)… *)
            ~~Seq.iter (IntSet.to_rev_seq squares) ~f:begin fun sq0 ->
              (* for each word pair of that length… *)
              ~~StrPairSet.iter pairs ~f:begin fun (w0, w1) ->
                (* try to map letters to digits such that the first word is
                 * mapped to this square number: *)
                begin match build_charmap ~src:w0 ~dst:(string_of_int sq0) with
                | None   -> ()
                | Some m ->
                    (* with the same letter-digit mapping, compute the number
                     * associated to the second word:
                     * NOTE: I thought I’d never find a use for [String.map]. *)
                    let w1' = String.map m w1 in
                    let sq1 = int_of_string w1' in
                    (* test whether the resulting number is a lesser square not
                     * starting with '0': *)
                    if sq0 > sq1 && w1'.[0] <> '0' && IntSet.mem sq1 squares then
                      Printf.printf "\t%s (%u = %u²) ~ %s (%u = %u²)\n"
                        w0 sq0 (isqrt sq0)
                        w1 sq1 (isqrt sq1)
                end
              end
            end
        end
      end
*)
