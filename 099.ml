#!/bin/env ocaml

(*
 *  Project Euler #99
 *)

let lt (a1, b1) (a2, b2) =
  log (float a1) *. float b1 < log (float a2) *. float b2

let () =
  let i = ref 0 in
  let i_max = ref 0
  and a_max = ref 1
  and b_max = ref 1 in
  begin try while true do
    Scanf.scanf "%u,%u " @@ fun a b ->
    incr i ;
    if lt (!a_max, !b_max) (a, b) then begin
      a_max := a ;
      b_max := b ;
      i_max := !i
    end
  done with End_of_file ->
    Printf.printf "%u\n" !i_max
  end
