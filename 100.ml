#!/usr/bin/env ocaml

(*
 *  Project Euler #100
 *)

(* [f nmin] returns the first pair (p, n) such that n ⩾ nmin and such that the
 * probability of drawing two blue balls, when drawing two balls from a box that
 * contains p blue balls among n in total, is exactly ½.
 * The last condition amounts to the equation p×(p−1) = n×(n−1)∕2. All solutions
 * of this diophantine equation can be generated from a simple recurrence
 * relation, similarly to the solutions of q² = n×(n−1)∕2, that is, the numbers
 * that are at the same time a square and a triangular number.
 *     https://en.wikipedia.org/wiki/Square_triangular_number#Recurrence_relations
 *)
let f nmin =
  let p0 = ref 1 and n0 = ref 0
  and p1 = ref 1 and n1 = ref 1 in
  while !n1 < nmin do
    let p = 6 * !p1 - !p0 - 2 in
    let n = 6 * !n1 - !n0 - 2 in
    (*assert (n = truncate (float p *. sqrt 2.0)) ;*)
    assert (2 * p * pred p = n * pred n) ;
    p0 := !p1 ; n0 := !n1 ;
    p1 := p   ; n1 := n   ;
  done ;
  (!p1, !n1)

let () =
  assert (f 21 = (15, 21)) ;
  assert (f 22 = (85, 120)) ;
  let (p, n) = f 1_000_000_000_000 in
  Printf.printf "number of blue balls: %u\ntotal number of balls: %u\n" p n
