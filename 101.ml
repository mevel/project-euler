(*
 *  Project Euler #101
 *)

(* on interpole la suite u à partir des k premières valeurs u_0, …, u_{k−1}.
 *
 * # MÉTHODE 1
 *
 * l’interpolation de Newton (écriture de l’interpolant de Lagrange plus
 * pratique pour calculer) donne le polynôme de degré k−1 :
 *   P_k(X)  =  ∑_{j=0}^{k−1}  [u_0, …, u_j] × ∏_{i=1}^j (X−i)
 * ce qui se calcule commodément par la méthode de Horner. [u_0, …, u_j] est la
 * différence divisée, qui est définie dans notre cas par :
 *   [u_i] = u_i
 *   [u_i, …, u_{i+j}] = ([u_{i+1}, …, u_{i+j] - [u_i, …, u_{i+j−1}]) / j
 *
 *   https://fr.wikipedia.org/wiki/Interpolation_newtonienne
 *
 * en notant i_k l’indice du premier indice i tel que P_k(i) ≠ u_i, on a :
 *   soit k = i_k < i_{k+1}
 *   soit k < i_k = i_{k+1}
 * en effet, si k < i_k, alors les polynômes P_k et P_{k+1} coïncident sur les
 * k+1 valeurs u_0, _, u_k donc sont égaux.
 *
 * # MÉTHODE 2
 *
 * on peut simplifier encore le problème : d’après ce qui précède, il suffit de
 * calculer les P_k(k). or, l’interpolation de Lagrange donne :
 *   P_k(X)  =  ∑_{i=0}^{k−1}  u_i × ∏_{j≠i} ((X−j)/(i−j))
 * en particulier, pour X = k, ça se simplifie en :
 *   P_k(k)  =  ∑_{i=0}^{k−1}  u_i × (−1)^{k−1−i} × \binom{k}{i}
 *)

(* load zarith: *)
try Topdirs.dir_directory (Sys.getenv "OCAML_TOPLEVEL_PATH") with Not_found -> () ;;
#use "topfind" ;;
#require "zarith" ;;

module type INPUT = sig
  val u : int -> Z.t
  val n : int
end

(*
 * méthode 1
 *)

module Solve1 (M : INPUT) = struct

  (* on fait commencer la suite à l’indice 0 plutôt que 1 (plus pratique) : *)
  let u i = M.u (i+1)
  (* n est le degré du polynôme u (on utilise donc u_0, …, u_n) : *)
  let n = M.n

  (* précalcul de u_i pour 0 ⩽ i ⩽ n : *)
  let u' = Array.init (n+1) u

  (* pour 0 ⩽ i ⩽ i+j ⩽ n, dd.(i).(j) est [u_i, …, u_{i+j}] : *)
  let dd = Array.init (n+1) (fun i -> Array.make (n-i+1) Q.zero)

  let () =
    for i = 0 to n do
      dd.(i).(0) <- Q.of_bigint u'.(i)
    done ;
    for j = 1 to n do
      for i = 0 to n-j do
        let d1 = dd.(i+1).(j-1)
        and d0 = dd.(i).(j-1) in
        dd.(i).(j) <- Q.( (d1 - d0) / ~$j )
      done
    done

  (* pour 1 ⩽ k ⩽ n, calcule P_k(X) : *)
  let interpol k x =
    let res = ref Q.zero in
    let x = Q.( ~$x ) in
    for j = k-1 downto 0 do
      res := Q.( dd.(0).(j) + (x - ~$j) * !res )
    done ;
    Q.to_bigint !res

  let () =
    let sum = ref Z.zero
    and i = ref 1 in
    for k = 1 to n do
      let y = ref Z.zero in
      while y := interpol k !i ; !i <= n && Z.equal !y u'.(!i) do
        incr i
      done ;
      if !i <= n then
        sum := Z.( !sum + !y )
    done ;
    Printf.printf "%a\n" Z.output !sum

end (* module Solve1 *)

(*
 * méthode 2
 *)

module Solve2 (M : INPUT) = struct

  (* on fait commencer la suite à l’indice 0 plutôt que 1 (plus pratique) : *)
  let u i = M.u (i+1)
  (* n est le degré du polynôme u (on utilise donc u_0, …, u_n) : *)
  let n = M.n

  (* précalcul de u_i pour 0 ⩽ i ⩽ n : *)
  let u' = Array.init (n+1) u

  (* pour 0 ⩽ i ⩽ k ⩽ n+1, binom.(k).(i) est « i parmi k » : *)
  let binom = Array.init (n+2) (fun k -> Array.make (k+1) Z.one)

  let () =
    for k = 2 to n+1 do
      for i = 1 to k-1 do
        binom.(k).(i) <- Z.add binom.(k-1).(i-1) binom.(k-1).(i)
      done
    done

  (* pour 1 ⩽ k ⩽ n, calcule P_k(k) : *)
  let interpol_k k =
    let res = ref Z.zero in
    for i = 0 to k-1 do
      let sgn = if (k+i) mod 2 = 0 then Z.minus_one else Z.one in
      res := Z.( !res + sgn * binom.(k).(i) * u'.(i) )
    done ;
    !res

  let () =
    let sum = ref Z.zero
    and i = ref 1 in
    for k = 1 to n do
      if !i < k then
        i := k ;
      let y = ref Z.zero in
      while y := interpol_k !i ; !i <= n && Z.equal !y u'.(!i) do
        incr i
      done ;
      if !i <= n then
        sum := Z.( !sum + !y )
    done ;
    Printf.printf "%a\n" Z.output !sum

end (* module Solve2 *)

(*
 * résultats
 *)

module Solve = Solve2

module Test = Solve (struct
    let u i = Z.( ~$i ** 3 )
    let n = 10
  end)

module Problem = Solve (struct
    let u i = Z.( (one + ~$i ** 11) /| (one + ~$i) )
    let n = 10
  end)
