#!/usr/bin/env ocaml

(*
 * Project Euler #102
 *)

let (--) (x1, y1) (x2, y2) =
  (x1 - x2, y1 - y2)

let det (x1, y1) (x2, y2) =
  x1 * y2 - x2 * y1

(* [contains p a b c] is true if and only if the DIRECT triangle [a-b-c]
 * strictly contains the point [p]: *)
let direct_contains p a b c =
  assert (det (b -- a) (c -- a) > 0) ;
  det (b -- a) (p -- a) > 0 &&
  det (c -- b) (p -- b) > 0 &&
  det (a -- c) (p -- c) > 0

(* [contains p a b c] is true if and only if the triangle [a-b-c]
 * strictly contains the point [p]: *)
let contains p a b c =
  let d = det (b -- a) (c -- a) in
  if d > 0 then
    direct_contains p a b c
  else if d < 0 then
    direct_contains p a c b
  else
    false

let () =
  let o = (0, 0) in
  assert (contains o (-1, -1) (1, -1) (0, 2)) ;
  assert (contains o (-340, 495) (-153, -910) (835, -947)) ;
  assert (not @@ contains o (-175, 41) (-421, -714) (574, -645)) ;
  let count = ref 0 in
  let input = Scanf.Scanning.open_in "102-triangles" in
  begin try while true do
    Scanf.bscanf input "%i,%i,%i,%i,%i,%i\n" @@fun x1 y1 x2 y2 x3 y3 ->
    if contains o (x1, y1) (x2, y2) (x3, y3) then
      incr count
  done with End_of_file -> () end ;
  Scanf.Scanning.close_in input ;
  Printf.printf "%u\n" !count
