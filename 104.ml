#!/bin/env ocaml

(*
 *  Project Euler #104
 *)

let is_pandigital (n : int) : bool =
  let seen = ref 0 in
  let r = ref n in
  for _ = 1 to 9 do
    let mask = 1 lsl (!r mod 10) in
    seen := !seen lor mask ;
    r := !r / 10
  done ;
  !seen = 0b1111111110 && !r = 0

let () =
  assert (is_pandigital 123456789) ;
  assert (is_pandigital 197648352) ;
  assert (not @@ is_pandigital 1234567890) ;
  assert (not @@ is_pandigital 9876543211) ;
  assert (not @@ is_pandigital 1123456789)

let prefix ~(len : int) (x : float) : int =
  let n = truncate (log10 x) + 1 in
  truncate (x *. 10. ** float (len - n))

let prefix_is_pandigital (x : float) : bool =
  is_pandigital (prefix ~len:9 x)

let ( +: ) a b = (a + b) mod 1_000_000_000

let iter_until (f : int -> float -> bool) : int =
  let rec iter k a a' b b' =
    if f a a' then
      k
    else if a' > 1_000_000_000. && b' > 1_000_000_000. then
      iter (succ k) b (b' /. 10.) (a +: b) ((a' +. b') /. 10.)
    else
      iter (succ k) b b' (a +: b) (a' +. b')
  in
  iter 0 0 0. 1 1.

let () =
  assert (  541 = iter_until (fun a a' -> is_pandigital a)) ;
  assert (2_749 = iter_until (fun a a' -> prefix_is_pandigital a'))

let () =
  Printf.printf "%u\n" (iter_until (fun a a' -> is_pandigital a && prefix_is_pandigital a'))
