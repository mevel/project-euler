#!/usr/bin/env ocaml

(*
 *  Project Euler #107
 *)

#use "topfind"
#require "containers"

(* this is simply computing a minimal spanning tree. *)

let parse_row input =
  input
  |> input_line
  |> String.split_on_char ','
  |> List.map begin function
    | "-" -> None
    | s   -> Some (int_of_string s)
    end

let parse_file filename =
  let rows = ref [] in
  let input = open_in filename in
  begin try while true do
    rows := parse_row input :: !rows
  done with End_of_file -> () end ;
  close_in input ;
  let rows = List.rev !rows in
  let mat = Array.of_list @@ List.map Array.of_list rows in
  let n = List.length rows in
  for i = 0 to pred n do
    assert (Array.length mat.(i) = n) ;
    assert (mat.(i).(i) = None) ;
    for j = 0 to pred i do
      assert (mat.(i).(j) = mat.(j).(i)) ;
      assert (match mat.(i).(j) with Some w -> w > 0 | None -> true) ;
    done
  done ;
  (* assertion: the graph is connected *)
  mat

type edge =
  {
    src : int ;
    dst : int ;
    weight : int ;
  }

type graph = edge list array

let graph_of_matrix mat =
  let n = Array.length mat in
  let g = Array.make n [] in
  for i = 0 to pred n do
    for j = 0 to pred n do
      begin match mat.(i).(j) with
      | Some w -> g.(i) <- { src = i ; dst = j ; weight = w } :: g.(i)
      | None   -> ()
      end
    done
  done ;
  g

let edge_leq e1 e2 =
  e1.weight <= e2.weight

module Heap = CCHeap.Make (struct type t = edge let leq = edge_leq end)

let minimal_spanning_tree g =
  let n = Array.length g in
  assert (n > 0) ;
  let g' = Array.make n [] in
  let heap = ref Heap.empty in
  let seen = Array.make n false in
  let nb_remaining = ref n in
  let mark_seen i =
    assert (not seen.(i)) ;
    seen.(i) <- true ;
    decr nb_remaining ;
    heap := Heap.add_list !heap g.(i)
  in
  mark_seen 0 ;
  while !nb_remaining > 0 do
    let (heap', e) = Heap.take_exn !heap in
    heap := heap' ;
    if not seen.(e.dst) then begin
      mark_seen e.dst ;
      g'.(e.src) <- e :: g'.(e.src)
    end
  done ;
  g'

let total_weight g =
  Array.fold_left begin fun sum edges ->
    List.fold_left (fun sum e -> sum + e.weight) sum edges
  end
    0
    g

let saving mat =
  let g = graph_of_matrix mat in
  (* the weight of the original graph is twice as much as expected because each
   * edge appears twice. *)
  total_weight g / 2 - total_weight (minimal_spanning_tree g)

let example_mat =
  [|
    [| None    ; Some 16 ; Some 12 ; Some 21 ; None    ; None    ; None    |] ;
    [| Some 16 ; None    ; None    ; Some 17 ; Some 20 ; None    ; None    |] ;
    [| Some 12 ; None    ; None    ; Some 28 ; None    ; Some 31 ; None    |] ;
    [| Some 21 ; Some 17 ; Some 28 ; None    ; Some 18 ; Some 19 ; Some 23 |] ;
    [| None    ; Some 20 ; None    ; Some 18 ; None    ; None    ; Some 11 |] ;
    [| None    ; None    ; Some 31 ; Some 19 ; None    ; None    ; Some 27 |] ;
    [| None    ; None    ; None    ; Some 23 ; Some 11 ; Some 27 ; None    |] ;
  |]

let () =
  assert (saving example_mat = 150) ;
  Printf.printf "%u\n" (saving @@ parse_file "107-network")
