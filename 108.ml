#!/usr/bin/env ocaml

(*
 *  Project Euler #108
 *)

(* the equation 1/x + 1/y = 1/n is equivalent to:
 *     (x−n)(y−n) = n²
 * hence counting the number of solutions (x, y) for a given n amounts to
 * counting the number of divisors of n². since we only count solutions with
 * x ≤ y, their number is (d+1) / 2, where d is the number of divisors of n².
 *)

(* this problem is subsumed by #110, where a better solution is presented. here,
 * we simply use a generalized prime sieve to count the number of divisors. *)

type sieve_cell =
  {
    mutable remaining_to_factor : int ;
    (*mutable factors : (int * int) list ;*)
    (*mutable nb_divisors : int ;*)
    mutable nb_divisors_of_square : int ;
  }

let sieve nmax =
  let s = Array.init (succ nmax) (fun n -> {
      remaining_to_factor = n ;
      (*factors = [] ;*)
      (*nb_divisors = 1 ;*)
      nb_divisors_of_square = 1 ;
    }) in
  for n = 2 to nmax do
    if s.(n).remaining_to_factor <> 1 then begin
      for k = 1 to nmax / n do
        let cell = s.(k*n) in
        let count = ref 0 in
        while cell.remaining_to_factor mod n = 0 do
          cell.remaining_to_factor <- cell.remaining_to_factor / n ;
          incr count
        done ;
        (*cell.factors <- (n, !count) :: cell.factors ;*)
        (*cell.nb_divisors <- cell.nb_divisors * (!count + 1) ;*)
        cell.nb_divisors_of_square <- cell.nb_divisors_of_square * (!count * 2 + 1) ;
      done
    end
  done ;
  s

let nmax = 1_000_000

let s = sieve nmax

exception Found of int

let f nb_solutions =
  let nbdivsq = 2 * nb_solutions - 1 in
  begin try
    for n = 1 to nmax do
      if s.(n).nb_divisors_of_square >= nbdivsq then
        raise (Found n)
    done ;
    None
  with Found n ->
    Some n
  end

let () =
  assert (s.(1260).nb_divisors_of_square = 225) ;
  assert (f 100 = Some 1260) ;
  begin match f 1_000 with
  | None   -> assert false
  | Some n -> Printf.printf "%u\n" n
  end
