#!/usr/bin/env ocaml

(*
 *  Project Euler #109
 *)

let (regions, doubles) =
  let simples = Array.init 21 (fun i -> succ i) in
  let triples = Array.init 20 (fun i -> succ i * 3) in
  let doubles = Array.init 21 (fun i -> succ i * 2) in
  simples.(20) <- 25 ;
  doubles.(20) <- 50 ;
  (* we add zero, for missed darts *)
  let regions = Array.concat [ [| 0 |] ; simples ; doubles ; triples ] in
  Array.sort (-) regions ;
  (regions, doubles)

let number_of_checkouts_aux score_to_cancel =
  let nb_regions = Array.length regions in
  let count = ref 0 in
  let exact_count = ref 0 in
  doubles |> Array.iter begin fun a ->
    regions |> Array.iteri begin fun i b ->
      let j = ref i in
      while !j < nb_regions && a + b + regions.(!j) < score_to_cancel do
        incr j
      done ;
      count := !count + (nb_regions - !j) ;
      while !j < nb_regions && a + b + regions.(!j) = score_to_cancel do
        incr exact_count ;
        incr j
      done
    end
  end ;
  (!count, !exact_count)

(* number of checkouts whose total is at least [score]: *)
let number_of_checkouts score =
  fst (number_of_checkouts_aux score)

(* number of checkouts whose total is exactly [score]: *)
let number_of_exact_checkouts score =
  snd (number_of_checkouts_aux score)

(* NOTE: another way to compute it: *)
let number_of_exact_checkouts' score =
  number_of_checkouts score - number_of_checkouts (score + 1)

(* number of checkouts whose total is (strictly) less than [score]: *)
let number_of_checkouts_under score =
  number_of_checkouts 1 - number_of_checkouts score

let () =
  assert (number_of_checkouts 171 = 0) ;
  assert (number_of_checkouts 170 = 1) ;
  assert (number_of_exact_checkouts 6 = 11) ;
  assert (number_of_checkouts 1 = 42336) ;
  for i = 1 to 170 do
    assert (number_of_exact_checkouts i = number_of_exact_checkouts' i)
  done ;
  Printf.printf "%u\n" (number_of_checkouts_under 100)
