#!/usr/bin/env ocaml

(*
 *  Project Euler #110
 *)

#use "topfind"
#require "containers"

let rec list_iter_tail f li =
  begin match li with
  | []      -> ()
  | x :: xs -> f x xs ; list_iter_tail f xs
  end

(* the 25 first prime numbers will be (largely) enough: *)
let primes =
  [  2 ;  3 ;  5 ;  7 ; 11 ; 13 ; 17 ; 19 ; 23 ; 29 ; 31 ; 37 ; 41 ; 43 ; 47 ;
    53 ; 59 ; 61 ; 67 ; 71 ; 73 ; 79 ; 83 ; 89 ; 97 ]

(* this problem is a follow-up of #108. the challenge is still to find the
 * smallest number n whose square has at least a certain amount of divisors. *)

(* the algorithm works with a set of numbers to be examined. we start with a set
 * containing only the number 1 (the empty product). then we repeat the step
 * described below until we find some number that satisfies our criterion
 * (its square has enough divisors). by design, it will be the smallest such
 * number. we need to examine numbers in order, so we implement the set by a
 * min-heap.
 *
 * a step consists in picking the smallest number n (given as a prime
 * factorization) of the set and generating new numbers which remain the
 * smallest possible, but whose square has more divisors than that of n. Each
 * new product is obtained by multiplying n with some prime number (which does
 * not have to be a factor of n).
 *
 * the number of divisors of the squares of the new products are not comparable
 * a priori, which is why we generate several of them. however, it is not
 * necessary to consider every prime number: if two primes p < q have the same
 * multiplicity k in n (including k = 0), then n×p < n×q and the number of
 * divisors of the squares of n×p and n×q are the same. hence, since we want to
 * generate products the smallest possible, is is enough to generate only one
 * new product per multiplicity k, by multiplying n with the smallest prime p
 * with that multiplicity.
 *
 * therefore, for each product n already reached, we store its value and its
 * prime factorization, under the form:
 *     [ (0, primes_0) ; (1, primes_1) ; … ; (kmax, primes_kmax) ]
 * where primes_k is the ordered list of the primes whose multiplicity in n is k
 * (primes_0 being the primes not present in n).
 *
 * in fact, with the technique explained so far, we get duplicate products; this
 * is because choices commute. for example, we get 12 twice, because
 * 12 = 1×2×2×3 = 1×2×3×2:
 *             1
 *             ↓ ×2
 *             2
 *           ↙×2 ↘ ×3
 *         4       6
 *       ↙×2 ↘×3 ↙×2 ↘ ×5
 *     8       12      30
 * to break symmetry, once we have chosen a prime p with multiplicity k in n, we
 * do not allow ourselves anymore to choose primes with lower multiplicities.
 * in the example, this means that the only path that leads to 12 is the right
 * one: 12 = 1×2×3×2. to implement this technique, we simply drop primes with
 * lower multiplicities (this is the effect of [list_iter_tail]).
 *)

type heap_element =
  {
    value : int ;
    factors_per_multiplicity : (int * int list) list ;
    (*nb_divisors : int ;*)
    nb_divisors_of_square : int ;
  }

let leq elt1 elt2 =
  elt1.value <= elt2.value

module Heap = CCHeap.Make (struct type t = heap_element let leq = leq end)

exception Found of int

let f nb_solutions =
  let nbdivsq = 2 * nb_solutions - 1 in
  (* initialize the heap: *)
  let heap = ref @@ Heap.of_list [
      {
        value = 1 ;
        factors_per_multiplicity = [ (0, primes) ] ;
        nb_divisors_of_square = 1 ;
      }
    ]
  in
  (* repeat as many times as needed: *)
  begin try
    while true do
      (* pick the lowest product: *)
      let (heap', elt) = Heap.take_exn !heap in
      heap := heap' ;
      (* if it satisfies the condition, stop: *)
      if elt.nb_divisors_of_square >= nbdivsq then
        raise (Found elt.value) ;
      (* otherwise, generate one new product per multiplicity: *)
      elt.factors_per_multiplicity |> list_iter_tail begin fun (k, primes_k) factors' ->
        begin match primes_k with
        | []             -> ()
        | p :: primes_k' ->
            let (primes_k1, factors'') =
              begin match factors' with
              | []                           -> ([], [])
              | (k1, primes_k1) :: factors'' ->
                  (* for the sake of simplicity, we do not “garbage-collect”
                   * multiplicities with no corresponding prime, so that the
                   * following invariant holds: *)
                  assert (k+1 = k1) ;
                  (primes_k1, factors'')
              end
            in
            let elt' = {
              value = elt.value * p ;
              factors_per_multiplicity = (k, primes_k') :: (k+1, primes_k1@[p]) :: factors'' ;
              (*nb_divisors = elt.nb_divisors * (k+2) / (k+1) ;*)
              nb_divisors_of_square = elt.nb_divisors_of_square * (2*k+3) / (2*k+1) ;
            } in
            heap := Heap.add !heap elt'
        end
      end
    done ;
    assert false
  with Found n ->
    n
  end

let () =
  assert (f 100 = 1260) ;
  assert (f 1_000 = 180_180) ; (* answer of Problem #108 *)
  Printf.printf "%u\n" (f 4_000_000)
