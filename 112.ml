#!/usr/bin/env ocaml

(*
 * Project Euler #112
 *)

let is_increasing n =
  let r = ref n in
  let next_digit = ref 9 in
  let increasing = ref true in
  while !r > 0 do
    let digit = !r mod 10 in
    increasing := !increasing && digit <= !next_digit ;
    next_digit := digit ;
    r := !r / 10
  done ;
  !increasing

let is_decreasing n =
  let r = ref n in
  let next_digit = ref 0 in
  let decreasing = ref true in
  while !r > 0 do
    let digit = !r mod 10 in
    decreasing := !decreasing && digit >= !next_digit ;
    next_digit := digit ;
    r := !r / 10
  done ;
  !decreasing

let is_bouncy n =
  not (is_increasing n || is_decreasing n)

(* [first_with_proportion p] is the first integer [n] such that exactly
 * [p] percent of the integers between 1 and [n], included, are bouncy: *)
let first_with_proportion p =
  let count = ref 0 in
  let n = ref 100 in (* there are no bouncy numbers below 100 *)
  while p * !n <> !count * 100 do
    incr n ;
    if is_bouncy !n then
      incr count
  done ;
  !n

let () =
  assert (first_with_proportion 50 = 538) ;
  assert (first_with_proportion 90 = 21_780) ;
  Printf.printf "%u\n" (first_with_proportion 99)
