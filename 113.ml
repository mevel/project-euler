#!/usr/bin/env ocaml

(*
 *  Project Euler #113
 *)

let binom n p =
  assert (0 <= p && p <= n) ;
  let p = min p (n - p) in
  let m = n - p in
  let c = ref 1 in
  for k = 1 to p do
    c := !c * m / k  +  !c
  done ;
  !c

let f k =
  binom (k + 9) 9 * (k + 20) / 10 - 10 * k - 2

let () =
  assert (f 6 = 12_951) ;
  assert (f 10 = 277_032) ;
  Printf.printf "%u\n" @@ f 100
