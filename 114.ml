#!/usr/bin/env ocaml

(*
 *  Project Euler #114
 *)

(* followed by #115, #116, #117 *)

let f k n =
  assert (0 < k && 0 <= n) ;
  let mem = Array.make (succ n) 1 in
  for m = k to n do
    (* with no red block starting on the first cell: *)
    let sum = ref mem.(m-1) in
    (* with a red block of length k <= l < m starting on the first cell: *)
    for l = k to m-1 do
      sum := !sum + mem.(m-l-1)
    done ;
    (* with a red block of length m starting on the first cell: *)
    sum := !sum + 1 ;
    mem.(m) <- !sum
  done ;
  mem.(n)

let () =
  assert (f 3 7 = 17) ;
  assert (f 3 29 = 673_135) ;    (* example from #115 *)
  assert (f 3 30 = 1_089_155) ;  (* example from #115 *)
  assert (f 10 56 = 880_711) ;   (* example from #115 *)
  assert (f 10 57 = 1_148_904) ; (* example from #115 *)
  Printf.printf "%u\n" (f 3 50)
