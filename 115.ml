#!/usr/bin/env ocaml

(*
 *  Project Euler #115
 *)

(* follow-up of #114, followed by #116, #117 *)

let f k n =
  assert (0 < k && 0 <= n) ;
  let mem = Array.make (succ n) 1 in
  for m = k to n do
    (* with no red block starting on the first cell: *)
    let sum = ref mem.(m-1) in
    (* with a red block of length k <= l < m starting on the first cell: *)
    for l = k to m-1 do
      sum := !sum + mem.(m-l-1)
    done ;
    (* with a red block of length m starting on the first cell: *)
    sum := !sum + 1 ;
    mem.(m) <- !sum
  done ;
  mem.(n)

let g k threshold =
  let n = ref 1 in
  while f k !n < threshold do
    incr n
  done ;
  !n

let threshold = 1_000_000

let () =
  assert (f 3 7 = 17) ; (* example from #114 *)
  assert (f 3 29 = 673_135) ;
  assert (f 3 30 = 1_089_155) ;
  assert (f 10 56 = 880_711) ;
  assert (f 10 57 = 1_148_904) ;
  assert (g 3 threshold = 30) ;
  assert (g 10 threshold = 57) ;
  Printf.printf "%u\n" (g 50 threshold)
