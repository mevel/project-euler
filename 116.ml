#!/usr/bin/env ocaml

(*
 *  Project Euler #116
 *)

(* follow-up of #114, #115, followed by #117 *)

let f_exactly k n =
  assert (0 < k && 0 <= n) ;
  let mem = Array.make (succ n) 1 in
  for m = k to n do
    (* either there is no block starting on the first cell,
     * or there is a block of length k starting on the first cell: *)
    mem.(m) <- mem.(m-1) + mem.(m-k)
  done ;
  mem.(n)

(* minus 1 because we do not want to count the solution with no block: *)
let f_red n = f_exactly 2 n - 1
let f_green n = f_exactly 3 n - 1
let f_blue n = f_exactly 4 n - 1

let f_rgb n = f_red n + f_green n + f_blue n

let () =
  assert (f_red 5 = 7) ;
  assert (f_green 5 = 3) ;
  assert (f_blue 5 = 2) ;
  assert (f_rgb 5 = 12) ;
  Printf.printf "%u\n" (f_rgb 50)
