#!/usr/bin/env ocaml

(*
 *  Project Euler #117
 *)

(* follow-up of #114, #115, #116 *)

let f_exactly k n =
  assert (0 < k && 0 <= n) ;
  let mem = Array.make (succ n) 1 in
  for m = k to n do
    (* either there is no block starting on the first cell,
     * or there is a block of length k starting on the first cell: *)
    mem.(m) <- mem.(m-1) + mem.(m-k)
  done ;
  mem.(n)

let f_rgb_mix n =
  assert (0 <= n) ;
  let mem = Array.make (succ n) 1 in
  if n >= 2 then  mem.(2) <- mem.(1) + mem.(0) ;           (* = 2 *)
  if n >= 3 then  mem.(3) <- mem.(2) + mem.(1) + mem.(0) ; (* = 4 *)
  for m = 4 to n do
    (* either there is no block starting on the first cell,
     * or there is a red block (of length 2) starting on the first cell,
     * or there is a green block (of length 3) starting on the first cell,
     * or there is a blue block (of length 4) starting on the first cell: *)
    mem.(m) <- mem.(m-1) + mem.(m-2) + mem.(m-3) + mem.(m-4)
  done ;
  mem.(n)

let () =
  assert (f_rgb_mix 5 = 15) ;
  Printf.printf "%u\n" (f_rgb_mix 50)
