#!/usr/bin/env ocaml

(*
 * Project Euler #120
 *)

(* by Newton’s binomial formula, we find:
 *     r(a, n)  =[a²]=  (1 + n a) + (1 − n a) (−1)^n
 * so that:
 *     r(a, n)  =[a²]=  2        if n is even
 *     r(a, n)  =[a²]=  2 n a    if n is odd
 * for a ⩾ 3, we have that 2 < 2a < a², so that r(a, 1) = 2a is greater than
 * r(a, n) = 2 for any even n.
 * moreover, r(a, _) is a'-periodic where a' = lcm(a, 2).
 *
 * then, for a ⩾ 3, in order to find n that maximize r(a, n), it is enough to
 * search for an odd n such that 1 ⩽ n < a'.
 *
 * if a is even, then a' = a, and we can describe r(a, _) piecewise:
 *     (0) if n is odd and 0   ⩽ n < a/2 then r(a, n) = 2 n a
 *     (1) if n is odd and a/2 ⩽ n < a   then r(a, n) = 2 n a − a²
 * on each interval, r(a, n) increases with n, so the maximum is either the
 * last value of the first interval or that of the second.
 * second interval: a being even, a−1 is odd, so the last value is
 *     r₁ = r(a, n−1)     = (a−2) a
 * first interval: if a =[4]= 0 then a/2 is even, so the last value is
 *     r₀ = r(a, a/2 − 1) = (a−2) a
 * otherwise, a/2 is odd, so the last value is
 *     r₀ = r(a, a/2 − 2) = (a−4) a
 * in either case, the maximum of r(a, _) is
 *     rmax(a) = max(r₀, r₁) = (a−2) a
 *
 * if a is odd, then a' = 2 a, and again we can describe r(a, _) piecewise:
 *     (1) if n is odd and 0    ⩽ n < a/2  then r(a, n) = 2 n a
 *     (1) if n is odd and a/2  ⩽ n < a    then r(a, n) = 2 n a − a²
 *     (2) if n is odd and a    ⩽ n < 3a/2 then r(a, n) = 2 n a − 2a²
 *     (3) if n is odd and 3a/2 ⩽ n < 2a   then r(a, n) = 2 n a − 3a²
 * same reasoning… we find:
 *     r₁ = r(a, a − 2)    = (a−4) a
 *     r₃ = r(a, 2a − 1)   = (a−2) a
 * and, if a =[4]= 1 then
 *     r₀ = r(a, (a−3)/2)  = (a−3) a
 *     r₂ = r(a, (3a−1)/2) = (a−1) a
 * otherwise a =[4]=3 and
 *     r₀ = r(a, (a−1)/2)  = (a−1) a
 *     r₂ = r(a, (3a−3)/2) = (a−3) a
 * in either case, the maximum of r(a, _) is
 *     rmax(a) = max(r₀, r₁, r₂, r₃) = (a−1) a
 *
 * conclusion:
 *     rmax(1) = 0
 *     rmax(2) = 2
 *     rmax(a) = (a−1) a    if a is odd
 *     rmax(a) = (a−2) a    if a is even
 *)

let rmax a =
  assert (a > 0) ;
  if a = 1 then
    0
  else if a = 2 then
    2
  else if a mod 2 = 0 then
    a * (a - 2)
  else
    a * (a - 1)

let () =
  let s = ref 0 in
  for a = 3 to 1_000 do
    s := !s + rmax a
  done ;
  Printf.printf "%u\n" !s
