#!/usr/bin/env ocaml

(*
 *  Project Euler #128
 *)

(*                    d = 1
 *
 *               26——25——24——23
 *              /              \
 *    d = 2    27  12——11——10  22     d = 0
 *            /   /          \   \
 *           28  13  04——03  09  21
 *          /   /   /      \   \   \
 *         29  14  05  01  02__08__20__
 *          \   \   \      _/  _/  _/
 *           30  15  06——07  19  37
 *            \   \          /   /
 *    d = 3    31  16——17——18  36     d = 5
 *              \              /
 *               32——33——34——35
 *
 *                    d = 4
 *
 * the numbering goes in concentric hexagonal layers. the first layer (radius 0)
 * contains only the value 01; the second layer (radius 1) contains the values
 * 02 to 07; the third layer (radius 2) contains the values 08 to 17; and so on.
 *
 * in general, the layer of radius r ≥ 1 contains 6×r values (6×r is the
 * perimeter of the hexagon), its first value is 2 + 3×r×(r−1).
 *
 * each value can be identified with the radius r of its layer, and its offset
 * 0 ≤ D < 6×r from the first value of that layer. we can write the euclidean
 * division D = d×r + a, where 0 ≤ d < 6 is the edge of the hexagon, or the
 * general “direction” (see the drawing above) and 0 ≤ a < r is the “angle”,
 * that is, the offset from the first value of that edge.
 *
 * for example, the edge d = 4 of the layer of radius r = 3 contains the three
 * values 32, 33, 34, with respective angles 0, 1, 2.
 *
 * we call the coordinates (r, d, a) of a value its polar coordinates.
 *
 * we are looking for values such that, among the differences between that value
 * and each of its six neighbours, there are three prime numbers. so let’s
 * examine the neighbours of some value N of layer r. there are several cases.
 *
 * CASE 1a: N is neither at a corner or the last value of the layer:
 *
 *     in this case, the six neighbours of N are as follows:
 *         — two consecutive values of layer r−1,
 *         — the previous and next values of layer r,
 *         — two consecutive values of layer r+1.
 *     the differences of the value N at (r, d, a≠0) with its neighbours are:
 *                      +1     +(6r+d+1)
 *                        \   /
 *                         \ /
 *         −(6(r−1)+d) ———— 0 ———— +(6r+d)
 *                         / \
 *                        /   \
 *           −(6(r−1)+d+1)     −1
 *     because the two neighbours of layer r−1 are consecutive, there is at most
 *     one prime among them. the same goes for the two neighbours of layer r+1.
 *     and the differences ±1 with the previous and next values of the same
 *     layer are not prime. so there can be at most two primes there, which is
 *     not enough. therefore, we do not need to consider such values.
 *
 * CASE 2a: N is at a corner, but is not the first value of the layer:
 *
 *     in this case, the six neighbours of N are as follows:
 *         — one value of layer r−1 (the corresponding corner),
 *         — the previous and next values of layer r,
 *         — three consecutive values of layer r+1.
 *     the differences of the value N at (r, d≥1, 0) with its neighbours are:
 *               +(6r+d+1)     +(6r+d)
 *                        \   /
 *                         \ /
 *                  +1 ———— 0 ———— +(6r+d−1)
 *                         / \
 *                        /   \
 *             −(6(r−1)+d)     −1
 *     among the three consecutive differences 6r+d−1, 6r+d, 6r+d+1, at most two
 *     can be prime. hence, in order to have three primes, these three
 *     differences must be prime: 6(r−1)+d, 6r+d−1 and 6r+d+1.
 *         + if d = 2, 3 or 4, then 6(r−1)+d is not prime (divisible by 2 or 3).
 *         + if d = 1, then 6r+d−1 is not prime (divisible by 6).
 *         + if d = 5, then 6r+d+1 is not prime (divisible by 6).
 *     therefore, there cannot be three primes in this configuration, so we do
 *     not need to consider such values either.
 *
 * CASE 1b: N is the last value of the layer:
 *
 *     in this case, the six neighbours of N are as follows:
 *         — the last and first values of layer r−1,
 *         — the penultimate and first values of layer r,
 *         — the penultimate and last values of layer r+1.
 *     the differences of the value N at (r, 5, r−1) with its neighbours are:
 *                 −(6r−1)     +(6r+6)
 *                        \   /
 *                         \ /
 *            −(12r−7) ———— 0 ———— +(6r+5)
 *                         / \
 *                        /   \
 *                      6r     −1
 *     among these differences, the only three that may be prime are 6r−1, 6r+5
 *     and 12r−7. so we only need to test whether these three values are prime.
 *
 * CASE 2b: N is the first value of the layer:
 *
 *     in this case, the six neighbours of N are as follows:
 *         — one value of layer r−1 (the first one),
 *         — the last and second values of layer r,
 *         — the last, first and second values of layer r+1.
 *     the differences of the value N at (r, 0, 0) with its neighbours are:
 *                 +(6r+1)     +(6r)
 *                        \   /
 *                         \ /
 *                  +1 ———— 0 ———— +(12r+5)
 *                         / \
 *                        /   \
 *                 −(6r−6)     +(6r−1)
 *     among these differences, the only three that may be prime are 6r−1, 6r+1
 *     and 12r+5. so we only need to test whether these three values are prime.
 *
 * to conclude, we only need to test the first and last values of each layer.
 *     + in the first case, we have to test the primality of:
 *           6r−1, 6r+1, 12r+5
 *     + in the second case, we have to test the primality of:
 *           6r−1, 6r+5, 12r−7
 *
 * as compared with testing naively every value, this represents a quadratic
 * speedup.
 *)

#use "lib/primality.ml"

(* naive implementation: tests naively all values (and uses the polar
 * coordinates, as well as a conversion to some cartesian coordinates).
 * too slow…
 *)

(*
type polar_coord =
  {
    radius : int ;
    direction : int ;
    angle : int ;
  }

let polar_origin =
    { radius = 0 ; direction = 0 ; angle = 0 }

let test_polar p =
  assert (p.radius >= 0) ;
  assert (0 <= p.direction && p.direction < 6) ;
  assert (p.radius = 0 || 0 <= p.angle && p.angle < p.radius)

let polar_of_ordinal n =
  assert (n >= 1) ;
  if n = 1 then
    polar_origin
  else begin
    let r = truncate ((1. +. sqrt (1. +. 4./.3. *. float (n-2))) /. 2.) in
    assert (r >= 1) ;
    let n' = n - 2 - 3 * r * (r-1) in
    { radius = r ; direction = n' / r ; angle = n' mod r }
  end

let ordinal_of_polar p =
  if p.radius = 0 then
    1
  else begin
    let r = p.radius in
    2+3*r*(r-1) + p.direction*r + p.angle
  end

let () =
  for n = 1 to 1_000 do
    test_polar (polar_of_ordinal n) ;
    assert (n = ordinal_of_polar @@ polar_of_ordinal n)
  done

let polar_of_cartesian (x, y) =
  if x = 0 && y = 0 then
    polar_origin
  else if 0 < x && 0 <= y then
    { radius = x+y ; direction = 0 ; angle = y }
  else if 0 <= -x && -x < y then
    { radius = y ; direction = 1 ; angle = -x }
  else if 0 < y && y <= -x then
    { radius = -x ; direction = 2 ; angle = -x - y }
  else if x < 0 && y <= 0 then
    { radius = -x-y ; direction = 3 ; angle = -y }
  else if 0 <= x && x < -y then
    { radius = -y ; direction = 4 ; angle = x }
  else let () = assert (0 < -y && -y <= x) in
    { radius = x ; direction = 5 ; angle = x - -y }

let cartesian_of_polar p =
  if p.radius = 0 then
    (0, 0)
  else begin
    begin match p.direction with
    | 0 -> (p.radius - p.angle, p.angle)
    | 1 -> (-p.angle, p.radius)
    | 2 -> (-p.radius, p.radius - p.angle)
    | 3 -> (-p.radius + p.angle, -p.angle)
    | 4 -> (p.angle, -p.radius)
    | 5 -> (p.radius, -p.radius + p.angle)
    | _ -> assert false
    end
  end

let () =
  for x = 1 to 1_000 do
    for y = 1 to 1_000 do
      test_polar (polar_of_cartesian (x, y)) ;
      assert ((x, y) = cartesian_of_polar @@ polar_of_cartesian (x, y)) ;
    done
  done

let neighbours_cartesian (x, y) =
  [ (x-1, y) ; (x+1, y) ; (x, y-1) ; (x, y+1) ; (x-1, y+1) ; (x+1, y-1) ]

let neighbours n =
  n
  |> polar_of_ordinal
  |> cartesian_of_polar
  |> neighbours_cartesian
  |> List.map polar_of_cartesian
  |> List.map ordinal_of_polar
  |> List.sort (-)

let () =
  assert (neighbours 8 = [ 2 ; 9 ; 19 ; 20 ; 21 ; 37 ]) ;
  assert (neighbours 17 = [ 6 ; 7 ; 16 ; 18 ; 33 ; 34 ])

let pd n =
  n
  |> neighbours
  |> List.fold_left (fun count n' -> if int_is_prime (abs (n - n')) then succ count else count) 0

let () =
  assert (pd 8 = 3) ;
  assert (pd 17 = 2) ;
  assert (pd 271 = 3)

let nth rank =
  assert (rank >= 1) ;
  let rank = ref rank in
  let exception Found of int in
  begin try for n = 1 to max_int do
    if pd n = 3 then begin
      decr rank ;
      if !rank = 0 then
        raise (Found n)
    end
  done ; -1 with Found n ->
    n
  end

let () =
  assert (nth 10 = 271) ;
  (* note: nth 100 = 1790270 *)
  Printf.printf "%u\n" (nth 2_000)
*)

(* smart implementation: tests only the first and last values of each layer
 * (and does not use intermediate representations).
 * very fast. *)

let nth rank =
  assert (rank >= 1) ;
  let rank = ref rank in
  let exception Found of int in
  begin try for r = 1 to max_int do
    let test1 = int_is_prime (6*r-1) in
    if test1 && int_is_prime (6*r+1) && int_is_prime (12*r+5) then begin
      decr rank ;
      if !rank = 0 then
        raise (Found (2 + 3*r*(r-1)))
    end ;
    if test1 && int_is_prime (6*r+5) && int_is_prime (12*r-7) then begin
      decr rank ;
      if !rank = 0 then
        raise (Found (1 + 3*(r+1)*r))
    end ;
  done ;
    -1
  with Found n ->
    n
  end

let () =
  assert (nth 10 = 271) ;
  (* note: nth 100 = 1790270 *)
  Printf.printf "%u\n" (nth 2_000)
