/*
 *  Project Euler  #144
 */
#include <math.h>
#include <stdio.h>
#include <stdbool.h>

/* In all the following code, the white cell is taken to have the equation:
 *                            4x² + y = 100.
 * and be opened at the top, for -0.01 ≤ x ≤ 0.01. This code can not run another
 * cell without modifications.
 */



/* Compares two floating-point numbers for equality (we must not use == since
 * floating-point results are approximate). */
bool  float_eq
  (double a, double b)
{
	return fabs(a-b) <= 0.0001;
}



/* Computes the intersection of the cell with the beam of given equation
 * (ax+by+c=0), starting at position (prevx,prevy). */
void  intersect
  (double a, double b, double c, double prevx, double prevy, double* x, double* y)
{
	/* Vertical line. */
	if(float_eq(b, 0.0)) {
		*x = -c / a;
		*y = -prevy;
	}
	/* Horizontal line. */
	else if(float_eq(a, 0.0)) {
		*y = -c / b;
		*x = -prevx;
	}
	/* Any line. */
	else {
		*x = (-a*c - 2*b*sqrt(25*a*a+100*b*b-c*c))  /  (a*a + 4*b*b);
		/* In fact, there are two solutions: the starting point and the point
		   we are looking for. The previous computation potentially led us to
		   our initial point, so we must check, and give the other solution if
		   this is the case. */
		if(float_eq(*x, prevx))
			*x = (-a*c + 2*b*sqrt(25*a*a+100*b*b-c*c))  /  (a*a + 4*b*b);
		*y = - (a*(*x) + c) / b;
	}
}



/* Do the reflections of the laser beam starting at (x0,y0) and hitting the cell
 * for first time at (x1,y1); Returns the number of reflections before exiting.
 */
unsigned  reflect_beam
  (double x0, double y0, double x1, double y1)
{
	unsigned count;
	double a, b, c;
	double tmp1, tmp2, tmp3, tmp4;
	
	count = 0;
	while(fabs(x1) > 0.01  ||  y1 < 0) {
		/* A few math here. :) */
		tmp1 = 8*x1*y1;
		tmp2 = 16*x1*x1 - y1*y1;
		tmp3 = x1-x0;
		tmp4 = y1-y0;
		a = tmp1*tmp3 - tmp2*tmp4;
		b = - tmp1*tmp4 - tmp2*tmp3;
		c = - a*x1 - b*y1;
		x0 = x1;
		y0 = y1;
		intersect(a, b, c, x0, y0, &x1, &y1);
		count++;
		printf("(%f, %f)  ->  (%f,%f)\n", x0, y0, x1, y1);
	}
	
	return count;
}



int main(void)
{
	printf("\nnumber of reflections of the laser beam before exiting: %u\n",
	  reflect_beam(0.0, 10.1,  1.4, -9.6));
	
	return 0;
}
