#require "zarith" ;;

(* computes the binary form of an integer, as the list of positions of bits set
 * eg· 2^17 + 2^13 + 2^4 + 1 → [17 ; 13 ; 4 ; 0] *)
(*
let bits_of_integer =
  let rec aux k res n =
    if n = 0 then
      res
    else if n mod 2 = 0 then
      aux (succ k) res (n / 2)
    else
      aux (succ k) (k :: res) (n / 2)
  in
  aux 0 []
*)
(* with zarith: *)
let bits_of_integer n =
  let li = ref [] in
  for i = 0 to pred (Z.numbits n) do
    if Z.testbit n i then
      li := i :: !li
  done ;
  !li

(*let bits_5p25 = bits_of_integer Z.(to_int (pow ~$5 25))
let bits_10p25 = List.map ((+) 25) bits_5p25*)
let bits_10p25 = bits_of_integer Z.(pow ~$10 25)
  (* = [83; 78; 74; 72; 71; 68; 66; 64; 60; 58; 57; 52; 50; 40; 38; 35; 30; 27; 25] *)

let rec delta_list = function
  | []                    -> []
  | x :: []               -> x :: []
  | x :: (y :: _ as tail) -> (x - y) :: delta_list tail

let count_decompositions_aux bits =
  let f = ref 1
  and g = ref 1 in
  bits
  |> delta_list
  |> List.rev
  |> List.iter begin fun di ->
    let f' = di * !f + !g
    and g' = (pred di) * !f + !g in
    f := f' ;
    g := g'
  end ;
  (!f, !g)

let count_decompositions_of bits = fst @@ count_decompositions_aux bits
let count_strict_decompositions_of bits = snd @@ count_decompositions_aux bits

let () =
  for i = 0 to 10 do
    Printf.printf "f %i = %i\n" i (count_decompositions_of @@ bits_of_integer @@ Z.of_int i)
  done

let () =
  Printf.printf "f 10^25 = %i\n" (count_decompositions_of bits_10p25)
