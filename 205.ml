#!/usr/bin/env ocaml

(*
 * Project Euler #205
 *)

(* [score_distrib ~faces:f ~dice:d] returns an array [p] such that [p.(s)] is
 * the probability that the sum of [d] independent [f]-sided dice, whose faces
 * are numbered [1, …, d], is exactly [s]. *)
let score_distrib ~faces ~dice =
  let max_score = faces * dice in
  (* dynamic programming *)
  let old_distrib = ref @@ Array.make (max_score + 1) 0.0 in
  let cur_distrib = ref @@ Array.make (max_score + 1) 0.0 in
  !cur_distrib.(0) <- 1.0 ;
  for _ = 1 to dice do
    let distrib  = !cur_distrib
    and distrib' = !old_distrib in
    old_distrib := distrib ;
    cur_distrib := distrib' ;
    for s = 0 to max_score do
      distrib'.(s) <- 0.0 ;
      for a = 1 to min faces s do
        distrib'.(s) <- distrib'.(s) +. distrib.(s-a) /. float faces
      done
    done
  done ;
  !cur_distrib

let () =
  let distrib_pete  = score_distrib ~faces:4 ~dice:9
  and distrib_colin = score_distrib ~faces:6 ~dice:6 in
  let sum = ref 0.0 in
  for u = 1 to 36 do
    for v = u+1 to 36 do
      sum := !sum +. distrib_colin.(u) *. distrib_pete.(v)
    done
  done ;
  Printf.printf "%.7f\n" !sum
