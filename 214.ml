(*! #!/usr/bin/env ocaml !*)

(*
 *  Project Euler #214
 *)

(* Algorithm: run a factorizing sieve (factorizations are stored implicitely:
 * we only store one prime factor for each integer, which saves memory and is
 * enough for unfolding the factorizations). The sieve makes it easy to iterate
 * on primes, and the factorizations make it easy to compute φ. *)

let f ~chain_len ~n_max =
  (* [some_factor.(n)] is the smallest prime factor of [n], or [n] itself if it
   * is prime or if we haven’t found a factor yet: *)
  let some_factor = Array.init (n_max+1) Fun.id in
  (* for debugging: *)
  let rec print_factorization n =
    assert (1 <= n) ;
    let p = some_factor.(n) in
    assert (n mod p = 0) ;
    if p = n then
      Printf.printf "%u\n" p
    else begin
      Printf.printf "%u × " p ;
      print_factorization (n / p) ;
    end
  in
  let _print_factorization n =
    Printf.printf "%u = " n ;
    print_factorization n
  in
  (* [phi] is memoized: *)
  let mem_phi = Array.make (n_max+1) 0 in
  mem_phi.(1) <- 1 ;
  let rec phi n =
    assert (1 <= n) ;
    if mem_phi.(n) = 0 then begin
      let p = some_factor.(n) in
      assert (n mod p = 0) ;
      let n' = n / p in
      let m = phi n' in
      (* NOTE: here [phi] assumes that repeated prime factors are presented
       * consecutively, hence it is important that [some_factor] consistently
       * contains not just a prime factor but the smallest (or greatest) one.
       * that’s easily remediable at the cost of more divisions: just replace
       * the test [p = some_factor.(n')] with [n' mod p = 0]: *)
      mem_phi.(n) <- if p = some_factor.(n') then p * m else (p-1) * m ;
    end ;
    mem_phi.(n)
  in
  let rec length_of_phi_chain n =
    if n = 1 then 1 else 1 + length_of_phi_chain (phi n)
  in
  let sum = ref 0 in
  for k = 1 to n_max / 2 do
    some_factor.(k * 2) <- 2
  done ;
  for n = 3 to n_max do
    if some_factor.(n) = n then begin (* if [n] is prime: *)
      (* sieve multiples of [n]:
       * NOTE: to be able to apply the usual optimization of starting at [k = n]
       * and only iterating on odd [k], we need that [some_factor] contains the
       * *smallest* prime factors, rather than the greatest ones (in the latter
       * case, the optimization would break the invariant). *)
      (*! for k = 2 to n_max / n do !*)
      for k' = n/2 to (n_max / n - 1) / 2 do
        let k = 2*k'+1 in
        (* NOTE: this test avoids overwriting an already recorded factor, thus
         * ensuring that [some_factor] always stores the *smallest* prime factor,
         * and that factors in the implied factorizations are sorted.
         * how the test works: we had already found a factor for [k * n] iff we
         * had already found one for [k], iff we now know a factor for [k] and
         * this factor is not [n] (which we would have just found).
         * so: [n] is the first factor we find for [k * n] iff we have not found
         * any factor for [k] below [n] (note that [k > n]). *)
        if some_factor.(k) >= n then
          some_factor.(k * n) <- n ;
      done ;
      (* do what the specific problem asks, i.e. compute the chain length: *)
      if length_of_phi_chain n = chain_len then
        sum := !sum + n ;
    end ;
    (*! _print_factorization n ; !*)
  done ;
  Printf.printf "sum = %u\n" !sum

(*! let chain_len = 4 !*)
(*! let n_max = 1000 !*)
let chain_len = 25
let n_max = 40_000_000

let () =
  f ~chain_len ~n_max
