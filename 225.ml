(* On considère la suite :
 *     T(1) = T(2) = T(3) = 1
 *     T(n) = T(n-1) + T(n-2) + T(n-3)
 * Pour un entier d, on peut déterminer en temps fini si d ne divise
 * aucun élément de la suite, de la façon suivante :
 *     U(n) = T(n) mod d
 * On regarde la suite à valeurs dans (ℤ/dℤ)³ :
 *     V(n) = (U(n),U(n+1),U(n+2))
 * Elle est définie par récurrence simple :
 *     V(1) = (1,1,1)
 *     V(n) = f(V(n-1))    où  f(a,b,c) = (b,c,a+b+c)
 * et son ensemble de valeurs est fini, donc elle boucle. De plus, f
 * est inversible (f⁻¹(b,c,d) = (d-b-c,b,c)), opour donc V(n) donné il
 * n’y a qu’un seul V(n-1) possible, ce qui implique que le cycle passe
 * par l’élément initial (1,1,1).
 * Il suffit donc de chercher un diviseur de d dans ce cycle.
 *)

let divides_at_least_one d =
  let rec cycle = function
    | (_,_,0) -> true
    | (1,1,1) -> false
    | (a,b,c) -> cycle (b, c, (a+b+c) mod d)
  in
  cycle (1,1,3)

let kth_odd_number_not_dividing_any k =
  let d = ref 1
  and count = ref 0 in
  while !count < k do
    d := !d + 2;
    if not (divides_at_least_one !d) then begin
      incr count;
      Printf.printf "%3u°: %u\n" !count !d
    end
  done;
  !d

let () =
  kth_odd_number_not_dividing_any 124 |> Printf.printf "answer: %u\n"
