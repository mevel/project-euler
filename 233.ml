(*
 *  Project Euler #233
 *)


(* Utilise le théormème des deux carrés de Fermat pour compter les triplets
 * pythagoriciens (x,y,z) avec z donné.
 *            http://fr.wikipedia.org/wiki/Théorème_des_deux_carrés_de_Fermat
 *
 * Pour n entier, n peut s’écrire comme somme de deux carrés si et seulement
 * si les valuations dans n des facteurs premiers congrus à 3 modulo 4 sont
 * paires. Alors, si a1, …, ak sont les valuations dans n des facteurs premiers
 * congrus à 1 modulo 4, alors en posant m = (a1+1)…(ak+1), le nombre de
 * décompositions « normalisées » de n de la forme n = x² + y² avec 0 ⩽ x < y
 * est :
 *                       M = partieEntière((m+1)/2)
 * Le nombre total de solutions (x,y) signées est alors :
 *                       8M      si n n’est pas un carré parfait
 *                       8M - 4  sinon
 * En effet, chaque couple (x,y) normalisé correspond à 8 couples en tout,
 * obtenus en permutant x et y et en changeant leurs signes. Seule exception,
 * le couple (0,√n) qui est solution si n est un carré parfait, et qui ne
 * correspond en tout qu’à 4 couples.
 *
 * En particulier, si n = z², alors n est un carré parfait et toutes les
 * valuations de n sont paires, donc ce nombre se simplifie ainsi :
 *                       4m
 * avec bi les valuations dans z, et m = (2b1+1)…(2bk+1).
 *)

(* Ci-dessous, un code d’exemple. Pour la résolution du problème de Project
 * Euler, voir 233b.py. *)

let is_perfect_square n = let r = (int_of_float % sqrt % float) n in r*r = n

let rec valuation_and_residue p n =
  if n mod p <> 0 then
    (0,n)
  else
    let (v,n') = valuation_and_residue p (n/p) in
    (1+v,n')

let primes =
  let q = Queue.create () in Queue.add 2 q;
  q

exception Break of int

let count_triplets n =
  let (n',m) =
   try Queue.fold (fun (n,m) p ->
      if n = 1 then raise @@ Break m;
      let (v,n') = valuation_and_residue p n in
      let m' = if p mod 4 = 1 then m*(2*v+1) else m in
      (n',m')
    )
    (n,1)
    primes
   with Break m -> (1,m)
  in
  if n' <> 1 then
    Queue.add n' primes;
  let m = if n' <> 1 && n' mod 4 = 1 then m*3 else m in
  4 * m

let () =
  for n = 1 to 10_000 do
    Printf.printf "f(%i) = %i\n" n (count_triplets n)
  done
