# 
#  Project Euler #233
#

# Utilise le théorême des deux carrés de Fermat : cf le commentaire dans 233.ml
# pour l’explication du nombre de triplets pythagoriciens (x,y,z) à z fixé.
# On cherche les n tels que f(n) = count_triplets(n²) = 420. Or, en notant
# a1, …, ak les valuations dans n des facteurs premiers congrus à 1 modulo 4,
# on a vu que f(n) = 4m avec m = (2a1+1)…(2ak+1).
# Il faut donc résoudre l’équation :
#                          m = 105 = 3×5×7
# En examinant les diviseurs de 105, on obtient les familles possibles de
# valuations a1, …, ak :
#      — [52]
#      — [17, 1]
#      — [10, 2]
#      — [7, 3]
#      — [3, 2, 1]
# Or, on ne veut que les n ⩽ 10¹¹, donc on élimine immédiatement les deux
# premiêres familles (le plus petit n qu’on pourrait former serait 5^52 ou
# 5^17 × 13^1 respectivement, qui sont tous deux supérieurs au plafond).
# Il reste à inspecter les trois dernières familles. On calcule donc tous
# les entiers inférieurs à 10¹¹ de l’une des trois formes :
#      — n = p^10 × q^2     × n'
#      — n = p^7  × q^3     × n'
#      — n = p^3  × q^2 × r × n'
# avec p, q, r des facteurs premiers congrus à 1 modulo 4 et n' un entier
# n’admettant pas de tel facteur premier.

max = 10**11

def primes(n):
    li = []
    def is_prime(k):
        for p in li:
            if k % p == 0: return False
            if p*p > k:    return True
        return True
    for k in range(2,n+1):
        if is_prime(k):
            li.append(k)
    return li

# On a besoin des nombres premiers inférieurs à 4 733 727
# car 10^11 // (5^3 × 13^2) = 4 733 727.
primes1 = primes(4733727)

def is_valid_factor(n1):
    for p in primes1:
        if p % 4 == 1 and n1 % p == 0:
            return False
        while n1 % p == 0:
            n1 /= p
        if p*p > n1:
            return n1 == 1 or n1 % 4 == 3
    return True

# On a besoin des facteurs « n' » jusqu’à 278 455
# car 10^11 // (5^3 × 13^2 × 17) = 278 454.
valid_factors = set(filter(is_valid_factor, range(1,278455)))

primes1 = [p for p in primes1 if p % 4 == 1]
primes1.append(max)    # pour faire s’arrêter l’inspection

def next_not_in(li, x):
    x += 1
    while x in li:
        x += 1
    return x

def enum(exponents):
    print("exponents", exponents, ":")
    e = len(exponents)
    indices = []
    def rec(k, max):
        sum = 0
        if k < e:
            i = -1
            while True:
                i = next_not_in(indices, i)
                p = primes1[i] ** exponents[k]
                max2 = max / p
                if max2 < 1.0:
                    break
                indices.append(i)
                sum += p * rec(k+1, max2)
                indices.pop()
        else:
            for k,a in enumerate(exponents):
                print("%u^%u × " % (primes1[indices[k]], a), end="")
            print("n1")
            for n1 in range(1, int(max)+1):
                if n1 in valid_factors:
                    sum += n1
        return sum
    sum = rec(0, max)
    print("sum: %u\n-----" % sum)
    return sum

sum  = enum((10,2))
sum += enum((7,3))
sum += enum((3,2,1))

print("final sum: %u" % sum)
