(*
let primes_sieve n =
  let sieve = Array.init n (fun i -> i mod 2 <> 0) in
  let i = ref 1 in
  while i := !i+2; !i < n do
    if sieve.(!i) then
      let j = ref !i in
      while j := !j+ !i; !j < n do
        sieve.(!j) <- false
      done
  done;
  sieve

let primes_list n =
  let sieve = primes_sieve n in
  let rec aux i =
    if i >= n then []
    else if sieve.(i) then i :: aux (i+2)
    else                        aux (i+2)
  in
  2 :: aux 3
*)

(* φ(n) l’indicatrice d’Euler. *)
(*
let phi0 n =
  let n' = ref n in
  let res = ref (Int64.of_int n) in
  if !n' mod 2 = 0 then begin
    res := Int64.(div !res (of_int 2));
    while !n' mod 2 = 0 do n' := !n' / 2 done
  end;
  let p = ref 3 in
  while !n' > 1 do
    if !n' mod !p = 0 then begin
      res := Int64.(div (mul !res (of_int (!p-1))) (of_int !p));
      while !n' mod !p = 0 do n' := !n' / !p done
    end;
    p := !p + 2;
    if !p * !p > !n' then
      p := !n'
  done;
  if n mod 100_000 = 3 then
  Printf.printf "φ(%i) = %s\n%!" n (Int64.to_string !res);
  !res
*)

(* avec mémoïsation *)
module DLList = struct
  type 'a t = {mutable prev: 'a t; v: 'a; mutable next: 'a t}
  let singleton x = let rec  r = {prev = r; v = x; next = r} in r
  let concat l m =
    let tl = l.prev in
    l.prev.next <- m;
    m.prev.next <- l;
    l.prev      <- m.prev;
    m.prev      <- tl
  let add l x = concat l (singleton x)
  let to_list l =
    let rec aux {prev;v;next} =
      v :: (if next == l then [] else aux next)
    in
    aux l
end
let mem_max = (*40_000_000*)Sys.max_array_length
let phi =
  let mem = Array.make mem_max 0 in
  mem.(1) <- 1;
  let primes = DLList.singleton 0 in
  let rec phi_rec n acc primes' =
    if n < mem_max && mem.(n) > 0 then
      acc * mem.(n)
    else
      search_prime_factor n acc primes'
  and search_prime_factor n acc {DLList.prev; DLList.v = p; DLList.next} =
    if p = 0 || p*p > n then begin
      DLList.add primes n;
      acc * (n-1)
    end else if n mod p = 0 then begin
      let n' = ref n in
      while !n' mod p = 0 do n' := !n' / p done;
      let powp = n / !n' in
      phi_rec !n' (acc*powp/p*(p-1)) next
    end else
      search_prime_factor n acc next
  in
  fun n ->
    let res = phi_rec n 1 primes.DLList.next in
    if n < mem_max then
      mem.(n) <- res;
    if n mod 100_000 = 3 then
      Printf.printf "φ(%i) = %i\n%!" n res;
    res

(* Ψ(n) = φ(n) / n = Prod_{p facteur premier de n} (1 - 1/p) *)
(*let psi n =
  (phi n, n)*)

(* R(n) = φ(n) / (n-1) *)
let r n =
  (phi n, n-1)

let search_first_under ((rationum, ratiodenom) as ratio) =
  (*let primes = primes_list 100_000 in
  let rec iteration ((a,b) as cur_ratio) = function
    | p :: primes' -> let (a',b') as x = (a*(p-1),b*p) in
                      if a'*ratiodenom <= (b'-1)*rationum
                        then b'
                        else iteration x primes'
    | []           -> failwith "pas assez de mémoire"
  in
  iteration (1,1) primes*)
  let rationum' = Int64.of_int rationum
  and ratiodenom' = Int64.of_int ratiodenom in
  let i = ref 1
  and continue = ref true in
  while !continue do
    let (a,b) = r !i in
    (*if a*ratiodenom < b*rationum then*)
    if Int64.(compare (mul (of_int a) ratiodenom') (mul (of_int b) rationum')) < 0 then begin
      Printf.printf "result: r(%i) = %i / %i < %i / %i\n" !i a b rationum ratiodenom;
      continue := false
    end
    else
      incr i
  done;
  !i
  

let () =
  search_first_under (4,10) |> Printf.printf "%i\n";
  search_first_under (15499,94744) |> Printf.printf "%i\n";
  (*let t1 = Sys.time () in
  for i = 1 to 1_000_000 do
    phi0 i |> ignore
  done;
  let t2 = Sys.time () in
  for i = 1 to 1_000_000 do
    phi i |> ignore
  done;
  let t3 = Sys.time () in
  Printf.printf "phi0(1..1_000_000): %.3fs\n" (t2-.t1);
  Printf.printf "phi (1..1_000_000): %.3fs\n" (t3-.t2)*)
