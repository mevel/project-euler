(*
 *  Project Euler #250
 *)

let d = 250
let ( *: ) a b = (a * b) mod d

let rec exp ?(acc=1) b n =
  if n = 0 then
    acc
  else if n mod 2 = 0 then
    exp ~acc (b *: b) (n / 2)
  else
    exp ~acc:(acc *: b) (b *: b) (n / 2)

let m = 10_000__000_000__000_000
let ( +:: ) a b = (a + b) mod m

(* [count_subsets nmax] is the number (modulo m) of non-empty subsets of
 * { 1^1 ; 2^2 ; … ; nmax^nmax } whose sum is a multiple of [d]. *)
let count_subsets nmax =
  let count0 = Array.make d 0 in
  count0.(0) <- 1 ;
  let buf_pre = ref count0
  and buf_cur = ref (Array.make d 0) in
  for n = 1 to nmax do
    (* this buffer technique permits reusing already allocated arrays instead of
     * allocating a new one at each iteration: *)
    let count_pre = !buf_pre
    and count_cur = !buf_cur in
    buf_pre := count_cur ;
    buf_cur := count_pre ;
    (* [count_cur.(k)] is destined to store the number (modulo m) of (possibly
     * empty) subsets of { 1^1 ; … ; n^n } whose sum is k modulo [d]. It needs
     * the result of iteration n−1, which is stored in [count_pre.(k)]. *)
    for k = 0 to d-1 do
      count_cur.(k) <- count_pre.(k) +:: count_pre.((d + k - exp n n) mod d)
    done
  done ;
  (!buf_pre).(0) - 1

let () =
  Printf.printf "%u\n" (count_subsets 250_250)
