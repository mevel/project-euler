#!/usr/bin/env ocaml

(*
 * Project Euler #315
 *)

type +!'a iter = ('a -> unit) -> unit
let iter ~f (it : _ iter) : unit = it f
let iter_flat_map (type a b) ~(f: a -> b iter) (it : a iter) : b iter =
  fun do_ -> it (fun x -> f x do_)
let iter_empty : type a. a iter =
  fun do_ -> ()
let iter_snoc (type a) (it : a iter) (x : a) : a iter =
  fun do_ -> it do_ ; do_ x
let iter_of_list li : _ iter =
  fun do_ -> List.iter do_ li

(*
 * 7-SEGMENT CLOCKS
 *
 *    eeee
 *   a    c
 *   a    c
 *   a    c
 *    ffff
 *   b    d
 *   b    d
 *   b    d
 *    gggg
 *
 *)

let encode_segments letters =
  let bits = ref 0 in
  StringLabels.iter letters ~f:begin fun c ->
    assert ('a' <= c && c <= 'g') ;
    let b = Char.code c - Char.code 'a' in
    bits := !bits lor (1 lsl b)
  end ;
  !bits

let segments_of_digit =
  [|
    (* 0 *) encode_segments "abgdce" ;
    (* 1 *) encode_segments "cd"  ;
    (* 2 *) encode_segments "ecfbg" ;
    (* 3 *) encode_segments "ecfdg" ;
    (* 4 *) encode_segments "afcd" ;
    (* 5 *) encode_segments "eafdg" ;
    (* 6 *) encode_segments "eabgdf" ;
    (* 7 *) encode_segments "aecd" ;
    (* 8 *) encode_segments "abcdefg" ;
    (* 9 *) encode_segments "faecdg" ;
  |]

(* The commented snippets encode segments of several digits as lists of digits,
 * each digit being a 7-bit bitfield stored in a native integer. With 63-bit
 * native integers, we can rather pack up to 8 digits in a single integer. Then
 * everything is simpler. *)
let () = assert (Sys.int_size >= 7*8)

(* NOTE: the number zero is represented with no segment set at all. *)
(*! let rec segments_of_number n = !*)
(*!   assert (0 <= n) ; !*)
(*!   if n = 0 then [] !*)
(*!   else segments_of_digit.(n mod 10) :: segments_of_number (n / 10) !*)
let rec segments_of_number n =
  assert (0 <= n && n < 100_000_000) ;
  if n = 0 then 0
  else segments_of_digit.(n mod 10) lor (segments_of_number (n / 10) lsl 7)

(*! let rec diff segs1 segs2 = !*)
(*!   begin match segs1, segs2 with !*)
(*!   | [], segs !*)
(*!   | segs, []               -> segs !*)
(*!   | s1::segs1', s2::segs2' -> (s1 lxor s2) :: diff segs1' segs2' !*)
(*!   end !*)
let diff segs1 segs2 =
  segs1 lxor segs2

let count_bits_set bits =
  let count = ref 0 in
  let bits = ref bits in
  while !bits <> 0 do
    if !bits land 1 = 1 then incr count ;
    bits := !bits lsr 1
  done ;
  !count

let count_segments segs =
  (*! List.fold_left (fun count s -> count + count_bits_set s) 0 segs !*)
  count_bits_set segs

(* The number of transitions done by Sam’s (resp. Max’s) clock when displaying
 * the given sequence of integers (represented as an iteration). *)

let cost_sam (it : int iter) =
  let count = ref 0 in
  iter it ~f:begin fun new_number ->
    let new_segs = segments_of_number new_number in
    count := !count + 2 * count_segments new_segs ;
  end ;
  !count

let cost_max (it : int iter) =
  (*! let cur_segs = ref [] in !*)
  let cur_segs = ref 0 in
  let count = ref 0 in
  iter it ~f:begin fun new_number ->
    let new_segs = segments_of_number new_number in
    count := !count + count_segments (diff !cur_segs new_segs) ;
    cur_segs := new_segs ;
  end ;
  (* Max’s clock only clears its display at the very end: *)
  !count + count_segments !cur_segs

let () =
  assert (cost_sam @@ iter_of_list [137; 11; 2] = 40) ;
  assert (cost_max @@ iter_of_list [137; 11; 2] = 30)

(*
 * DIGITAL ROOTS
 *)

let rec digital_root ?(base=10) n =
  if n = 0 then 0 else n mod base + digital_root ~base (n / base)

let rec iterated_digital_root ?base n : int iter =
  fun do_root ->
    do_root n ;
    if n >= 10 then iterated_digital_root ?base (digital_root ?base n) do_root

(*
 * PRIME NUMBERS
 *)

(* Naive Eratosthenes sieve: *)
let primes ~nmax : int iter =
  let s = Array.make (nmax+1) true in
  fun do_prime ->
    for n = 2 to nmax do
      if s.(n) then begin
        do_prime n ;
        for k = n to nmax/n do
          s.(n*k) <- false
        done
      end
    done

(*
 * WRAP UP
 *)

let nmin = 10_000_000
let nmax = 20_000_000

let () =
  let it =
    iter_flat_map (primes ~nmax) ~f:begin fun p ->
      if nmin <= p && p <= nmax then
        (* We add a zero after each calculation of a digital root sequence
          * because both clocks, including Max’s clock, clear their segments
          * entirely after each full calculation. *)
        iter_snoc (iterated_digital_root p) 0
      else
        iter_empty
    end
  in
  let sam = cost_sam it
  and max = cost_max it in
  Printf.printf "%i\n" (sam - max)
