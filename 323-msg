Simple probability exercise. I found the same formulas as many here, [b]but[/b] what I think has not been mentioned yet is that when implemented directly using floating-point numbers, the loss of precision is too high for getting the expected answer.

[b]Math[/b]

Let $p := 32$. We have $N = \max_{1 \leq k \leq p} N_k$ where $N_k$ is the time after which the $k$-th bit is set. The $N_k$ are independent random variables with the same probability distribution: $\mathbb{P}(N_k > n) = \tfrac1{2^n}$ (the [url=https://en.wikipedia.org/wiki/Geometric_distribution]geometric distribution[/url]).

The max is less than $n$ iff every component is less than $n$, so by independence: $\mathbb{P}(N \leq n) = \mathbb{P}(N_1 \leq n)^p$.

Let’s compute the expected value. [url=https://projecteuler.net/thread=323#35197]Caesar[/url] and other people above have written similar proofs, but that part can be made much simpler by starting from the formula $\sum_n \mathbb{P}(N \geq n)$ rather than $\sum_n n \mathbb{P}(N=n)$ for the expected value of an integral random variable $N$.
$$
  \mathbb{E}(N) = \sum_{n \geq 0} \mathbb{P}(N > n) \\
$$
By using complements, independence and the distribution, we get [url=https://projecteuler.net/thread=323#35178]Ryals’ formula[/url]:
$$\boxed{
  \mathbb{E}(N) = \sum_{n \geq 0} \left(1 - \left(1-\tfrac1{2^n}\right)^p\right)
}$$
Then we can develop the product, cancel the first term…
$$
  \mathbb{E}(N) = - \sum_{n \geq 0} \sum_{1 \leq k \leq p} \binom{p}{k} \frac {(-1)^k} {2^{kn}}
$$
… swap summations and simplify the geometric sum, to obtain [url=https://projecteuler.net/thread=323#35182]smartie61’s formula[/url]:
$$\boxed{
  \mathbb{E}(N) = - \sum_{1 \leq k \leq p} \binom{p}{k} \frac {(-1)^k} {1 - \left(\tfrac12\right)^k}
}$$

[b]Precision issues[/b]

So I thought I’d answer the problem with that last formula and standard 64-bit floating point numbers. Unfortunately, with a direct implementation of this formula, I get $6.3551759333$ while the expected answer is $6.3551758451$, so only 6 decimals after the decimal point are correct… I guess we could use the other formula and stop the summation as soon as the desired precision is reached, but at that point I got lazy and fed the formula to a computer algebra software that gave me the exact value.
