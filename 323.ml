#!/usr/bin/env ocaml

(*
 * Project Euler #323
 *)

(* See message for the math involved. *)

let binom n k =
  let m = ref 1.0 in
  for i = 1 to n-k do
    m := !m *. float (k+i) /. float i
  done ;
  !m

(* Compute the expected value of the maximum of [p] independent random variables
 * with the same geometric distribution, i.e. Pr(Xi > n) = 1/2^n for all n ≥ 0. *)
let e ~p =
  let sum = ref 0.0 in
  for k = 1 to p do
    let sign = if k land 1 = 0 then -1.0 else 1.0 in
    sum := !sum +. binom p k *. sign /. (1.0 -. 0.5 ** float k)
  done ;
  !sum

let () =
  Printf.printf "%.10f\n" (e ~p:2) ; (* expected answer: 8/3 = 2.6666666667 *)
  Printf.printf "%.10f\n" (e ~p:3) ; (* expected answer: 22/7 = 3.1428571429 *)
  Printf.printf "%.10f\n" (e ~p:4) ; (* expected answer: 368/105 = 3.5047619048 *)
  Printf.printf "%.10f\n" (e ~p:32) ;
(* We get:
 *     6.3551759333
 * but apparently, with 64-bit floating point numbers, this summation loses too
 * much precision, so to answer the problem I rather fed the formula to
 * a computer algebra system that gave the exact value. When correctly rounded,
 * the answer is:
 *     6.3551758451
 *)
