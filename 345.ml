(* algo naïf : complexité factorielle… *)

(*
let matrix_sum matrix =
  let n = Array.length matrix in
  let state = Array.make n true in
  let max_sum = ref 0 in
  let rec explore i sum =
    if i = n then
      max_sum := max !max_sum sum
    else for j = 0 to n-1 do
      if state.(j) then begin
        state.(j) <- false;
        explore (i+1) (sum + matrix.(i).(j));
        state.(j) <- true
      end
    done
  in
  explore 0 0;
  !max_sum
*)

(* programmation dynamique : mémorisation de 2^n états, le calcul de chaque état
 * est en O(n), donc complexité O(2^n) en mémoire et O(n×2^n) en temps. Un état
 * est un couple (i,state) où i est un numéro de ligne et state est un masque
 * binaire indiquant les colonnes utilisées après avoir choisi un élément dans
 * toutes les lignes de 0 à i (i est donc le nombre de bits à 1 dans ce masque),
 * On veut donc calculer la somme maximale de l’état (n-1, 11111…1).
 *)

let pow2 = (lsl) 1
let is_set b n = (n lsr b) land 1 <> 0
let unset  b n = n land lnot (1 lsl b)

let matrix_sum matrix =
  let n = Array.length matrix in
  let p = pow2 n in
  let sums = Array.make p (-1) in
  sums.(0) <- 0;
  let rec explore i state =
    if sums.(state) < 0 then begin
      sums.(state) <- 0;
      for j = 0 to n-1 do
        if is_set j state then
          sums.(state) <- max sums.(state)
                              (matrix.(i).(j) + explore (i-1) (unset j state))
      done
    end;
    sums.(state)
  in
  explore (n-1) (p-1)

let () =
  Printf.printf "%i\n" @@ matrix_sum
    (*[|
      [|   7;  53; 183; 439; 863 |];
      [| 497; 383; 563;  79; 973 |];
      [| 287;  63; 343; 169; 583 |];
      [| 627; 343; 773; 959; 943 |];
      [| 767; 473; 103; 699; 303 |];
    |]*)    (* expected sum: 3315 *)
    [|
      [|   7;  53; 183; 439; 863; 497; 383; 563;  79; 973; 287;  63; 343; 169; 583 |];
      [| 627; 343; 773; 959; 943; 767; 473; 103; 699; 303; 957; 703; 583; 639; 913 |];
      [| 447; 283; 463;  29;  23; 487; 463; 993; 119; 883; 327; 493; 423; 159; 743 |];
      [| 217; 623;   3; 399; 853; 407; 103; 983;  89; 463; 290; 516; 212; 462; 350 |];
      [| 960; 376; 682; 962; 300; 780; 486; 502; 912; 800; 250; 346; 172; 812; 350 |];
      [| 870; 456; 192; 162; 593; 473; 915;  45; 989; 873; 823; 965; 425; 329; 803 |];
      [| 973; 965; 905; 919; 133; 673; 665; 235; 509; 613; 673; 815; 165; 992; 326 |];
      [| 322; 148; 972; 962; 286; 255; 941; 541; 265; 323; 925; 281; 601;  95; 973 |];
      [| 445; 721;  11; 525; 473;  65; 511; 164; 138; 672;  18; 428; 154; 448; 848 |];
      [| 414; 456; 310; 312; 798; 104; 566; 520; 302; 248; 694; 976; 430; 392; 198 |];
      [| 184; 829; 373; 181; 631; 101; 969; 613; 840; 740; 778; 458; 284; 760; 390 |];
      [| 821; 461; 843; 513;  17; 901; 711; 993; 293; 157; 274;  94; 192; 156; 574 |];
      [|  34; 124;   4; 878; 450; 476; 712; 914; 838; 669; 875; 299; 823; 329; 699 |];
      [| 815; 559; 813; 459; 522; 788; 168; 586; 966; 232; 308; 833; 251; 631; 107 |];
      [| 813; 883; 451; 509; 615;  77; 281; 613; 459; 205; 380; 274; 302;  35; 805 |];
    |]
