#!/bin/env ocaml

(*
 *  Project Euler #387
 *)

let is_prime =
  (* 10_000_000 is √( 10^16 ). *)
  let primes_under_10_000_000 =
    let li = ref [] in
    let file = Scanf.Scanning.open_in "data/primes-under-10_000_000.data" in
    begin try while true do
      (* "%_1[\r]@\n" is a format trick that matches \n, \r\n and end-of-file. *)
      Scanf.bscanf file "%u%_1[\r]@\n" @@fun p ->
      li := p :: !li
    done with End_of_file -> () end ;
    Scanf.Scanning.close_in file ;
    List.rev !li
  in
fun n ->
  let rec test primes =
    begin match primes with
    | []           -> true
    | p :: primes' -> p * p > n || (n mod p <> 0 && test primes')
    end
  in
  n > 1 && test primes_under_10_000_000

(* nmax is 10^{dmax + 1} *)
let dmax = 13

let rec iter_harshad_truncatable d s k f =
  if d < dmax then begin
    let d' = d + 1 in
    for c = (if d = 0 then 1 else 0) to 9 do
      let k' = 10*k + c in
      let s' = s + c in
      if k' mod s' = 0 then begin
        f k' s' ;
        iter_harshad_truncatable d' s' k' f
      end
    done
  end

(* these tests serve only as a check. *)
let rec sum_of_digits n =
  if n = 0 then 0 else (n mod 10) + sum_of_digits (n / 10)
let is_harshad n =
  n mod sum_of_digits n = 0
let is_harshad_strong n =
  let s = sum_of_digits n in
  n mod s = 0 && is_prime (n / s)
let rec is_harshad_truncatable n =
  if n = 0 then true else is_harshad n && is_harshad_truncatable (n / 10)
let is_harshad_strong_truncatable n =
  is_harshad_strong n && is_harshad_truncatable (n / 10)
let is_harshad_prime n =
  is_prime n && is_harshad_strong_truncatable (n / 10)

let () =
  let sum = ref 0 in
  iter_harshad_truncatable 0 0 0 begin fun k s ->
    assert (is_harshad_truncatable k) ;
    if is_prime (k / s) then begin
      assert (is_harshad_strong_truncatable k) ;
      for c = 0 to 9 do
        let k' = 10*k + c in
        if is_prime k' then begin
          assert (is_harshad_prime k') ;
          sum := !sum + k'
        end
      done
    end
  end ;
  Printf.printf "%u\n" !sum
