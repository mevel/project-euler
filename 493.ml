#!/bin/env ocaml

(*
 *  Project Euler #493
 *)

#require "zarith" ;;

let expected_number_of_colors ~colors ~per_color ~to_draw =
  let total = colors * per_color in
  (* [proba.(k).(c)] is the probability that there are [c] distinct colors after
   * we have drawed [k] balls. *)
  let proba = Array.make_matrix (succ to_draw) (succ colors) Q.zero in
  proba.(0).(0) <- Q.one ;
  for k = 1 to to_draw do
    for c = 1 to colors do
      let p1 = per_color * (colors - pred c) in
      let p2 = per_color * c - pred k in
      let q = total - pred k in
      proba.(k).(c) <-
        Q.( ((proba.(pred k).(pred c) * ~$p1 + proba.(pred k).(c) * ~$p2))
          / ~$q )
    done
  done ;
  assert (proba |> Array.for_all (fun row -> Q.equal (Array.fold_left Q.(+) Q.zero row) Q.one)) ;
  (* compute the expectation: *)
  let row = proba.(to_draw) in
  let sum = ref Q.zero in
  for c = 0 to colors do
    sum := Q.(!sum + row.(c) * ~$c)
  done ;
  !sum

let () =
  Format.printf "%a\n" Q.pp_print
    (expected_number_of_colors ~colors:7 ~per_color:10 ~to_draw:20)
