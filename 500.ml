(*
 *  Project Euler #500
 *)

(* list of the 500 500 first primes
 *
 * note that we really needs that many prime numbers, because the smallest
 * number with exactly 2^k divisors is ALMOST the product of the k first primes.
 *)

let primes_list_from_file ~count =
  let li = ref [] in
  let file = Scanf.Scanning.open_in "data/primes-under-10_000_000.data" in
  for _ = 1 to count do
    (* "%_1[\r]@\n" is a format trick that matches \n, \r\n and end-of-file. *)
    Scanf.bscanf file "%u%_1[\r]@\n" @@fun p ->
    li := p :: !li ;
  done ;
  Scanf.Scanning.close_in file ;
  List.rev !li

let primes = primes_list_from_file ~count:500_500

(* modular arithmetic *)

let m = 500_500_507
let ( *: ) a b = (a * b) mod m

let () =
  assert (m <= truncate @@ sqrt @@ float max_int)

(* we store the prime factorization of the number being computed; each factor is
 * of the form p^{2^k − 1} where p is a prime number and k is a natural integer,
 * which we will call the “log2‐multiplicity” of the factor.
 *
 * (this allows for trivial factors 1, with k = 0.)
 *
 * the “total log2‐multiplicity” of the product is the sum of the
 * log2‐multiplicity of its factors, so that the problem consists in computing
 * the smallest product whose total log2‐multiplicity is 500 500.
 *
 * we use a greedy approach to the problem, by starting with 1 (the empty
 * product) and repeatedly incrementing the total log2‐multiplicity until it
 * reaches 500 500. each step consists in incrementing the log2‐multiplicity of
 * some factor f = p^{2^k − 1}, which becomes p^{2^{k+1} − 1}. then the “growth”
 * of the product (that is, the quantity by which it is multiplied) is:
 *     p^{2^k}  =  f × p
 * we choose the factor to increase so that the growth remains minimal. for
 * that, we store the factors in a min‐heap.
 *
 * for convenience, for each factor, we also store the growth it will create
 * when it will be incremented; more precisely, since growths can be very large
 * and we are only interested in comparing them, we store the log∘log of the
 * growth, which is equal to:
 *    log log p + k × log 2
 *)

type factor =
  {
    prime : int ;             (* the base prime number p *)
(*     log2_multiplicity : int ; (* the log2‐multiplicity k *) *)
    factor_mod_m : int ;      (* the quantity p^{2^k − 1}, modulo m *)
    log_log_growth : float ;  (* the log ∘ log of the growth p^{2^k} *)
  }

(* [create_factor p] creates a trivial factor from the given prime number [p]:
 *)
let create_factor p =
  {
    prime = p ;
(*     log2_multiplicity = 0 ; *)
    factor_mod_m = 1 ;
    log_log_growth = log (log (float p))
  }

(* [incr_factor factor] increments the log2‐multiplicity of the given factor: *)
let incr_factor =
  let log2 = log 2.0 in
fun factor ->
  {
    prime = factor.prime ;
(*     log2_multiplicity = factor.log2_multiplicity + 1 ; *)
    factor_mod_m = factor.factor_mod_m *: factor.factor_mod_m *: factor.prime ;
    log_log_growth = factor.log_log_growth +. log2 ;
  }

(* [le factor1 factor2] compares the growth of the two given factors: *)
let le factor1 factor2 =
  factor1.log_log_growth <= factor2.log_log_growth

(* a min‐heap datastructure on factors, with the order defined above: *)
module Heap = CCHeap.Make (struct type t = factor let leq = le end)

(* we use sequences here because List.map is not tail‐rec, and provokes a
 * stack‐overflow with such a large list: *)
module Seq = struct
  type 'a t = ('a -> unit) -> unit
  let of_list (type a) (li : a list) : a t =
    fun f -> List.iter f li
  let map (type a b) (g : a -> b) (s : a t) : b t =
    fun f -> s (fun x -> f @@ g x)
end

let initial_heap = Heap.of_seq @@ Seq.map create_factor @@ Seq.of_list primes

let minimal_product_with_log2_multiplicity multiplicity =
  let product_mod_m = ref 1 in
  let factors = ref initial_heap in
  for _ = 1 to multiplicity do
    let (factors', factor) = Heap.take_exn !factors in
    product_mod_m := !product_mod_m *: factor.factor_mod_m *: factor.prime ;
    factors := Heap.insert (incr_factor factor) factors' ;
  done ;
  !product_mod_m

let () =
  assert (minimal_product_with_log2_multiplicity 4 = 120) ;
  Printf.printf "%u\n" (minimal_product_with_log2_multiplicity 500_500)
