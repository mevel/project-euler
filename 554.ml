(*
 *  Projet Euler #554
 *)

(* we need an array of size 50 000 003 but this exceeds Sys.max_array_length on
 * a 32‐bit system, so we use this instead: *)
class ['a] bigarray =
  let blk_sz = Sys.max_array_length in
fun n (v : 'a) -> object
  val mem = Array.init ((n + blk_sz - 1) / blk_sz) (fun _ -> Array.make blk_sz v)
  method get i   = mem.(i / blk_sz).(i mod blk_sz)
  method set i x = mem.(i / blk_sz).(i mod blk_sz) <- x
end

let m      = Z.(~$100_000_007)    (* m is prime *)
let half_m = 50_000_003
let two    = Z.(~$2)

(* central_binomial_mem n = \binom{2n}{n} \mod m, for 0 ⩽ n < m/2 *)
let central_binomial_mem =
  let mem = new bigarray (succ half_m) Z.one in
  mem#set 0 Z.one ;
  for i = 1 to half_m do
    let prev = mem#get (pred i)
    and i_z  = Z.(~$i) in
    mem#set i Z.(prev * two * pred (two * i_z) * invert i_z m mod m)
  done ;
  mem#get

(* central_binomial n = \binom{2n}{n} \mod m;
 * uses Lucas’ theorem supplemented with Kummer’s theorem *)
let central_binomial =
  let open Z in
  let rec prod acc n =
    if equal n zero then
      acc
    else begin
      let r = to_int (n mod m) in
      if r > half_m then
        zero
      else
        prod (acc * central_binomial_mem r mod m) (n / m)
    end
  in
  prod one

(* c n = number of arrangements of n² centaurs on a (2n)×(2n) board, modulo m;
 * see ecnerwal’s explanation for this formula (first message on the forum) *)
let c n =
  Z.(~$8 * central_binomial n - ~$3 * n * n - ~$2 * n - ~$7)

(* fib n = n‐th term of Fibonacci, for 0 ⩽ n ⩽ 90;
 * note that fib 90 = 2 880 067 194 370 816 120 ~ 10¹⁸ *)
let fib =
  let fib = Array.make 91 Z.zero in
  fib.(1) <- Z.one ;
  for i = 2 to 90 do
    fib.(i) <- Z.add fib.(i-1) fib.(i-2)
  done ;
  Array.get fib

let () =
  let sum = ref Z.zero in
  for i = 2 to 90 do
    sum := Z.(erem (!sum + c (fib i)) m)
  done ;
  Printf.printf "total mod (10⁸+7) = %a\n" Z.output !sum
