(*
 *  Project Euler #555
 *)

open Z

let two = ~$2

(*
 * O(p) algorithm:
 *   s p m  =  ∑_{1 ⩽ d ⩽ p/2}  qd (2m − qd + 1) / 2    where q = p ÷ d − 1
 *)

let for_z from til ?(by=Z.one) (f : _ -> unit) =
  let i = ref from in
  while Z.leq !i til do
    f !i ;
    i := Z.add !i by
  done

let s p m =
  let two_m_plus_one = succ (two * m) in
  let sum = ref zero in
  for_z one (p / two) begin fun d ->
    let qd = pred (p / d) * d in
    sum := !sum + qd * (two_m_plus_one - qd)
  end ;
  !sum /| two

(*
 * O(√p) algorithm:
 *   s_{p,m} = 1/12 × ∑_{1 ⩽ q ⩽ p} term_{q, p÷q, p÷(q+1)}
 *   term_{q,d,d’} = (q−1) [  d  (d +1) (6m + 3 − (q−1)(2d +1))
 *                          − d’ (d’+1) (6m + 3 − (q−1)(2d’+1)) ]
 * the set of “useful” indexes q being:
 *   Q = ⋃_{1 ⩽ q       s·t· p÷(q+1) < p÷q}  \{ q \}
 *     = ⋃_{1 ⩽ q ⩽ p÷q s·t· p÷(q+1) < p÷q}  \{ q ; p÷q \}
 *)

let s p m =
  let tmp = ~$6 * m + ~$3 in
  (* term of the summation with index q, provided d = p÷q and d' = p÷(q+1): *)
  let term q d d' =
    let pred_q = pred q in
    pred_q * ( + d  * succ d  * (tmp - pred_q * succ (two * d ))
               - d' * succ d' * (tmp - pred_q * succ (two * d')) )
  in
  (* inductive summation:
   *   q  = current index
   *   q' = p÷(d+1) = the last used q (initially 0)
   *   d  = p÷q *)
  let rec sum acc q' q d =
    if gt q d then
      acc
    else if equal q d then
      acc + term q q q'
    else begin
      let succ_q = succ q in
      let d' = p / succ_q in
      if equal d d' then
        sum acc q' succ_q d
      else
        sum (acc + term q d d' + term d q q') q succ_q d'
    end
  in
  sum zero zero one p /| ~$12

let ss p = s ~$p ~$p

let () =
  Printf.printf "%a\n" output @@ ss 1_000_000
