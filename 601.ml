#require "zarith" ;; (* we need zarith only because 4^31 is max_int + 1… *)

let rec gcd a b =
  if b = 0 then a
  else gcd b (a mod b)

let lcm_from_1_to =
  let rec aux prod n =
    if n < 2 then prod
    else aux (prod * (n / gcd prod n)) (pred n)
  in
  aux 1

(*
 * on note π(s) = ppcm(1,2,…,s) et q(s) = π(s) / π(s-1) = s / pgcd(s,π(s-1)).
 *
 * pour commencer on reformule streak ainsi :
 *     streak(n) = s  ⇔  ∀ k < s, n == 1 mod (k+1) et n =/= 1 mod (s+1)
 *                    ⇔  ∀ k ⩽ s, n == 1 mod k     et n =/= 1 mod (s+1)
 *
 * en appliquant le théorème des restes chinois, on montre que 1 est l’unique
 * entier modulo π(s) tel que :
 *     ∀ k ⩽ s, n == 1 mod k
 * autrement dit la condition ci‐dessus est équivalente à :
 *     n == 1 mod π(s)
 * de plus, sous cette condition on montre aussi l’équivalence :
 *     n == 1 mod (s+1)  ⇔  n == 1 mod π(s+1)  ⇔  n == 1 mod q(s+1)
 * par ailleurs, toujours d’après le théorème chinois, pour chaque t entre 0 et
 * q(s+1)−1, il existe un unique entier modulo π(s+1) vérifiant les deux
 * équations :
 *     n == 1 mod π(s)
 *     n == t mod q(s+1)
 * pour t variant, l’ensemble des q(s+1) solutions obtenues modulo π(s+1) est le
 * suivant, à l’ordre près (car π(s) est inversible modulo q(s+1)) :
 *     1, 1 + π(s), 1 + 2 π(s), …, 1 + (q(s+1)−1) π(s)
 * et toutes sauf 1 (correspondant à t = 1) satisfont :
 *     n == t =/= 1 mod q(s+1)
 *
 * ainsi, il y a exactement q(s+1)−1 solutions modulo π(s+1) à l’équation
 *     streak(n) = s
 * ce sont les suivantes :
 *     1 + k π(s),  k ∈ [[ 1 ; q(s+1)−1 ]]
 *
 * notons que streak(0) = 1 et que streak(1) = ∞.
 *)

let p s nmax =
  let m = lcm_from_1_to s in
  let q = succ s / gcd m (succ s) in
  let m' = m * q in
  (pred q) * Z.(to_int (nmax / ~$m')) + (max 0 (Z.(to_int (nmax mod ~$m')) - 2)) / m

(* for s = 1, the algorithm above counts the element 0 (since streak(0) = 1),
 * but the exercise of the website ignores it: *)
let p s nmax =
  if s = 1 then p 1 nmax - 1
  else p s nmax

let () =
  assert Z.(p 3 ~$14 = 1) ;
  assert Z.(p 6 ~$1_000_000 = 14286)

let () =
  let four = Z.(~$4) in
  let four_pow_i = ref Z.one
  and sum = ref 0 in
  for i = 1 to 31 do
    four_pow_i := Z.(!four_pow_i * four) ;
    sum := !sum + p i !four_pow_i
  done ;
  Printf.printf "sum = %i\n" !sum
