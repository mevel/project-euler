#!/bin/env ocaml

(*
 *  Project Euler #623
 *)

let m = 1_000_000_007
let ( +: ) a b = (a + b) mod m
let ( *: ) a b = (a * b) mod m

let count_lambda_terms nmax =
  (* count.(p).(n) is the number of λ-terms of size exactly n with at most p
   * distinct variables. *)
  let pmax = (nmax + 4) / 5 in
  let count = Array.init pmax (fun p -> Array.make (nmax - 5*p + 1) 0) in
  let get p n =
    if n <= 0 then 0 else count.(p).(n)
  in
  for p = pmax - 1 downto 0 do
    count.(p).(1) <- p ;
    for n = 2 to nmax - 5*p do
      let s = ref (get (p+1) (n-5)) in
      for k = 1 to n-3 do
        s := !s +: count.(p).(k) *: count.(p).(n-2-k)
      done ;
      count.(p).(n) <- !s
    done ;
  done ;
  Array.fold_left (+:) 0 count.(0)

let () =
  Printf.printf "%i\n" (count_lambda_terms 2_000)
