#!/usr/bin/env ocaml

(*
 *  Project Euler #624
 *)

let rec pow ~mult ~unit b n =
  assert (n >= 0) ;
  if n = 0 then
    unit
  else if n mod 2 = 0 then
    pow ~mult ~unit (mult b b) (n / 2)
  else
    pow ~mult ~unit:(mult unit b) (mult b b) (n / 2)

(*
 * Arithmetic in the field ℤ∕mℤ
 *)

module ModArith (M : sig val modulo : int end)
= struct

  let m = M.modulo

  let () =
    assert (0 < m) ;
    assert (m <= truncate@@sqrt@@float max_int)

  type t = int

  let ( +: ) a b = (a + b) mod m
  let ( ~-: ) a = if a = 0 then 0 else m - a
  let ( -: ) a b = (a + ~-:b) mod m
  let ( *: ) a b = (a * b) mod m
  let pow_mod = pow ~mult:( *: ) ~unit:1
  let inv_mod a =
    if a mod m = 0 then
      raise Division_by_zero
    else
      pow_mod a (m-2)
  let ( /: ) a b = a *: inv_mod b

end (* module ModArith *)

(*
 * Arithmetic in the ring ℤ∕mℤ [ √5 ]
 *)

module Make (M : sig val modulo : int end)
= struct

  module Scalar = ModArith (M)
  include Scalar

  type u = int * int

  let one = (1, 0)
  let inj a = (a, 0)
  let ra (x, _) = x
  let conj (x, y) = (x, ~-:y)
  let norm2 (x, y) = x*:x -: 5*:y*:y
  let ( ++: ) (ax, ay) (bx, by) = (ax+:bx, ay+:by)
  let ( --: ) (ax, ay) (bx, by) = (ax-:bx, ay-:by)
  let ( **: ) (ax, ay) (bx, by) = (ax*:bx +: 5*:ay*:by, ax*:by +: ay*:bx)
  let ( *:: ) (ax, ay) b = (ax *: b, ay *: b)
  let ( /:: ) (ax, ay) b = (ax /: b, ay /: b)
  let inv (x, y) = (x, ~-:y) /:: norm2 (x, y)
  let ( //: ) a b = a **: inv b
  let pow = pow ~mult:( **: ) ~unit:one

  let aaaa = (5, 3) /:: 10
  let bbbb = (5, ~-:1) /:: 10
  let root = (1, 1) /:: 4

  let () =
    assert (bbbb = aaaa //: (root **: root **: root) /:: 8)

  let q n =
    ra (aaaa **: pow root n *:: 2)

  let () =
    let q0 = ref 1 in
    let q1 = ref 1 in
    assert (q 0 = !q0) ;
    assert (q 1 = !q1) ;
    for n = 2 to 100 do
      let q2 = 1/:2 *: !q1 +: 1/:4 *: !q0 in
      assert (q n = q2) ;
      q0 := !q1 ;
      q1 := q2
    done

  let p n =
    ra (bbbb //: (pow (inv root) n --: inj 1) *:: 2)
    (* here we hope that the denominator will be inversible modulo m; i have no
     * justification for that, but it works for solving the problem. *)

end (* module Make *)

let () =
  let module M = Make (struct let modulo = 109 end) in
  let open M in
  assert (3/:5 = 66) ;
  assert (9/:31 = 46) ;
  assert (p 2 = 66) ;
  assert (p 3 = 46)

let () =
  let module M = Make (struct let modulo = 1_000_000_009 end) in
  let open M in
  Printf.printf "%u\n" (p 1_000_000_000_000_000_000)
