#!/bin/env ocaml

(*

let list_init n f =
  let rec aux acc n =
    begin match n with
    | 0 -> acc
    | _ -> aux (f (n-1) :: acc) (n-1)
    end
  in
  aux [] n

let list_iter_permut elts f =
  let rec insert left y right kont =
    kont (List.rev_append left (y :: right)) ;
    begin match right with
    | []      -> ()
    | x :: xs -> insert (x :: left) y xs kont
    end
  in
  let rec aux acc elts =
    begin match elts with
    | [] ->
        f acc
    | x :: xs ->
        insert [] x acc @@fun acc' ->
        aux acc' xs
    end
  in
  aux [] (List.rev elts)

let pp_board out pos =
  let n = List.length pos in
  let board = Array.init n (fun _ -> Bytes.make n '+') in
  pos |> Array.of_list |> Array.iteri begin fun i j ->
    board.(i).[j] <- '#'
  end ;
  board |> Array.to_list |> List.rev |> List.iter begin fun row ->
    Printf.fprintf out "\t|%s|\n" (Bytes.to_string row)
  end

let is_open pos =
  let n = List.length pos in
  let board = Array.make_matrix n n true in
  pos |> Array.of_list |> Array.iteri begin fun i j ->
    board.(i).(j) <- false
  end ;
  for i = 0 to n-1 do
    for j = 0 to n-1 do
      if board.(i).(j) && (i,j) <> (0,0) then
        board.(i).(j) <- (i <> 0 && board.(i-1).(j)) || (j <> 0 && board.(i).(j-1))
    done
  done ;
  board.(n-1).(n-1)

let count_open n =
  let c = ref 0 in
  let elts = list_init n (fun i -> i) in
  list_iter_permut elts begin fun pos ->
    if is_open pos then
      incr c
  end ;
  Printf.printf "%u\n" !c

*)

let m = 1_008_691_207
let ( +: ) a b = (a + b) mod m
let ( *: ) a b = (a * b) mod m

(* about sum of factorials, see
 *     http://mathworld.wolfram.com/FactorialSums.html
 * *)

let count_open n =
  let s = ref 0 in
  let f = ref 1 in
  for k = 0 to n-1 do
    (* invariant: f = factorial k, s = sum of factorial i for i ∈ 0..(k-1) *)
    s := !s +: !f ;
    f := !f *: (k+1)
  done ;
  2 +: (n-3) *: !s

let () =
  Printf.printf "%u\n" (count_open 100_000_000)
