#!/bin/env ocaml

(*
 *  Project Euler #630
 *)

(* load findlib (to be able to #require libraries) *)
try Topdirs.dir_directory (Sys.getenv "OCAML_TOPLEVEL_PATH") with Not_found -> () ;;
#use "topfind" ;;

#require "zarith" ;;

let points n =
  let s = Array.make (2*n+1) 0 in
  s.(0) <- 290_797 ;
  for k = 1 to 2*n do
    s.(k) <- (s.(k-1) * s.(k-1)) mod 50_515_093
  done ;
  Array.init n begin fun k ->
    (s.(2*k+1) mod 2000 - 1000, s.(2*k+2) mod 2000 - 1000)
  end

module SetQ = Set.Make (Q)

let s n =
  (* [sets] associates to each direction (slope) [d] the set of all lines with
   * that direction; a line in this set is represented by its offset [y0]
   * (ordinate of the intersection with the Y axis).
   * vertical lines are coded taking as direction ∞, and as offset their
   * intersection with the X axis. *)
  let sets : (Q.t, SetQ.t ref) Hashtbl.t = Hashtbl.create 1_204 in
  let points = points n in
  for i = 0 to n-1 do
    for j = i+1 to n-1 do
      let (x1, y1) = points.(i) in
      let (x2, y2) = points.(j) in
      let (a, b) = (x2-x1, y2-y1) in
      let d = if a = 0 then Q.inf else Q.(~$b / ~$a) in (* direction *)
      let y0 = if a = 0 then Q.(~$x1) else Q.(~$y1 - d * ~$x1) in (* offset *)
      begin match Hashtbl.find_opt sets d with
      | Some set -> set := SetQ.add y0 !set
      | None     -> Hashtbl.add sets d (ref (SetQ.singleton y0))
      end
    done
  done ;
  let total_count = ref 0 in
  sets |> Hashtbl.iter begin fun _ set ->
    total_count := !total_count + SetQ.cardinal !set
  end ;
  let s = ref 0 in
  sets |> Hashtbl.iter begin fun _ set ->
    let card = SetQ.cardinal !set in
    s := !s + card * (!total_count - card)
  end ;
  !s

let () =
  assert (points 3 = [|(527, 144); (-488, 732); (-454, -947)|]) ;
  assert (s 3 = 6) ;
  assert (s 100 = 24_477_690)

let () =
  Printf.printf "%u\n" (s 2_500)
