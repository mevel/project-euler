#!/usr/bin/env ocaml

(*
 *  Project Euler #688
 *)

(* Explanation: see Mike’s excellent answer in the forum (3rd post):
 *     https://projecteuler.net/thread=688#343705
 *
 * In one word, Fubini. :-) In a few more words, we have a double sum:
 *
 *   S(N) = ∑∑_{n,k} f(n,k)
 *
 * where the range of the summation is k(k+1)/2 ≤ n ≤ N
 * (in particular, k = O(√N), k ≤ (√(8n+1)−1)/2).
 * The statement of the problem suggests to compute it like this:
 *
 *   ∑_n ∑_k f(n,k)
 *
 * but we rather compute it like that:
 *
 *   ∑_k ∑_n f(n,k)
 *
 * It turns out the inner sum can be solved to a closed form, by regrouping the
 * possible n into buckets in which the quotient of dividing by k is the same.
 * All these buckets have exactly k elements, except perhaps the last one which
 * has the remaining elements.
 *
 * We compute the inner sum in O(1), hence we compute the outer sum in O(√N).
 * I think that, by starting from the closed form of the inner sum, we might
 * even try harder and give a closed form for the outer sum as well, but I did
 * not try, it was too boring.
 *
 * Closed form for f(n,k):
 *
 *   f(n,k) = floor( n/k − (k−1)/2 ) = floor( (n − k(k−1)/2) / k )
 *
 *)

let modulus = 1_000_000_007

(* Compute just as few modulos as needed for the problem: *)
let ( +: ) a b = (a + b) mod modulus
let ( *:: ) a b = (a mod modulus) * (b mod modulus)

let compute_k_max ~n =
  truncate (0.5 *. (sqrt (float (8*n+1)) -. 1.))

(* This is the naive code, only for testing our actual optimized code. *)

let f ~n ~k =
  (n - k*(k-1)/2) / k

let sum_k ~n =
  let sum = ref 0 in
  let k_max = compute_k_max ~n in
  for k = 1 to k_max do
    sum := !sum +: f ~n ~k
  done ;
  !sum

let sum_nk_naive ~n_max =
  let sum = ref 0 in
  for n = 1 to n_max do
    sum := !sum +: sum_k ~n
  done ;
  !sum

(* This is the optimized code. *)

(* My own formula, unnecessarily convoluted because I regarded f(n,k) as the
 * floor of a sum of fractions, floor(n/k − k(k−1)/2), and had to do case
 * distinction on the parity of k in order to simplify that to just a sum. *)
let sum_n ~n_max ~k =
  let l = k lsr 1 in
  let m_max = if k land 1 = 0 then n_max + l else n_max in
  let q = m_max / k
  and r = m_max mod k in
  (* This case distinction is only there to avoid overflowing the multiplication
   * by doing the division by 2 first, so that afterwards we can use modular
   * multiplication: *)
  k *:: ( if (q+l) land 1 = 0 then
            (q-l-1) *:: ((q+l) / 2)
          else
            ((q-l-1) / 2) *:: (q+l) )
  + (r+1) *:: q
  - l *:: (m_max-k*(l+1)+1)

(* Mike’s formula, more straightforward: regard f(n,k) as the floor of
 * a fraction of a sum (put everything on a common denominator) and do a change
 * of variables. Simpler formula, no case distinction needed! *)
let sum_n ~n_max ~k =
  let m_max = n_max - k * (k+1) / 2 + 1 in
  let q = m_max / k
  and r = m_max mod k in
  (* Same remark: the middle part computes q*(q+1)/2 carefully so as to avoid
   * overflowing the intermediate product, avoiding branching (slightly faster)
   * (Mike does it by multiplying by the inverse of 2 modulo MOD): *)
  k *:: ((q+1)/2) *:: (q+1-(q land 1)) + r *:: (q+1)

let sum_nk ~n_max =
  let sum = ref 0 in
  let k_max = compute_k_max ~n:n_max in
  for k = 1 to k_max do
    sum := !sum +: sum_n ~n_max ~k
  done ;
  !sum

let () =
  assert (f ~n:10 ~k:3 = 2) ;
  assert (f ~n:10 ~k:5 = 0) ;
  assert (sum_k ~n:100 = 275) ;
  for n_max = 1 to 100 do
    assert (sum_nk ~n_max = sum_nk_naive ~n_max) ;
  done ;
  assert (sum_nk ~n_max:100 = 12656) ;
  Printf.printf "%u\n" (sum_nk ~n_max:1_0000_0000_0000_0000 mod modulus)
