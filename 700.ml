#!/usr/bin/env ocaml

(*
 * Project Euler #700
 *)

(* I don’t really know why it works, but it does… I had the intuition that we
 * should look at the successive maximums at the same time as the successive
 * minimums, so I printed the first obtained values, then I saw the pattern
 * (the index of the next extremum, which is either a max or a min, is always
 * the sum of the indexes of the last min and of the last max) and wrote the
 * code below, which just worked.
 *
 * It really reminds of Euclid’s algorithm, but I’m not sure how to make it
 * formal. People on the forum confirmed this and gave a shorter algorithm,
 * but still no clear explanation as to WHY it works. :-(
 *
 * gcd(m, a) = 1.
 *
 * Euclid’s algorithm with inputs (m, a):
 *   m = q·a + r,  0 < r < a  if a > 1
 *   a = p·r + s,  0 < s < r  if r > 1
 *   r = o·s + t,  0 < t < s  if s > 1
 *   ...
 *
 * f(n) := a·n mod m
 *
 * values described by f(n), sorted (values are obtained from left to right
 * and from top to bottom, excepted 0 and m which are never reached but are
 * written here for reading purposes):
 *
 *                                                                                                 < m
 *                     < a                 < 2a                 < …                 < qa = m−r                     < (q+1)a = m+a−r
 *                 < a−r               < 2a−r               < …                 < qa−r = m−2r                  <* (q+1)a−r = m+a−2r  [*: if a > 2r, ie p ≥ 2]
 *             < a−2r              < 2a−2r              < …                 < qa−2r = m−3r                 <* (q+1)a−2r = m+a−3r     [*: if a > 3r, ie p ≥ 3]
 *            .·                  .·                   .·                  .·                             .·
 *         < a−(p−1)r          < 2a−(p−1)r          < …                 < qa−(p−1)r                    < (q+1)a−(p−1)r = m+a−pr = m+s
 *     < a−pr = s          < 2a−pr              < …                 < qa−pr                    < (q+1)a−pr = m+a−(p+1)r = m+s−r
 *   0
 *
 * so we find in order:
 *
 * - min&MAX: a               at n = 1
 * - MAX: 2a                  at n = 2
 * - ...
 * - MAX: qa = m−r            at n = q
 * - min: a−r                 at n =  q+1
 * - min: a−2r                at n = 2q+1
 * - ...
 * - min: a−pr = s            at n = pq+1
 * - MAX: m+a−(p+1)r = m−r+s  at n = (p+1)q+1
 * - then what?
 *
 *)

let rec gcd a b =
  assert (0 <= a) ;
  assert (0 <= b) ;
  if b = 0 then a else gcd b (a mod b)

let a = 1_504_170_715_041_707
let m = 4_503_599_627_370_517

let () =
  assert (gcd a m = 1) ;
  let n_last_min = ref 1 in let last_min = ref a in
  let n_last_max = ref 1 in let last_max = ref a in
  let sum_mins = ref a in
  while !last_min > 1 || !last_max < m-1 do
    let n = !n_last_min + !n_last_max in
    assert (n > 0) ;
    let y = (!last_min + !last_max) mod m in
    if y < !last_min then begin
      Printf.printf "new min (n = %#i): %#i\n%!" n y ;
      n_last_min := n ;
      last_min := y ;
      sum_mins := !sum_mins + y ;
      assert (!sum_mins > 0) ;
    end
    else if y > !last_max then begin
      Printf.printf "new max (n = %#i): %#i\n%!" n y ;
      n_last_max := n ;
      last_max := y ;
    end
    else
      assert false
  done ;
  Printf.printf "%i\n" !sum_mins ;
