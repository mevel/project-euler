(*
 *  Project Euler #775
 *)

open! Euler.Arith

module M = Euler.Modular.Make (struct let modulo = 1_000_000_007 end)

(**
 **  Naive computation
 **)

let f n =
  assert (0 < n) ;
  (* To minimize the surface of [n] unit cubes, we pack as much of them as
   * possible in a cube, whose side is therefore [a := floor(cuberoot(n))].
   * Then, we arrange the [r] remaining units along one to three sides of
   * that big cube.
   * NOTE: We rather take the preceding cube, i.e. [a := floor(cuberoot(n−1))],
   * so that the number of remaining units is non-zero; this reduces the number
   * of cases to treat separately. If [n] is a perfect cube, then the algorithm
   * builds the preceding cube, of side [a], then covers three of its sides
   * entirely (case 2), so that the result is the cube of side [a+1] as
   * expected. *)
  let a = icbrt (n-1) in
  let r = n - a*a*a in
  assert (1 <= r && r <= 3*a*a + 3*a + 1) ;
  (* CASE 0:
   * If [r ≤ a²] then we fit all units on a single side of the big cube,
   * of size [a×a]. *)
  if r <= a*a then begin
    (* Now we must arrange [r] units on a [a×a] plane while minimizing their
     * total perimeter. To do that, we pack as much of them as possible in
     * a square, whose side is therefore [b := floor(sqrt(n))].
     * The [c] remaining units are arranged along one side of the square if
     * [c ≤ b], or along two adjacent sides if [c > b].
     * NOTE: Same remark about taking the preceding square so as to leave at
     * least one unit. *)
    let b = isqrt (r-1) in
    let c = r - b*b in
    assert (n = a*a*a + b*b + c) ;
    assert (0 <= b && b < a) ;
    assert (1 <= c && c <= 2*b+1) ;
    6*a*a + 4*b + (if c <= b then 2 else 4)
  end
  else let s = r - a*a in
  (* CASE 1:
   * Otherwise, if [r ≤ a² + a(a+1)] then first we cover one of the sides of the
   * cube entirely, which gives us a rectangular cuboid of size [a×a×(a+1)],
   * then we fit the [s] remaining units along one side of size [a×(a+1)]. *)
  if s <= a*(a+1) then begin
    (* Similar logic as in case 0. *)
    let b = isqrt (s-1) in
    let c = s - b*b in
    assert (n = a*a*a + a*a + b*b + c) ;
    assert (0 <= b && (b < a || b = a && c <= a)) ;
    assert (1 <= c && c <= 2*b+1) ;
    6*a*a + 4*a + 4*b + (if c <= b then 2 else 4)
  end
  else let t = s - a*(a+1) in
  (* CASE 2:
   * Otherwise, we first cover two sides of the cube, which gives a rectangular
   * cuboid of size [a×(a+1)×(a+1)], then we fit the [t] remaining units along
   * one side of size [(a+1)×(a+1)]. *)
  begin
    assert (1 <= t && t <= a*a + 2*a + 1) ;
    (* Similar logic as in case 0. *)
    let b = isqrt (t-1) in
    let c = t - b*b in
    assert (n = a*a*a + 2*a*a + a + b*b + c) ;
    assert (0 <= b && b <= a) ;
    assert (1 <= c && c <= 2*b+1) ;
    6*a*a + 8*a + 4*b + 2 + (if c <= b then 2 else 4)
  end

let g n =
  assert (0 < n) ;
  6*n - f n

(* NOTE on overflows: [f(n)] is much smaller than [6n] (asymptotically we have
 * [f(n) ~ 6 n^(2/3)]), and we only consider [n ≤ 10^16] which is less than
 * [max_int/6] with 64-bit OCaml, so there is no overflow in [f]. *)

let sumg_naive ~from ~til =
  assert (0 < from) ;
  let sum = ref (M.of_int 0) in
  for n = from to til do
    sum := M.(!sum +:. (g n)) ;
  done ;
  !sum

(**
 **  Clever summation
 **)

(* Finding formulas for the sum of f is not hard (the only thing we need are
 * summations of monomials up to degree 4; see Faulhaber’s formulas for that),
 * but it is tedious because of the many cases. I used Sage (computer algebra
 * software) for deriving the formulas for the various pieces. See the attached
 * Sage/Jupyter notebook.
 *
 * Let’s reuse the notations from the code of [f] above… We’ll write
 *     n = Case0(a,b,c)
 * or  n = Case1(a,b,c)
 * or  n = Case2(a,b,c)
 * when [n] falls in the CASE 0, CASE 1 or CASE 2 of [f]. In each case, the
 * decompositions (a,b,c) follow the lexicographic order.
 *
 * NOTE: the decomposition is not unique on edge cases, e.g.
 *
 *   Case0(a, b, 2b+1) = Case0(a, b+1, 0)    if  b < a
 *   Case0(a, a−1, 2a−1) = Case0(a, a, 0) = Case1(a, 0, 0)
 *
 *   Case1(a, b, 2b+1) = Case1(a, b+1, 0)    if  b < a
 *   Case1(a, a, a) = Case2(a, 0, 0)
 *
 *   Case2(a, b, 2b+1) = Case2(a, b+1, 0)    if  b ≤ a
 *   Case2(a, a, 2a+1) = Case2(a, a+1, 0) = Case0(a+1, 0, 0)
 *)

(* For given [a,b,c], this is the sum of f over all Case0(a,b',c')
 * such that (a,b',c') ≤ (a,b,c) in lexicographic order.
 * In other words, this is the sum of f(n) for n in the range:
 *   from  a^3 + 1
 *   to    a^3 + b^2 + c *)
let sumf_case0_upto a b c =
  assert (0 <= b && (b < a || b = a && c = 0)) ;
  assert (0 <= c && c <= 2*b+1) ;
  let open! M in
  (* sum over all Case0(a,b',c') such that [b < b']: *)
  let s1 =
    let a = !:a and b = !:b in
    (18*.:a*:a*:b +: 8*.:b*:b +: 3*.:b +:. 1) *: b /:. 3
  (* sum over all Case0(a,b,c') such that [c ≤ c']: *)
  and s2 = M.of_int @@ (6*a*a + 4*b + 2)*c + 2*max 0 (c-b) in
  s1 +: s2

(* For given [a], this is the sum of f over all Case0(a,b',c').
 * In other words, this is the sum of f(n) for n in the range:
 *   from  a^3 + 1
 *   to    a^3 + a^2 *)
let sumf_case0 a =
  (*! sumf_case0_upto a (a-1) (2*a-1) !*)
  (* or: *)
  sumf_case0_upto a a 0
  (* or: *)
  (*! let open! M in !*)
  (*! let a = !:a in !*)
  (*! (18*.:a*:a*:a +: 8*.:a*:a +: 3*.:a +:. 1) *: a /:. 3 !*)

(* For given [a,b,c], this is the sum of f over all Case1(a,b',c')
 * such that (a,b',c') ≤ (a,b,c) in lexicographic order.
 * In other words, this is the sum of f(n) for n in the range:
 *   from  a^3 + a^2 + 1
 *   to    a^3 + a^2 + b^2 + c *)
let sumf_case1_upto a b c =
  assert (0 <= b && (b < a || b = a && c <= a)) ;
  assert (0 <= c && c <= 2*b+1) ;
  let open! M in
  (* sum over all Case1(a,b',c') such that [b < b']: *)
  let s1 =
    let a = !:a and b = !:b in
    (18*.:a*:a*:b +: 8*.:b*:b +: 12*.:a*:b +: 3*.:b +:. 1) *: b /:. 3
  (* sum over all Case1(a,b,c') such that [c ≤ c']: *)
  and s2 = M.of_int @@ (6*a*a + 4*a + 4*b + 2)*c + 2*max 0 (c-b) in
  s1 +: s2

(* For given [a], this is the sum of f over all Case1(a,b',c').
 * In other words, this is the sum of f(n) for n in the range:
 *   from  a^3 + a^2 + 1
 *   to    a^3 + 2a^2 + a *)
let sumf_case1 a =
  sumf_case1_upto a a a
  (* or: *)
  (*! let open! M in !*)
  (*! let a = !:a in !*)
  (*! (18*.:a*:a +: 20*.:a +:. 7) *: (a +:. 1) *: a /:. 3 !*)

(* For given [a,b,c], this is the sum of f over all Case2(a,b',c')
 * such that (a,b',c') ≤ (a,b,c) in lexicographic order.
 * In other words, this is the sum of f(n) for n in the range:
 *   from  (a^3 + 2a^2 + a) + 1
 *   to    (a^3 + 2a^2 + a) + b^2 + c *)
let sumf_case2_upto a b c =
  assert (0 <= b && (b <= a || b = a+1 && c = 0)) ;
  assert (0 <= c && c <= 2*b+1) ;
  let open! M in
  (* sum over all Case2(a,b',c') such that [b < b']: *)
  let s1 =
    let a = !:a and b = !:b in
    (18*.:a*:a*:b +: 8*.:b*:b +: 24*.:a*:b +: 9*.:b +:. 1) *: b /:. 3
  (* sum over all Case2(a,b,c') such that [c ≤ c']: *)
  and s2 = M.of_int @@ (6*a*a + 8*a + 4*b + 4)*c + 2*max 0 (c-b) in
  s1 +: s2

(* (unused)
 * For given [a], this is the sum of f over all Case2(a,b',c').
 * In other words, this is the sum of f(n) for n in the range:
 *   from  (a^3 + 2a^2 + a) + 1
 *   to    a^3 + 3a^2 + 3a + 1 = (a+1)^3 *)
let sumf_case2 a =
  (*! sumf_case2_upto a a (a*2+1) !*)
  (* or: *)
  sumf_case2_upto a (a+1) 0
  (* or: *)
  (*! let open! M in !*)
  (*! let a = !:a in !*)
  (*! (18*.:a*:a*:a +: 50*.:a*:a +: 49*.:a +:. 18) *: (a +:. 1) /:. 3 !*)

(* Sum of f(n) for n in the range:
 *   from  1
 *   to    a^3 *)
let sumf_upto_cube a =
  assert (0 <= a) ;
  let open! M in
  let a = !:a in
  (108*.:pow a 4 +: 15*.:pow a 3 +: 40*.:pow a 2 +: 15*.:a +:. 2) *: a /:. 30

(* Sum of f from 1 to n. *)
let sumf ~til:n =
  assert (0 < n) ;
  let a = icbrt n in
  let r = n - a*a*a in
  assert (0 <= r && r < 3*a*a + 3*a + 1) ;
  if r < a*a then begin
    let b = isqrt r in
    let c = r - b*b in
    assert (0 <= b && b < a) ;
    assert (0 <= c && c < 2*b+1) ;
    M.(sumf_upto_cube a
      +: sumf_case0_upto a b c)
  end
  else let s = r - a*a in
  if s < a*(a+1) then begin
    let b = isqrt s in
    let c = s - b*b in
    assert (0 <= b && (b < a || b = a && c < a)) ;
    assert (0 <= c && c < 2*b+1) ;
    M.(sumf_upto_cube a
      +: sumf_case0 a
      +: sumf_case1_upto a b c)
  end
  else let t = s - a*(a+1) in
  begin
    assert (0 <= t && t < a*a + 2*a + 1) ;
    let b = isqrt t in
    let c = t - b*b in
    assert (0 <= b && b <= a) ;
    assert (0 <= c && c < 2*b+1) ;
    M.(sumf_upto_cube a
      +: sumf_case0 a
      +: sumf_case1 a
      +: sumf_case2_upto a b c)
  end

let sumg ~til =
  assert (0 < til) ;
  M.(3*..til*:.(til+1) -: sumf ~til)

let () =
  assert (f 10 = 30) ;
  assert (f 18 = 42) ;
  assert (g 18 = 66) ;
  assert (sumg_naive ~from:1 ~til:18 = M.of_int 530) ;
  for n = 1 to 100 do
    assert (sumg_naive ~from:1 ~til:n = sumg ~til:n) ;
  done ;
  assert (sumg ~til:1_000_000 = M.of_int 951_640_919) ;
  Printf.printf "%i\n" (sumg ~til:10_000_000_000_000_000 :> int)
