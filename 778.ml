#!/usr/bin/env ocaml

(*
 * Project Euler #778
 *)

(* See message for explanations! *)

let next_pow ?(base=10) m =
  assert (2 <= base) ;
  assert (0 <= m) ;
  let p = ref 1 in
  while !p <= m do p := !p * base done ;
  !p

let number_of_digits ?(base=10) m =
  assert (2 <= base) ;
  assert (0 <= m) ;
  let k = ref 0 in
  let p = ref 1 in
  while !p <= m do p := !p * base ; incr k done ;
  !k

let rec ipow b n =
  assert (0 <= n) ;
  if n = 0 then
    1
  else if n land 1 = 0 then
    ipow (b*b) (n lsr 1)
  else
    b * ipow (b*b) (n lsr 1)

let () =
  assert (ipow 10 3 = 1_000)

(* We do as few modulo operations as necessary not to overflow: *)
let modulus = 1_000_000_009
let ( +: ) a b = (a + b) mod modulus
let ( *: ) a b = (a * b) mod modulus

(*
 * NAIVE CALCULATION, only for checking small values
 *)

let ( *? ) a b =
  assert (0 <= a) ;
  assert (0 <= b) ;
  let a = ref a in
  let b = ref b in
  let p10 = ref 1 in
  let mul = ref 0 in
  while !a > 0 && !b > 0 do
    mul := !mul + (!a * !b mod 10) * !p10 ;
    a := !a / 10 ;
    b := !b / 10 ;
    p10 := !p10 * 10 ;
  done ;
  !mul

let rec g ~m ~a ~r =
  assert (0 <= r) ;
  if r = 0 then
    a
  else begin
    let sum = ref 0 in
    for b = 1 to m do
      sum := !sum + g ~a:(a *? b) ~r:(r-1) ~m
    done ;
    !sum
  end

let f ~m ~r =
  assert (0 <= m) ;
  assert (0 <= r) ;
  let ones = (next_pow m - 1) / 9 in (* 11111…1 *)
  g ~m ~a:ones ~r

let g9 = g ~m:9
let f9 = f ~m:9

(*
 * FIRST APPROACH: We are able to compute F(r, 10^k−1) very efficiently with an
 * explicit formula. Unfortunately, that doesn’t tell us how to compute F(r, m)
 * in the general case.
 *)

(* The first improvement; still inefficient, as F(r, 9) is not computed
 * efficiently yet. *)
let f_smart ~k ~r =
  assert (0 <= k) ;
  assert (0 <= r) ;
  if k = 0 then 0 else f ~m:9 ~r * (ipow 10 k - 1) / 9 * ipow 10 (r * (k-1))

(* Now this is efficient! *)
let f9_smarter ~r =
  assert (0 <= r) ;
  if r = 0 then 1 else 5 * (ipow 8 r - ipow 4 r + ipow 5 r)

let f_smarter ~k ~r =
  assert (0 <= k) ;
  assert (0 <= r) ;
  if k = 0 then 0 else f9_smarter ~r * (ipow 10 k - 1) / 9 * ipow 10 (r * (k-1))

(*
 * SECOND APPROACH: This one actually solves the problem. :-)
 *)

let f_clever ~m ~r =
  assert (0 <= m) ;
  assert (0 <= r) ;
  let sum = ref 0 in
  let p = ref 1 in (* [p = 10^k] *)
  let mquo = ref m in (* [mquo = m / 10^k] *)
  let mrem = ref 0 in (* [mrem = m % 10^k] *)
  for _k = 0 to number_of_digits m - 1 do
    assert (!mquo > 0 && !mrem >= 0 && !p >= 1 && m = !mquo * !p + !mrem) ;
    (* [m] is the concatenation of three parts:
     * - [mquo']: the higher digits
     * - [m_k]: the digit in position k
     * - [mrem]: the lower digits *)
    let mquo' = !mquo / 10 in
    let m_k = !mquo mod 10 in
    (* For all 0 ≤ [a] ≤ 9 we compute [counts.(a)], the count of integers between
     * 0 and [m] inclusive whose digit in position [k] is equal to [a]. *)
    let counts = Array.init 10 (fun a ->
        mquo' * !p + (if a < m_k then !p else if a = m_k then !mrem + 1 else 0)
      ) in
    (* We compute [G_k(a, r, m)] for all 0 ≤ [a] ≤ 9 by linear recursion on [r].
     * Row [i+1] is computed from row [i] only, so (optimization trick) we only
     * allocate two arrays and reuse them by swapping them after each iteration.
     * Ultimately, we are only interested in [G_k(1, r, m)].
     * NOTE: This is where the algorithm is still slow: O(r). We can reduce it
     * to O(log r) by writing the linear recurrence system as a matrix and doing
     * exponentation-by-squaring on that matrix. *)
    let old_gk = ref @@ Array.make 10 0 in
    let cur_gk = ref @@ Array.init 10 Fun.id in
    for _i = 1 to r do
      let gk  = !cur_gk in
      let gk' = !old_gk in
      old_gk := gk ;
      cur_gk := gk' ;
      for a = 1 to 9 do
        gk'.(a) <- 0 ;
        for b = 1 to 9 do
          gk'.(a) <- gk'.(a) + counts.(b) *: gk.(a * b mod 10)
        done ;
      done ;
    done ;
    sum := !sum + !cur_gk.(1) *: !p ;
    mquo := mquo' ;
    mrem := !mrem + m_k * !p ;
    p := !p * 10 ;
  done ;
  assert (!mquo = 0 && !mrem = m && !p > m) ;
  let res = !sum mod modulus in
  (* just some sanity checks: *)
  if m = 0 then assert (res = 0) ;
  if m = 1 then assert (res = 1) ;
  if r = 0 then assert (res = (next_pow m - 1) / 9) ;
  if r = 1 then assert (res = m * (m + 1) / 2) ;
  res

(*
 * TESTING
 *)

let () =

  (* NAIVE *)
  assert (234 *? 765 = 480) ;
  assert (f ~m:7 ~r:2 = 204) ;
  (*! assert (f ~m:76 ~r:23 = 5870548) ; !*) (* [f] is too slow *)

  (* COMPUTING FIRST VALUES using the naive code *)
  assert (g9 ~a:2 ~r:0 = 2) ;
  assert (g9 ~a:2 ~r:1 = 40) ;
  assert (g9 ~a:2 ~r:2 = 320) ;
  assert (g9 ~a:2 ~r:3 = 2560) ;
  assert (g9 ~a:2 ~r:4 = 20480) ;
  assert (f9 ~r:0 = 1) ;
  assert (f9 ~r:1 = 45) ;
  assert (f9 ~r:2 = 365) ;
  assert (f9 ~r:3 = 2865) ;
  assert (f9 ~r:4 = 22325) ;
  (*! for m = 2 to 9 do !*)
  (*!   for r = 1 to 7 do !*)
  (*!     Printf.eprintf "f%u(%u) = %u\n" m r (f ~m ~r) !*)
  (*!   done !*)
  (*! done ; !*)

  (* SMART *)
  assert (f_smart ~k:1 ~r:1 = f ~m:9 ~r:1) ;
  assert (f_smart ~k:1 ~r:2 = f ~m:9 ~r:2) ;
  assert (f_smart ~k:1 ~r:3 = f ~m:9 ~r:3) ;
  assert (f_smart ~k:2 ~r:1 = f ~m:99 ~r:1) ;
  assert (f_smart ~k:2 ~r:2 = f ~m:99 ~r:2) ;
  assert (f_smart ~k:2 ~r:3 = f ~m:99 ~r:3) ;
  assert (f_smart ~k:3 ~r:1 = f ~m:999 ~r:1) ;
  assert (f_smart ~k:3 ~r:2 = f ~m:999 ~r:2) ;
  (*! assert (f_smart ~k:3 ~r:3 = f ~m:999 ~r:3) ; !*) (* [f] is too slow *)

  (* SMARTER *)
  for r = 0 to 7 do
    assert (f9_smarter ~r = f9 ~r) ;
  done ;
  for k = 0 to 7 do
    for r = 0 to 7 do
      assert (f_smarter ~k ~r = f_smart ~k ~r) ;
    done ;
  done ;

  (* CLEVER *)
  [0; 1; 2; 3; 4; 5; 6; 7; 8; 9; 10; 11; 12; 19; 20; 21] |>
  List.iter begin fun m ->
    for r = 0 to 5 do
      (*! Printf.eprintf "checking m=%u, r=%u…\n%!" m r ; !*)
      assert (f_clever ~m ~r = f ~m ~r)
    done
  end ;
  [99; 100; 101] |>
  List.iter begin fun m ->
    for r = 0 to 3 do
      (*! Printf.eprintf "checking m=%u, r=%u…\n%!" m r ; !*)
      assert (f_clever ~m ~r = f ~m ~r)
    done
  end ;
  [999; 1000; 1001] |>
  List.iter begin fun m ->
    for r = 0 to 2 do
      (*! Printf.eprintf "checking m=%u, r=%u…\n%!" m r ; !*)
      assert (f_clever ~m ~r = f ~m ~r)
    done
  end ;
  assert (f_clever ~m:7 ~r:2 = 204) ;
  assert (f_clever ~m:76 ~r:23 = 5870548) ;
  Printf.printf "answer: %u\n" (f_clever ~m:765432 ~r:234567) ;
