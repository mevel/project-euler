#!/bin/sh

# Compiles the file $1.c with the template.
gcc -Wall -Wextra -pedantic -std=c99 \
  bignum.c template.c -DCPL_FILE="\"$1.c\"" -lm  -o "$1.exe"

# Executes the program if the compilation did not failed.
if [ $? -eq 0 ] ; then
    "./$1.exe"
fi
