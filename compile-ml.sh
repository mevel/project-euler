#!/bin/sh

# Remove the shebang, if any (leave a blank line, to preserve line numbers).
tmp="$(mktemp "compile-ml.XXX.ml")"
sed '1s/^#!.*$//' < "$1.ml" > "$tmp"

# Compile the file $1.ml with useful libraries and optimizations.
ocamlfind ocamlopt -package zarith -package euler -linkpkg \
  -O3 -inline 3 -unboxed-types -g \
  -w -24 "$tmp" -o "$1.exe"
  #-noassert

status=$?

# Cleanup.
rm -f -- "${tmp%.ml}".*

# Execute the program if the compilation did not failed.
if [ $status -eq 0 ] ; then
  time "./$1.exe"
fi
