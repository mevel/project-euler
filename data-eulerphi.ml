(**
 ** Calcule bourrinement l’indicatrice d’Euler φ(n) pour tout 1 < n < nmax.
 ** Calcule du même coup la liste des nombres premiers entre 2 et nmax.
 **
 **)

let nmax = max_int

(* fichiers où écrire les données calculées : *)
let file_primes = open_out (Printf.sprintf "data-primes-under-%u.out" nmax)
let file_phi    = open_out (Printf.sprintf "data-eulerphi-under-%u.out" nmax)

(* taille pour la mémoïsation de φ(n) : *)
let mem_max = (*40_000_000*)Sys.max_array_length


(* Implémentation d’une liste chaînée circulaire (pour stocker la liste des
 * nombres premiers et pouvoir y ajouter un élément en temps constant). *)
module DLList = struct
  type 'a t = {mutable prev: 'a t; v: 'a; mutable next: 'a t}
  let singleton x = let rec  r = {prev = r; v = x; next = r} in r
  let concat l m =
    let tl = l.prev in
    l.prev.next <- m;
    m.prev.next <- l;
    l.prev      <- m.prev;
    m.prev      <- tl
  let add l x = concat l (singleton x)
  let to_list l =
    let rec aux {prev;v;next} =
      v :: (if next == l then [] else aux next)
    in
    aux l
end


let phi =
  let mem = Array.make mem_max 0 in
  mem.(1) <- 1;
  let primes = DLList.singleton 0 in
  let rec phi_rec n acc primes' =
    if n < mem_max && mem.(n) > 0 then
      acc * mem.(n)
    else
      search_prime_factor n acc primes'
  and search_prime_factor n acc {DLList.prev; DLList.v = p; DLList.next} =
    if p = 0 || p*p > n then begin
      DLList.add primes n;
      acc * (n-1)
    end else if n mod p = 0 then begin
      let n' = ref n in
      while !n' mod p = 0 do n' := !n' / p done;
      let powp = n / !n' in
      phi_rec !n' (acc*powp/p*(p-1)) next
    end else
      search_prime_factor n acc next
  in
  fun n ->
    let res = phi_rec n 1 primes.DLList.next in
    if n < mem_max then
      mem.(n) <- res;
    res


let () =
  for n = 1 to nmax do
    let res = phi n in
    Printf.fprintf file_phi "φ(%i) = %i\n%!" n res;
    if res = n-1 then
      Printf.fprintf file_primes "%u\n%!" n;
  done
