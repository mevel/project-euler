(* the first N lines to skip: *)
let lines_to_skip = 0

let () =
  let fin = Scanf.Scanning.from_channel stdin in
  let fout = stdout in
  for _ = 1 to lines_to_skip do
    Scanf.bscanf fin "%_s@\n" ()
  done ;
  begin try while true do
    (* "%_1[\r]@\n" is a format trick that matches \n, \r\n and end-of-file. *)
    Scanf.bscanf fin "φ(%u) = %u%_1[\r]@\n" @@fun n phi_n ->
    (* insert here what to do with n and phi_n;
     * for instance, print n if n is prime: *)
    if phi_n + 1 = n then
      Printf.fprintf fout "%u\n" n
  done with End_of_file -> () end ;
  Scanf.Scanning.close_in fin ;
  close_out fout
