/* 
 *  Projet Euler  #9
 */
#include <stdio.h>
#include <stdint.h>



#define  MAXPERIM  10000



uintmax_t  gcd
  (uintmax_t a, uintmax_t b)
{
	return (a%b) ? gcd(b, a%b) : b;
}



uintmax_t  nextCoprime
  (uintmax_t n, uintmax_t coprime)
{
	while(gcd(n, coprime) > 1)
		coprime += 2;
	
	return coprime;
}



/*
	
	| a + b + c = p   ⇔   c = p-a-b
	| a² + b² = c²
	⇔   a² + b² = (p-a-b)²
	⇔   …   
	⇔   0 = p² - 2pa - 2pb + 2ab
	⇔   b = p(p-2a) / (2(p-a))
	
	So we must find a such as b is an integer (p(p-2a) is divisible by 2(p-a)).
	
	x² + y² + z²
	⇔  | x = k(m²-n²)        m and n coprime and of opposite parities, m > n
	   | y = 2k(mn)
	   | z = k(m²+n²)
	⇒   p = a + b + c = 2km(m-n)
	
*/
uintmax_t  countPythagoreanTriplets
  (uintmax_t p)
{
	uintmax_t count;
	uintmax_t a, b, c;
	uintmax_t m, n, k;
	
	if(p%2)
		return 0;
	
	/*n = 0;
	for(a = 1;  a  <=  p/2 * (p-2*a) / (p-a);  a++) {
		/* If b is an integer… * /
		if(!( p/2 * (p-2*a) % (p-a) )) {
			b = p/2 * (p-2*a) / (p-a);
			c = p - a - b;
			if(!( c % (b-a) )) {
				n++;
				//printf("[%u] %u² + %u² = %u²\n", p, a, b, c);
			}
		}
	}*/
	
	count = 0;
	for(n = 1;  n < p/2;  n++) {
		for(m = nextCoprime(n, n+1);  m < p/2/*2*m*(m-n) < p*/;  m = nextCoprime(n, m+2)) {
			for(k = 1;  2*k*m*(m+n) < p;  k++) {
				a = k*(m*m - n*n);
				b = k*(2*m*n);
				c = k*(m*m + n*n);
				if(!( c % (b-a) )) {
					count++;
					//printf("(%ju,%ju)*%-2ju   %ju² + %ju² = %ju²\n", m, n, k, a, b, c);
				}
			}
		}
	}
	
	return count;
}



int main(void)
{
	uintmax_t n = 0;
	
	/*for(uintmax_t p = 0;  p < MAXPERIM;  p++)
		n += countPythagoreanTriplets(p);*/
	n = countPythagoreanTriplets(MAXPERIM);
	
	printf(
	  "number of “tilable” Pythagorean triplets with perimeter <= %ju:\n"
	  "    %ju\n",
	  (uintmax_t)MAXPERIM, n
	);
	return 0;
}
