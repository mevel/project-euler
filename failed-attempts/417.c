/*
 *  Project Euler  #417
 */
#include <stdint.h>
#include <limits.h>
#include <stdio.h>

#define  BASE  10
#define  UPPER  1000000
#define  UPPER  10000



unsigned  lcm
  (unsigned a, unsigned b)
{
	unsigned m = a;
	while(m % b)
		m += a;
	return m;
}



unsigned  cycle_prime
  (unsigned n)
{
	unsigned len;
	unsigned rem;
	
	rem = BASE % n;
	for(len = 1;  rem != 1 && rem != n-1;  len++)
		rem = (BASE*rem) % n;
	
	return rem==1 ? len : 2*len;
}



unsigned  recurring_cycle_length
  (unsigned n)
{
	/*unsigned len;
	unsigned rem;
	
	while(n%2 == 0)
		n /= 2;
	while(n%5 == 0)
		n /= 5;
	if(n == 1)
		return 0;
	
	rem = BASE % n;
	for(len = 1;  rem != 1 && rem != n-1;  len++)
		rem = (BASE*rem) % n;
	
	return rem==1 ? len : 2*len;*/
	
	unsigned d, p, len;
	
	while(n%2 == 0)
		n /= 2;
	while(n%5 == 0)
		n /= 5;
	if(n == 1)
		return 0;
	
	len = 1;
	for(d = 3;  n != 1;  d+=2) {
		if(n % d == 0) {
			p = 1;
			do {
				p *= d;
				n /= d;
			} while(n % d == 0);
			len = lcm(len, cycle_prime(p));
		}
	}
	
	return len;
	
	/*unsigned p10[sizeof(unsigned)*CHAR_BIT+1];
	
	while(n%2 == 0)
		n /= 2;
	while(n%5 == 0)
		n /= 5;
	if(n == 1)
		return 0;
	
	p = phi(n);
	
	p10[1] = BASE % n;
	for(size_t i = 2;  i < sizeof(p10)/sizeof(*p10);  i++)
		p10[i] = (p10[i-1]*p10[i-1]) % n;
	
	for(len = 1;  ;  len++) {
		if(p % len == 0) {
			rem = 1;
			d = len;
			for(size_t i = 1;  d;  i++) {
				if(d % 2)
					rem = (rem * p10[i]) % n;
				d /= 2;
			}
			if(rem == 1 || rem == n-1)
				break;
		}
	}
	
	return rem==1 ? len : 2*len;*/
}


uintmax_t sum_cycles0
  (unsigned n)
{
	uintmax_t sum;
	unsigned i;
	
	sum = 0;
	for(i = 3;  i <= n;  i++)
		sum += recurring_cycle_length(i);

	return sum;
}


uintmax_t sum_cycles
  (unsigned n, uintmax_t* odd_sum, uintmax_t* total_sum)
{
	uintmax_t sum;
	unsigned i;
	
	if(n < 3) {
		*odd_sum = *total_sum = 0;
		return 0;
	}
	
	sum = 0;
	for(i = (n/2)+1+(n/2)%2;  i <= n;  i+=2)
		sum += recurring_cycle_length(i);
	
	sum_cycles(n/2, odd_sum, total_sum);
	
	*odd_sum += sum;
	*total_sum += *odd_sum;
	
	return *total_sum;
}


int main(void)
{
	//printf("%u\n", recurring_cycle_length(983));
	/*for(unsigned i = 1;  i <= 10000;  i++)
		recurring_cycle_length(983);*/
	
	//unsigned n;
	//uintmax_t sum;
	
	/*sum = 0;
	for(n = 3;  n <= UPPER;  n++)
		sum += recurring_cycle_length(n);
	printf("%ju\n", sum);*/
	/*printf("%ju\n", sum_cycles0(UPPER));//*/
	uintmax_t odd, total;
	printf("%ju\n", sum_cycles(UPPER, &odd, &total));//*/
	/*printf(
	  "Number n below %u for which 1/n has the longest recurring cycle"
	  " (in base %u): %u (length of the cycle: %u)\n",
	  UPPER, BASE, nmax, lenmax);*/
	
	return 0;
}

