#!/usr/bin/env ocaml

(*
 * Project Euler #549
 *)

let rec fact n =
  assert (0 <= n) ;
  assert (n <= 20) ;
  if n < 2 then 1 else n * fact (n-1)

(* almost naive factorization sieve: *)
let compute ~nmax =
  let exception Break in
  let s = Array.make (nmax+1) 0 in
  for n = 2 to nmax do
    if s.(n) = 0 then begin
      Printf.eprintf "|  prime: %i\n%!" n ;
      let n_pow_k = ref 1 in (* [n_pow_k = n^k] *)
      let n_mul_k = ref 0 in (* [n_mul_k = n×k] *)
      (* for all [k] such that [n^k ≤ nmax]… *)
      for k = 1 to truncate @@ log (float nmax) /. log (float n) do
        n_pow_k := !n_pow_k * n ; (* no overflow because n^k ≤ nmax ≤ max_int *)
        n_mul_k := !n_mul_k + n ;
        Printf.eprintf "|    k = %i,  n^k = %i,  n×k = %i\n%!"
          k !n_pow_k !n_mul_k ;
        (* iterate directly on numbers divisible by [n^k] but not by [n^(k+1)]
         * (that way, we avoid computing repeated divisions): *)
        (*! begin try for i = 0 to max_int do !*)
        (*!   for j = 1 to n-1 do !*)
        (*!     let m = !n_pow_k * (n*i + j) in (* beware of overflows! *) !*)
        (*!     Printf.eprintf "|      m = %u = n^k × %i\n%!" m (n*i + j) ; !*)
        (*!     if m > nmax then raise Break ; !*)
        (*!     s.(m) <- max s.(m) !n_mul_k !*)
        (*!   done !*)
        (*! done with Break -> () end !*)
        for i = 1 to nmax / !n_pow_k do
          let m = !n_pow_k * i in (* beware of overflows! *)
          Printf.eprintf "|      m = %u = n^k × %i\n%!" m i ;
          s.(m) <- max s.(m) !n_mul_k
        done
      done
    end ;
    Printf.eprintf "s(%i) = %i\n%!" n s.(n) ;
    if s.(n) <= 20 then assert (fact s.(n) mod n = 0) ;
  done ;
  if nmax >= 10 then assert (s.(10) = 5) ;
  if nmax >= 25 then assert (s.(25) = 10) ;
  Array.fold_left (+) 0 s

let nmax = 100_000_000

let () =
  Printf.printf "%i\n" @@ compute ~nmax:100 ;
  (*! Printf.printf "%i\n" @@ compute ~nmax ; !*)
