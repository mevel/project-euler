(*
let primes = Array.make 300_000 0

let nprimes = ref 0

let add_prime p =
  primes.(!nprimes) <- p ;
  incr nprimes

let is_prime n =
  let r = ref true in
  let i = ref 0 in
  while !r && !i < !nprimes && primes.(!i) * primes.(!i) <= n do
    r := 0 <> n mod primes.(!i) ;
    incr i
  done ;
  !r

let () =
  add_prime 2 ;
  let n = ref 3 in
  for i = 1 to Array.length primes - 1 do
    while not @@ is_prime !n do
      n := !n + 2
    done ;
    add_prime !n ;
    n := !n + 2
  done
*)

let (mod) a b =
  if a < 0 then
    Pervasives.(a mod b + abs b)
  else
    Pervasives.(a mod b)

let ( / ) a b =
  let r = a mod b in
  Pervasives.((a - r) / b)

let rec bezout a b =
  if a = 0 then
    (0, 1)
  else
    let (u, v) = bezout (b mod a) a in
    (v - u * (b / a), u)

let rec bezout_mod p a b =
  if a = 0 then
    (0, 1)
  else
    let (u, v) = bezout_mod p (b mod a) a in
    ((v - u * (b / a)) mod p, u)

let pgcd a b =
  let (u, v) = bezout a b in
  u * a + b * v

let bezout_min a b n =
  Printf.eprintf "\tbezout_min %i %i %i\n" a b n ;
  assert (abs @@ pgcd a b = 1) ;
  assert (a * b < 0) ;
  let (u, v) = bezout_mod b a b in
  (n mod b * u mod b, n * v mod a)

let () =
  let x = ref 0 in
  let q = ref 1 in
  for i = 0 to 10 do
    let p = primes.(i) in
    let (a, _) = bezout_min !q ~-p (succ i - !x) in
    x := !x + !q * a ;
    Printf.printf "a %i = %i (q = %i)\n" i !x !q ;
    q := !q * p ;
  done

(*
let test a b =
  let (u, v) = bezout a b in
  Printf.printf "%i × %i  +  %i × %i   =   %i\n" a u b v (a*u + b*v) ;
  let (q, q') = (~-(u / b), v / a) in
  Printf.printf "     %i          %i\n" (u + b * q) (v - a * q') ;
  assert (q = q')
*)
