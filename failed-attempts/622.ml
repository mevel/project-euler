#!/bin/env ocaml

(*
 *  Project Euler #622
 *)

let primes_sieve nmax =
  let sieve = Array.init nmax (fun i -> i mod 2 <> 0) in
  sieve.(2) <- true ;
  let i = ref 1 in
  while i := !i+2; !i < nmax do
    if sieve.(!i) then
      let j = ref !i in
      while j := !j+ !i; !j < nmax do
        sieve.(!j) <- false
      done
  done;
  sieve
let primes_list nmax =
  let sieve = primes_sieve nmax in
  let rec aux i =
    if i >= nmax then []
    else if sieve.(i) then i :: aux (i+2)
    else                        aux (i+2)
  in
  2 :: aux 3

let primes = primes_list 100_000

(* précondition: [primes] est triée et contient aux moins tous les nombres
 * premiers inférieurs à √n. *)
let is_prime ~primes n =
  let rec test primes =
    begin match primes with
    | []           -> true
    | p :: primes' -> p * p > n || (n mod p <> 0 && test primes')
    end
  in
  n > 1 && test primes

let rec gcd a b =
  if b = 0 then a
  else gcd b (a mod b)

let lcm a b =
  a / gcd a b * b

(*
let f n i =
  if i mod 2 = 0 then i/2
  else n/2 + i/2
*)
let f n i =
  if i < n/2 then 2*i
  else 2*i-n+1

let order n i =
  let c = ref 0 in
  let j = ref i in
  while
    incr c ;
    j := f n !j ;
    !j <> i
  do () done ;
  !c

let order_of_permutation n =
  let deck = Array.make n true in
  let c = ref 1 in
  for i = 0 to n-1 do
    if deck.(i) then begin
      let d = ref 0 in
      let j = ref i in
      while
        deck.(!j) <- false ;
        incr d ;
        j := f n !j ;
        !j <> i
      do () done ;
      c := lcm !c !d
    end
  done ;
  !c

let () =
  for p = 1 to 256 do
    let n = 2*p in
(*
    let o = order_of_permutation n in
    Printf.printf "%4u:  %4u\n" n r ;
*)
    let o = order n 1 in
    Printf.printf "%4u:  %4u  [" n o ;
    let os = Array.make (n/2) 1 in
    for i = 1 to n/2 - 1 do
      assert (order n i = order n (n-1-i)) ;
      let d = o / order n i in
      assert (d <= i && (d = 1 || n mod (2*i) = i+1 || not @@ is_prime ~primes i)) ;
      if d <> 1 then
        Printf.printf " %2u" d
      else
        Printf.printf " --" ;
      if n mod (2*i) = i+1 && n <> i+1 then begin (* implique 3*i < n *)
        assert (d mod os.(i) = 0) ;
        let d' = d / os.(i) in
        if os.(i) <> 1 && d' <> 1 then
          Printf.eprintf "[%4u, %4u]:  %3u × %3u\n" n i os.(i) d' ;
        for j = 1 to (n/2 - 1) / i do
          os.(j*i) <- os.(j*i) * d'
        done
      end else
        assert (os.(i) = d)
    done ;
    Printf.printf " ]%u\n" (n/2)
(*     if o = 60 then Printf.printf "%4u\n" n *)
  done

(* théorème 0 :
 *
 * il y a une symétrie 0…n−1 ↔ n−1…0.
 * en particulier, l’ordre de k égale l’ordre de n−1−k.
 *)

(* théorème 1 :
 *
 * l’ordre de la permutation est au moins égal à ~ log2 n.
 *)

(* conjecture 1 (vérifiée jusqu’à n = 10 000):
 *
 * l’ordre de la permutation est égal à l’ordre de 1 (et de n/2).
 *
 * (ordre d’un élément = cardinal de son orbite;
 *  degré d’un élément = ordre de la permutation ∕ ordre de l’élement)
 *
 *)

(* conjecture 2 (vérifiée jusqu’à n = 1 000):
 *
 * plus généralement, le degré de k pour k < n/2 est toujours inférieur à k;
 * de plus, si le degré de k est différent de 1, alors il existe un facteur
 * premier p de k tel que n ≡ p+1 [mod 2p].
 *
 * ça implique la conjecture 1.
 *
 * rem: si k = 2^p alors k est dans le cycle de 1 et donc est de degré 1.
 *)
