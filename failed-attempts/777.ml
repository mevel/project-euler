#!/usr/bin/env ocaml

(*
 *  Project Euler #777
 *)

(* TODO *)

let pi = 4. *. atan 1.

let (/..) x y = float x /. float y

(* Returns (−1)^k. *)
let sign_parity k =
  1 - ((k land 1) lsl 1)

let rec gcd a b =
  assert (0 < a) ;
  assert (0 < b) ;
  let r = a mod b in
  if r = 0 then b else gcd b r

let iter_intersections a b ~f =
  assert (2 <= a) ;
  assert (2 <= b) ;
  (*! assert (gcd a b = 1) ; !*)
  let d = gcd a 10 in
  let e = gcd b 10 in
  let good = (d*e <> 10) in
  let two_ab = 2*a*b in
  (* PM: *)
  for l = 1 to b-1 do
        let x = (* float (sign_parity k) *. *) cos (pi *. (l*a/..b)) in
    let k_min = l*a/b + 1 in
    for k = k_min to 2*a - k_min do
      if good
      || let ten_k = 10*k in ten_k mod a <> 0
      || (ten_k / a - 1) mod d <> 0
      then begin
        (*! let t' = 0.5 *. (k/..a -. l/..b) in !*)
        (*! let u' = 0.5 *. (k/..a +. l/..b) in !*)
        let m = k*b - l*a
        and n = k*b + l*a in
        (*! let t' = m /.. two_ab !*)
        (*! and u' = n /.. two_ab in !*)
        let y = (* float (sign_parity l) *. *) cos (pi *. (k*b/..a -. b/..10)) in
        (**)
        Printf.eprintf
          "intersection:  t' = %u/%u,  u' = %u/%u,  (x,y) = (%+.2g, %+.8g)\n"
          m two_ab n two_ab x y ;
        (**)
        f x y ;
      end ;
    done ;
  done ;
  (* MP: *)
  for k = 1 to a-1 do
        let y = (* float (sign_parity l) *. *) cos (pi *. (k*b/..a)) in
    let l_min = k*b/a + 1 in
    for l = l_min to 2*b - l_min do (* FIXME *)
      if good
      || let ten_l = 10*l in ten_l mod b <> 0
      || (ten_l / b + 1) mod e <> 0
      then begin
        (*! let t' = 0.5 *. (l/..b -. k/..a) in !*)
        (*! let u' = 0.5 *. (l/..b +. k/..a) in !*)
        let m = - k*b + l*a + a*b / 10
        and n =   k*b + l*a + a*b / 10 in
        (*! let t' = m /.. two_ab !*)
        (*! and u' = n /.. two_ab in !*)
        let x = (* float (sign_parity k) *. *) cos (pi *. (l*a/..b +. a/..10)) in
        (**)
        Printf.eprintf
          "intersection:  t' = %u/%u,  u' = %u/%u,  (x,y) = (%+.2g, %+.8g)\n"
          m two_ab n two_ab x y ;
        (**)
        f x y ;
      end ;
    done ;
  done

let sum a b =
  let s = ref 0.0 in
  iter_intersections a b ~f:begin fun x y ->
    s := !s +. x*.x +. y*.y ;
  end ;
  if a*b mod 10 = 0 then begin
    let s' = 0.25 *. !s in
    (*! Printf.eprintf "sum(%u, %u) = %g  (= %g / 4)\n" a b s' !s ; !*)
    s'
  end else begin
    (*! Printf.eprintf "sum(%u, %u) = %g\n" a b !s ; !*)
    !s
  end

let sum_of_sums m =
  let ss = ref 0.0 in
  for a = 2 to m do
    for b = 2 to m do
      if gcd a b = 1 then begin
        ss := !ss +. sum a b
      end
    done
  done ;
  Printf.eprintf "sum_of_sums(%u) = %g\n" m !ss ;
  !ss

let fprecision = 1e-12

let (=~) x y =
  let m = 0.5 *. (abs_float x +. abs_float y) in
  abs_float (x -. y) < fprecision *. m

let () =
  (*! assert (sum 2 3 =~ 4.5) ; !*)
  assert (sum 2 5 =~ 0.75) ; (* = 3 / 4 *)
  (*! assert (sum 7 4 =~ 39.5) ; !*)
  (*! assert (sum 7 5 =~ 52.0) ; !*)
  (*! assert (sum 10 7 =~ 23.25) ; (* = 93 / 4 *) !*)
  (*! for half_b = 1 to 10 do !*)
  (*!   ignore @@ sum 2 (2*half_b + 1) ; !*)
  (*! done ; !*)
  (*! for third_b = 1 to 10 do !*)
  (*!   ignore @@ sum 3 (3*third_b - 1) ; !*)
  (*!   ignore @@ sum 3 (3*third_b + 1) ; !*)
  (*! done ; !*)
  (*! assert (sum_of_sums 10 =~ 1602.5) ; !*)
  (*! assert (sum_of_sums 100 =~ 24256505.0) !*)
