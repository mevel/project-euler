#!/usr/bin/env ocaml

(*
 *  Project Euler #782
 *)

module A = Euler.Arith

(*

    c n k = c n (n²−k)

   +--+
   |  | a        a + b = n
   +--+------+   k = a² + b²
      |      |   l = n² - a² - b² = 2ab
      | b    |
      |      |
      +------+

    idea: pre-compute the sets S2 := { a²+b² | a+b = n }
      a² + b² = a² + (n−a)² = 2a² − 2an + n² = n² − 2a(n−a)
    D2 := { a²−b² | b < a ≤ n }
    S3 := { a²+b²+c² | a+b+c = n }
    R := { ab | a, b ≤ n }

    S =
      1×2
      + 2×(n−2)×2
      + 2×(n−2)×2
      + 2×(#S2 − 1 − (n−2) − (1 if n is even))×2
      + …

 *)

let c n k =
  assert (0 <= n) ;
  assert (n <= A.isqrt max_int) ; (* for avoiding overflows *)
  assert (0 <= k && k <= n*n) ;
  let l = n*n - k in
  let r = A.isqrt k
  and s = A.isqrt l in
  if k = 0 || l = 0 then
    1
  else if k mod n = 0 then
    2
  else if A.is_square ~root:r k || A.is_square ~root:s l then
    2
  else if is_sum_of_two_squares ~sum_sides:n k
       || is_sum_of_two_squares ~sum_sides:n l then
    2
  (* equivalent to the preceding: *)
  else if (l mod 2 = 0 && is_bounded_rectangle ~max_side:n l)
       || (k mod 2 = 0 && is_bounded_rectangle ~max_side:n k) then
    2
  else if false then (* FIXME: other cases? *)
    2
  else if is_diff_of_two_squares ~max_side:n k
       || is_diff_of_two_squares ~max_side:n l then
    3
  else if is_sum_of_three_squares ~sum_sides:n k
       || is_sum_of_three_squares ~sum_sides:n l then
    3
  else if is_bounded_rectangle ~max_side:n k
       || is_bounded_rectangle ~max_side:n l then
    3
  else if false then (* FIXME: other cases? *)
    3
  else
    4
