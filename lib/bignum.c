/* 
 *  Bignums
 */
#include "bignum.h"



/*     n  =  a                               */
void  BigInt_set_
  (BigInt* n, unsigned a)
{
	BigInt_zero(n);
	for(unsigned i = 0;  i < sizeof(unsigned);  i++)
		n->b[i] = ((unsigned char*)&a)[i];
}



/*     n  =  a                               */
void  BigInt_set
  (BigInt* n, BigInt const* a)
{
	memcpy(n->b, a->b, SIZEOF_BIGINT);
}



/*     n  =  2^p                             */
void  BigInt_pow2_
  (BigInt* n, unsigned p)
{
	unsigned i = p / CHAR_BIT;
	
	BigInt_zero(n);
	
	if(i < SIZEOF_BIGINT)
		n->b[i] = 1 << (p%CHAR_BIT);
}



/*     n  =  0                               */
void  BigInt_zero
  (BigInt* n)
{
	memset(&n->b, 0, SIZEOF_BIGINT);
}



bool  BigInt_isZero
  (BigInt const* n)
{
	for(unsigned i = 0;  i < SIZEOF_BIGINT;  i++)
		if(n->b[i])
			return false;
	return true;
}



/* Compare two big ints, as strcmp does. */
int  BigInt_cmp
  (const BigInt* a, const BigInt* b)
{
	for(unsigned i = SIZEOF_BIGINT;  i;  i--)
		if(a->b[i-1] != b->b[i-1])
			return a->b[i-1] - b->b[i-1];
	return 0;
}



/*     n--
 * Returns 0 if n is null after the decrementation, 1 otherwise. */
int  BigInt_dec
  (BigInt* n)
{
	for(unsigned i = 0;  i < SIZEOF_BIGINT;  i++) {
		n->b[i]--;
		if(n->b[i] != UCHAR_MAX)
			break;
	}
	
	return !BigInt_isZero(n);
}



/*     n  =  a + b
 * Returns 0, or 1 if there was an overflow. */
int  BigInt_add_
  (BigInt* n, BigInt const* a, unsigned b)
{
	for(unsigned i = 0;  b  &&  i < SIZEOF_BIGINT;  i++) {
		b += a->b[i];
		n->b[i] = b;
		b >>= CHAR_BIT;
	}
	
	return b;
}



/*     n  =  a + b
 * Returns 0, or 1 if there was an overflow. */
int  BigInt_add
  (BigInt* n, BigInt const* a, BigInt const* b)
{
	unsigned tmp = 0;
	
	for(unsigned i = 0;  i < SIZEOF_BIGINT;  i++) {
		tmp += a->b[i] + b->b[i];
		n->b[i] = tmp;
		tmp >>= CHAR_BIT;
	}
	
	return tmp;
}



/*     n  =  a * b  +  c
 * Returns 0, or 1 if there was an overflow. */
int  BigInt_mulAdd_
  (BigInt* n, BigInt const* a, /*BigInt const* b*/unsigned b, unsigned c)
{
	unsigned tmp;
	
	for(unsigned i = 0;  i < SIZEOF_BIGINT;  i++) {
		tmp = a->b[i];
		tmp *= b;
		tmp += c;
		n->b[i] = tmp;
		c = tmp>>CHAR_BIT;
	}
	
	return c;
}



/*     n  =  a * b
 * Returns 0, or 1 if there was an overflow. */
int  BigInt_mul_
  (BigInt* n, BigInt const* a, /*BigInt const* b*/unsigned b)
{
	return BigInt_mulAdd_(n, a, b, 0);
}



/*     n  =  (a+c) / b
 * Returns the remainder. */
unsigned  BigInt_addDiv_
  (BigInt* n, BigInt const* a, /*BigInt const* b*/unsigned b, unsigned c)
{
	for(unsigned i = SIZEOF_BIGINT;  i;  i--) {
		c <<= CHAR_BIT;
		c += a->b[i-1];
		n->b[i-1] = c/b;
		c %= b;
	}
	
	return c;
}



/*     n  =  a / b
 * Returns the remainder. */
unsigned  BigInt_div_
  (BigInt* n, BigInt const* a, /*BigInt const* b*/unsigned b)
{
	return BigInt_addDiv_(n, a, b, 0);
}



void  BigInt_fromStr
  (BigInt* n, char const* s)
{
	BigInt_zero(n);
	for(;  isdigit(*s);  s++)
		BigInt_mulAdd_(n, n, 10, *s-'0');
}



void  BigInt_toStr
  (BigInt const* n, char s[SIZEOF_BIGINT_BUFFER])
{
	BigInt m = *n;
	char buf[SIZEOF_BIGINT_BUFFER];
	unsigned i;
	
	for(i = 0;  !BigInt_isZero(&m);  i++) {
		buf[i] = BigInt_div_(&m, &m, 10) + '0';
	}
	
	for(unsigned j = i;  j;  j--)
		s[i-j] = buf[j-1];
	s[i] = '\0';
}
