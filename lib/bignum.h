/* 
 *  Bignums
 */
#ifndef INCLUDED_BIGNUM_20120827_2302_MM
#define INCLUDED_BIGNUM_20120827_2302_MM

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <limits.h>


#define  SIZEOF_BIGINT  1280
#define  SIZEOF_BIGINT_BUFFER  3200



typedef struct {
	unsigned char b[SIZEOF_BIGINT];
} BigInt;



/*     n  =  a                               */
void  BigInt_set_
  (BigInt* n, unsigned a);



/*     n  =  a                               */
void  BigInt_set
  (BigInt* n, BigInt const* a);



/*     n  =  2^p                             */
void  BigInt_pow2_
  (BigInt* n, unsigned p);



/*     n  =  0                               */
void  BigInt_zero
  (BigInt* n);



bool  BigInt_isZero
  (BigInt const* n);



/* Compare two big ints, as strcmp does. */
int  BigInt_cmp
  (const BigInt* a, const BigInt* b);



/*     n--
 * Returns 0 if n is null after the decrementation, 1 otherwise. */
int  BigInt_dec
  (BigInt* n);



/*     n  =  a + b
 * Returns 0, or 1 if there was an overflow. */
int  BigInt_add_
  (BigInt* n, BigInt const* a, unsigned b);



/*     n  =  a + b
 * Returns 0, or 1 if there was an overflow. */
int  BigInt_add
  (BigInt* n, BigInt const* a, BigInt const* b);



/*     n  =  a * b  +  c
 * Returns 0, or 1 if there was an overflow. */
int  BigInt_mulAdd_
  (BigInt* n, BigInt const* a, /*BigInt const* b*/unsigned b, unsigned c);



/*     n  =  a * b
 * Returns 0, or 1 if there was an overflow. */
int  BigInt_mul_
  (BigInt* n, BigInt const* a, /*BigInt const* b*/unsigned b);



/*     n  =  (a+c) / b
 * Returns the remainder. */
unsigned  BigInt_addDiv_
  (BigInt* n, BigInt const* a, /*BigInt const* b*/unsigned b, unsigned c);



/*     n  =  a / b
 * Returns the remainder. */
unsigned  BigInt_div_
  (BigInt* n, BigInt const* a, /*BigInt const* b*/unsigned b);



void  BigInt_fromStr
  (BigInt* n, char const* s);



void  BigInt_toStr
  (BigInt const* n, char s[SIZEOF_BIGINT_BUFFER]);



#endif    /* header not included */
