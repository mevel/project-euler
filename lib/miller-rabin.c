#include <stdbool.h>
#define FOR(i,n) for(long long int i=0; i<n; i++)

// modular exponentiation
__int128 fexp(__int128 a, __int128 b, __int128 m) {
  if(b==0) return 1;
  if(b==1) return a%m;
  __int128 c = fexp(a,b/2,m);
  c=c*c%m;
  if(b&1) c=c*(a%m)%m;
  return c;
}

// Miller-Rabin
bool f(__int128 n, __int128 a) {
  if(n==a) return 1;
  int s = __builtin_ctzll(n-1);
  __int128 d = (n-1)>>s;
  if(fexp(a,d,n) == 1) return 1;
  FOR(r,s) if(fexp(a,d<<r,n) == n-1) return 1;
  return 0;
}

bool is_prime(__int128 n) {
  if(n != 2 && n%2==0) return 0;
  if(n==46856248255981) return 0;
  if(!f(n,2)) return 0;
  if(!f(n,3)) return 0;
  if(!f(n,7)) return 0;
  if(!f(n,61)) return 0;
  if(!f(n,24251)) return 0;
  return 1;
}

#include <stdio.h>

int main(void) {
	// le contre-exemple magique:
	printf("should be 0: %u\n", is_prime(46856248255981));
	// autres contre-exemples < 10^16:
	printf("should be 0: %u\n", is_prime(  669094855201));
	printf("should be 0: %u\n", is_prime( 1052516956501));
	printf("should be 0: %u\n", is_prime( 2007193456621));
	printf("should be 0: %u\n", is_prime( 2744715551581));
	printf("should be 0: %u\n", is_prime( 9542968210729));
	printf("should be 0: %u\n", is_prime(17699592963781));
	printf("should be 0: %u\n", is_prime(19671510288601));
	// ...
}
