/*
 *  Project Euler
 *    template file
 */

#ifdef CPL_FILE
  
  #define  main  ProjectEuler_main
  #include CPL_FILE
  #undef main
  
  #include <sys/time.h>
  #include <stdio.h>
  
	int main(void)
	{
		struct timeval start, end;
		long s, ms, us;
		int ret;
		
		gettimeofday(&start, NULL);
		
		ret = ProjectEuler_main();
		
		gettimeofday(&end, NULL);
		
		s  = end.tv_sec  - start.tv_sec;
		us = end.tv_usec - start.tv_usec;
		ms = s*1000 + us/1000.0 + 0.5;
		
		printf("\n--[ %lu ms (%lu µs) ]--\n", ms, us);
		return ret;
	}
  
#else
  
  #include <stdio.h>
  
	int main(void)
	{
		puts("Compile this file with ‘ -DCPL_FILE=\"wanted_source_file.c\" ’.");
		return -1;
	}
  
#endif
